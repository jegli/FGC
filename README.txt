#FGC IP Core project
This IP is a function generator able to generate up to 16 functions at the same time. 
An external memory is required to store the function vectors.

## Structure
- `Vivado/`: IP core project
- `MemMap/`: Memory Map done in Cheburashka

## Build
1. Check if the `Vivado/Sources/IPCores/` directory is not empty. If it is, update the submodules:

    `git submodule update --init`
	
2. Build the project: 

	- open Vivado
	- In the tcl console, run the tcl script: source ./build_project.tcl
	
## Simulation
1. In Vivado, compile the project.
2. SIMULATION -> Run Simulation 
3. At the end of the simulation, check the result in the Tcl Console