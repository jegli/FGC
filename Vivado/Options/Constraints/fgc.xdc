#creating timing constaints
#set_clock_groups -asynchronous -group [get_clocks -of_objects [get_ports clk_i]] -group [get_clocks -of_objects [get_ports m_axi_clk_i]]

set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports clk_i]] -to [get_clocks -of_objects [get_ports m_axi_clk_i]] 8.000
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports m_axi_clk_i]] -to [get_clocks -of_objects [get_ports clk_i]] 8.000