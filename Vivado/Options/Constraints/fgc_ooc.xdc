# creating 125MHz and 200MHz clocks
create_clock -name clk -period 8.000 [get_ports clk_i]
create_clock -name m_axi_clk -period 5.000 [get_ports m_axi_clk_i]
