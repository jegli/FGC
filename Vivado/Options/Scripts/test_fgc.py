# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#from PyCheb.PyCheb import PyCheb
#
# 
#
#sps_map = PyCheb("../MemMap/SPS200CavityLoops.xml").get()
#
# 
#
#for element in sps_map.walk():
#
#    print(element)

 

import pandas as pd
import argparse
from collections import namedtuple
import re
from sys import exit
from time import sleep
from PyChebDHWA import PyChebRemotePCI
from DHWA import DHWA

#Vector format
Vector = namedtuple("Vector", ["slope", "interval"])

#debug tuple
debug = ("funct_busy_0","funct_busy_1","funct_busy_2","funct_busy_3","buff_sel","interlock","inter_clk")

#paramaters
MAX_FUNCT = 16
BUFFER_SIZE = 4096
MEM_BUFFER_0_START = 0x00000000
MEM_BUFFER_1_START = 0x00000000
MEM_SIZE =  0x80000000
CYCLES = 1
COAST_VECTORS = 10

FIRMWARE_VERSION = 20181031

#==============================================================================
class VirtualFGC:
    def __init__(self, buffer_size = BUFFER_SIZE):
        self._enable =  [False for i in range(MAX_FUNCT)]
        self._start =   [0 for i in range(MAX_FUNCT)]
        self._length =  [0 for i in range(MAX_FUNCT)]
        self._value =   [0 for i in range(MAX_FUNCT)]
        self._buffer = [Vector(0,0) for i in range(MAX_FUNCT * BUFFER_SIZE)]
        
        self._selector = 0
        
        self.buffer_size = BUFFER_SIZE
    
    def __str__(self):
        vals = []
        for s in range(MAX_FUNCT):
            self.selector = s
            vals.append(f"#{s}: {self.enable} - {self.start} - {self.length}") 
            vals.append(f"{self.buffer}")       
        return "\n".join(vals)
    
    @property
    def selector(self):
        return self._selector
    
    @selector.setter
    def selector(self, value):
        assert (0 <= value <= MAX_FUNCT-1)
        self._selector = value
    
        
    @property
    def enable(self):
        return self._enable[self._selector]
    
    @enable.setter
    def enable(self, value):
        assert(value == True or value == False)
        self._enable[self._selector] = value
        
    @property
    def start(self):
        return self._start[self._selector]
    
    @start.setter
    def start(self, value):
        assert(0 <= value < MEM_SIZE)
        self._start[self._selector] = value
        
    @property
    def length(self):
        return self._length[self._selector]
    
    @length.setter
    def length(self, value):
        assert(0 <= value < self.buffer_size)
        self._length[self._selector] = value

    @property
    def buffer(self):
        return self._buffer[self._selector]
    
    @buffer.setter
    def buffer(self, value):
        self._buffer[self._selector] = value

#==============================================================================
def change_value(obj, value):
    obj._value = value
    #new_value = obj._value
    #print("{0: <30}: {1}".format("New Setting '" + obj._name + "'", new_value))
    
#==============================================================================
def debug_select(cheb,select):
    index = debug.index(select)
    change_value(cheb.gpio.gpio0,  index<<3)
    print("debug output signal: {}" .format(debug[index]))
    
#==============================================================================
def fgc_reset(cheb):   
    print("FGC reset...")
    change_value(cheb.fgc.control.softReset,  1)                    #perform a soft reset
    change_value(cheb.fgc.control.clrFaults,  1)
    print("Sleep 2 seconds...")
    sleep(2)
#==============================================================================
def set_fgc_control_reg(cheb,value=None):
    print("Init control register...")
    if value:
        change_value(cheb.fgc.control, value)
    else:
        change_value(cheb.fgc.control.clearFunctsAfterCoast,  0)
        change_value(cheb.fgc.control.softReset,  0)                    
        change_value(cheb.fgc.control.clrFaults,  0)
        change_value(cheb.fgc.control.fifoManEnable,  1)
        change_value(cheb.fgc.control.functMemReady,  0)
        change_value(cheb.fgc.control.coastMemReady,  0)
        change_value(cheb.fgc.control.softTrigPrepare,  0)
        change_value(cheb.fgc.control.softTrigTrim,  0)
        change_value(cheb.fgc.control.softTrigCoastStart,  0)
        change_value(cheb.fgc.control.softTrigCoastStop,  0)
        change_value(cheb.fgc.control.prepareTrigSel,  0)		#software trigger selected
        change_value(cheb.fgc.control.coastTrigSel,  0)			#software trigger selected
        change_value(cheb.fgc.control.coastStartTrigSel,  0)	#software trigger selected	
        change_value(cheb.fgc.control.coastStopTrigSel,  0)	    #software trigger selected	
#==============================================================================		
def set_fgc_faultsEnable_reg(cheb,value=None):	
	print("Init faults enable register...")
	if value:
		change_value(cheb.fgc.faultsEnable,value)
	else:
		change_value(cheb.fgc.faultsEnable.accSatFaultEnable,  1)
		change_value(cheb.fgc.faultsEnable.fifoEmptyFaultEnable,  1)
		change_value(cheb.fgc.faultsEnable.fifoFullFaultEnable,  1)
		change_value(cheb.fgc.faultsEnable.cycleMismatchFaultEnable,  1)
		change_value(cheb.fgc.faultsEnable.functMemReadyFaultEnable,  1)
		change_value(cheb.fgc.faultsEnable.coastMemReadyFaultEnable,  1)
#==============================================================================		
def set_fgc_functConfig_reg(cheb,hw,value=None):	
    print("Init faults enable register...")
    if value:
        change_value(cheb.fgc.faultsEnable,value)
    else:
        change_value(cheb.fgc.functConfig.startTrigSel,  0)
        change_value(cheb.fgc.functConfig.restartTrigSel,  0)
        change_value(cheb.fgc.functConfig.interpolDisable,  0)
        change_value(cheb.fgc.functConfig.wrapEn,  0)
        change_value(cheb.fgc.functConfig.coastEnable,  hw.enable)
        change_value(cheb.fgc.functConfig.enable, hw.enable)         
#==============================================================================
def set_function_config(cheb,hw,f,ddr):	
    hw.selector = f

    if f==None:
        print("Error function configuration %d" %f)
        return
    else:
        print("****************************************")
        print("Configure function %d..." %f)
        print("Init function configuration register...")
        change_value(cheb.fgc.functSel.functionSelect,  f)
        set_fgc_functConfig_reg(cheb,hw)
        
        print("Init start value register...")
        print(int(hw.start))
        change_value(cheb.fgc.startValue,  int(hw.start))							
        
        print("Init function size register...")
        print(hw.length)
        change_value(cheb.fgc.functionSize.functionSize,  hw.length)		

        print("Loading buffer %d in external memory..." %f)
        data = []

        for I in range (1,len(hw.buffer)+1):
            # write data to memory access
            data.append(int(hw.buffer["slope"][I]))
            data.append(int(hw.buffer["interval"][I]))#hw.buffer["slope"][I])#,hw.buffer["interval"][I])
#            print(data,I,len(hw.buffer))
            
        mem_addr = MEM_BUFFER_0_START+f*BUFFER_SIZE    
        print("Writing function %d [%d vectors] at address %d" %(f,len(hw.buffer),mem_addr))    
        ddr.write(mem_addr,data)
        print("Memory content: {}" .format(ddr.read(mem_addr,len(data))))
#==============================================================================
def load_coast_trim(cheb):

    x = 0
    while x < COAST_VECTORS*2:
        cheb.fgc.coastMemory[x]      = (10+x)<<16
        cheb.fgc.coastMemory[x+1]    = 20
        x += 2
 
    cheb.fgc.coastMemory[COAST_VECTORS*2] = 0;
    cheb.fgc.coastMemory[(COAST_VECTORS*2)+1] = 0xffffffff;
#    print(cheb.fgc.coastMemory._raw)
    change_value(cheb.fgc.control.coastMemReady,  1)
    
def coast_testing(cheb,enable):
    
    print("****************************************")
    print("Coast mode testing:") 
    change_value(cheb.fgc.control.functMemReady,  1)
    change_value(cheb.fgc.control.softTrigPrepare, 1)
    sleep(1)
    
    while cheb.fgc.status.fifoManPreloadingFinished._value == 0:
        sleep(1)
        
    print("start function playing...")    
    change_value(cheb.fgc.softTrig, enable)
    sleep(2)
        
    print("Enter coast mode") 
    change_value(cheb.fgc.control.softTrigCoastStart,  1)
    
    #loading coast in memory 
    while True:
        load_coast_trim(cheb)
        
        var = input("Perform a coast trim ? (Y/N) ")
        
        if(var == 'Y'):
            print("Performing coast trim...")
            change_value(cheb.fgc.control.softTrigTrim,  1)
            print(cheb.fgc.faults)
            print(cheb.fgc.functionFaults)
            print(cheb.fgc.fifoFaults)
            print(cheb.fgc.currentValue._value)
        else:
            break
        
    print("Exit coast mode...") 
    change_value(cheb.fgc.control.softTrigCoastStop,  1)    
    print("Waiting 5 seconds...")
    sleep(5)  
#==============================================================================
            
def cyle_generator(cheb,enable):
    for cycle in range(CYCLES):
        change_value(cheb.fgc.functSel.functionSelect,  0)
        
        print("****************************************")
        print("Start cycle %d generation..." %cycle)
        # set memory ready bit -> functions were loaded in the memory
        change_value(cheb.fgc.control.functMemReady,  1)
    
        buff_sel = cheb.fgc.status.buffSel._value
        print("buffer selection is %d" %buff_sel)
        
    #    if buff_sel:
        change_value(cheb.fgc.bufferStartAddress, MEM_BUFFER_0_START) # hardcoded to buffer 0
    #    else:
    #    change_value(cheb.fgc.bufferStartAddress, MEM_BUFFER_1_START)
    
        change_value(cheb.fgc.control.softTrigPrepare, 1)
        sleep(1)
        
        while cheb.fgc.status.fifoManPreloadingFinished._value == 0:
            sleep(1)
        
        print("Function preloading finished")
        print("Start the function playing...")
        change_value(cheb.fgc.softTrig, enable)
        print("waiting that function playing is finished...")
        while cheb.fgc.busy.functBusy._value != 0:
            print(cheb.fgc.currentValue._value)
            sleep(1)
        print("function playing finished!")
      
    coast_testing(cheb,enable)
    
#==============================================================================
def test_fgc(files,cheb,ddr):
    enable = 0
    fgc_reset(cheb)
    print("FGC configuration started:")
    
    # configuration control and faultsEnable registers
    set_fgc_control_reg(cheb) 
    set_fgc_faultsEnable_reg(cheb)
    
    # create virtual fgc IP 
    hw = VirtualFGC()
    
    #read file and configure virtual FGC registers
    for f in files:
        m = re.match("function(\d+).csv", f.name)   
        if not m:
            raise ValueError(f"Invalid filename: {f.name}")
        
        # get function number defined by the file name 
        index = int(m[1])
        if not index in range(MAX_FUNCT):
            raise ValueError(f"Invalid function numer: {index}")
            
        # read file
        data_frame = pd.read_csv(f)         
        
        rows, cols = data_frame.shape
        # select function
        hw.selector = index
        # enable function
        hw.enable = True
        # get function length
        hw.length = rows-1 
        # get start value
        hw.start = data_frame.iloc[0]["slope"]
        
        data_frame = data_frame.tail(rows-1)
        # get function vectors
        hw.buffer = data_frame
        
        # record which function was enabled
        enable = enable | (1 << index)

        # configure the FGC
        set_function_config(cheb,hw,index,ddr)
        
    print("****************************************")
    #print(hw)
    cyle_generator(cheb,enable)

def main():
    #arguments 
    parser = argparse.ArgumentParser(
        description='Run FGC test')
    parser.add_argument('-file',type=argparse.FileType('r'), nargs='+', required = True,
                                help='file containing the function data')
    parser.add_argument('-s','--select',type=str, default = None,
                                help='I/O debug signal selection')
    args = parser.parse_args()
    
    #create remote connection and cheburshka memory map 
    with PyChebRemotePCI("../MemMap/SPS200CavityLoops.xml",
            base_addr=0x01000000,   #Cheb offset
            devid=0x030000,         #SIS8300 Board 0
            bar=0,                  #Bar 0 => M_AXI of PCI bridge
            pagesize=0x2000000,     #32MB? 16MB doesn't work!
            offset=0,
            hostname="localhost") as cheb:
        
        print("****************************************")
        print(cheb.firmwareID)
        print(cheb.firmwareVersion)
        #connect the selected signal on debug pin
        debug_select(cheb,args.select)
        
        if cheb.firmwareVersion != FIRMWARE_VERSION:
            exit("Firmware version mismatch")
        print("****************************************")
        
        # create remote connection to ddr memory
        with DHWA(
            class_name="RPC",
            remote_class_name="ual",
            word_width=32,
            bus_type="pci",
            slot=None,
            devid=0x030000,         #SIS8300 Board 0
            bar=4,                  #Bar 4 => PCIe to DMA Bypass
            pagesize=0x20000000,    #512MB
            offset=0,
            hostname="localhost") as ddr:

            # start fgc testing
            test_fgc(args.file,cheb,ddr)    
 
if __name__ == "__main__":
    main()
