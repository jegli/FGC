-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, FGC simulation Package --
-- --
-------------------------------------------------------------------------------
--
-- unit name: FGC Package
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 03/08/2018
--
-- version: 1.0
--
-- description: function generator simulation package
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library work;
use work.FGC_PACK.all;
use work.FGC_CONST_SIM_PACK.all;
use work.MemMap_fgc.all;

use STD.textio.all;
use ieee.std_logic_textio.all;

package FGC_SIM_PACK is
    --=====================================================================
    -- Constants definition 
    --=====================================================================
    
    -- matlab constants for simulation
    constant c_CYCLES                           : integer := c_cycles;                  -- number of machine cycles 
    constant c_FGC_CHANNELS                     : integer := c_function_nbr;            -- number of functions  
    constant c_VECTOR_NUMBER                    : integer := c_vector_nbr;              -- number of vectors per function
    constant c_FUNCT_BUFFER_SIZE                : integer := c_funct_buffer_size;       -- memory buffer size available per function (in bytes)
    constant c_COAST_TRIM_NUMBER                : integer := c_coast_trim_nbr;          -- number of coast trim
    constant c_COAST_VECTOR_NUMBER              : integer := c_coast_vector_nbr;        -- number of coast vectors per coast trim
    constant c_FUNCTION_TESTING                 : integer := c_function_testing;      --function selection for coast testing
    constant c_CYCLE_SEL_FOR_COAST              : integer := c_cycle_sel_for_coast;     --cycle selection for coast mode testing with trigger
    constant c_CYCLE_SEL_FOR_SPECIAL_VECTOR     : integer := c_cycle_sel_for_coast;     --cycle selection for coast mode testing with special vector
  
    -- general IP core constants
    constant c_MEM_AXI_DATA_WIDTH           : integer := 64;
    constant c_COAST_AXI_DATA_WIDTH         : integer := 64;
    constant c_REG_AXI_DATA_WIDTH           : integer := 32;
    constant c_AXI_ADDR_WIDTH               : integer := 32;
    constant c_FIFO_INFO_DATA_WIDTH         : integer := 8;
    constant c_FUNCT_OUT_WIDTH              : integer := 32;
    constant c_COAST_BUFFER_DEPTH_64BIT     : integer := 32;
    constant c_COAST_BUFFER_DEPTH_32BIT     : integer := 2*c_COAST_BUFFER_DEPTH_64BIT;
    constant c_FUNCTION_MEMORY_DEPTH        : integer := 16384;
    constant c_MAX_FUNCTIONS                : integer := 16;
    
    -- faults register
    constant c_ACC_SAT_FAULT            : integer := 0;
    constant c_FIFO_EMPTY_FAULT         : integer := 1;
    constant c_FIFO_FULL_FAULT          : integer := 2;
    constant c_CYCLE_MISMATCH_FAULT     : integer := 3;
    constant c_FUNCT_MEM_READY_FAULT    : integer := 4;
    constant c_COAST_MEM_READY_FAULT    : integer := 5;
    constant c_ENABLE_FAUTLS            : integer := 16#0003F#;
    
    -- control register
    constant c_DEFAULT_CTRL_REG         : integer := 16#4055#;              
    constant c_COASTMEMREADY_BIT_FIELD  : integer := 12;                    
    constant c_FUNCTMEMREADY_BIT_FIELD  : integer := 13;   
    constant c_CLRFAULTS_BIT_FIELD      : integer := 15;   
    
    -- status register
    constant c_PRELOADING_FINISHED_BIT_FIELD : integer := 14;                        

    -- faults tester 
    constant c_DEFAULT_FUNCT_CONFIG     : integer := 16#8004#;

    -- general constants
    constant c_ALL_ONE                  : std_logic_vector(c_FGC_CHANNELS-1 downto 0) := (others => '1');
    constant c_ALL_ZERO                 : std_logic_vector(c_FGC_CHANNELS-1 downto 0) := (others => '0');
    --=====================================================================
    -- AXI read/write bus type definition
    --=====================================================================
    type t_AXI_WR_bus_out is
    record 
        AWVALID :  std_logic;
        BREADY  :  std_logic;
        WLAST   :  std_logic;
        WVALID  :  std_logic;
        AWADDR  :  std_logic_vector (c_AXI_ADDR_WIDTH-1 downto 0);
        WDATA   :  std_logic_vector (c_REG_AXI_DATA_WIDTH-1 downto 0);
        WSTRB   :  std_logic_vector (3 downto 0);
    end record;
    
    type t_AXI_RD_bus_out is
    record 
        ARVALID :  std_logic;
        RREADY  :  std_logic;
        ARADDR  :  std_logic_vector (c_AXI_ADDR_WIDTH-1 downto 0);
    end record;
    
    type t_AXI_WR_bus_in is
    record 
        AWREADY : std_logic;
        BVALID  : std_logic;
        WREADY  : std_logic;
        BRESP   : std_logic_vector (1 downto 0);
    end record;
    
    type t_AXI_RD_bus_in is
    record 
        ARREADY : std_logic;
        RLAST   : std_logic;
        RVALID  : std_logic;
        RRESP   : std_logic_vector (1 downto 0);
        RDATA   : std_logic_vector (c_REG_AXI_DATA_WIDTH-1 downto 0);
    end record;
    
    type t_function_data_std_array    is array (0 to c_MAX_FUNCTIONS-1) of std_logic_vector(c_FUNCT_OUT_WIDTH-1 downto 0);
    type t_function_table_std_array   is array (0 to c_CYCLES-1,0 to c_FGC_CHANNELS-1,0 to c_FUNCTION_MEMORY_DEPTH-1) of std_logic_vector(c_FUNCT_OUT_WIDTH-1 downto 0);
    type t_function_data_int_array    is array (0 to c_CYCLES-1,0 to c_FGC_CHANNELS-1) of integer;
    type t_function_table_int_array   is array (0 to c_CYCLES-1,0 to c_FGC_CHANNELS-1,0 to c_FUNCTION_MEMORY_DEPTH-1) of integer;
    
    type t_funct_data is
    record
        start_value    : t_function_data_int_array; 
        length         : t_function_data_int_array;
        data           : t_function_table_int_array;
    end record;

    type t_coast_data_file_array      is array (0 to c_COAST_BUFFER_DEPTH_64BIT-1) of t_vector_format;
    type t_coast_table_array          is array (0 to 1) of t_coast_data_file_array;
    type t_coast_length_array         is array (0 to c_COAST_TRIM_NUMBER-1) of integer;
    type t_coast_data_array           is array (0 to c_COAST_TRIM_NUMBER-1, 0 to 50*c_COAST_VECTOR_NUMBER) of integer; 

    type t_coast_data is
    record
        length : t_coast_length_array;
        data   : t_coast_data_array;
    end record;
    
    --=====================================================================
    -- procedure definition
    --=====================================================================
        -- not generic for the moment
        procedure AXI_WRITE
        (    
            addr_i      : in std_logic_vector;
            data_i      : in integer;
            signal      AXI_bus_in_i    : in  t_AXI_WR_bus_in;
            signal      AXI_bus_out_o   : out t_AXI_WR_bus_out;
            signal      clk_i           : in  std_logic
        );
        -- not generic for the moment
        procedure AXI_READ
        (    
            addr_i      : in std_logic_vector;
            data_o      : out std_logic_vector (c_REG_AXI_DATA_WIDTH-1 downto 0);
            signal      AXI_bus_in_i    : in  t_AXI_RD_bus_in;
            signal      AXI_bus_out_o   : out t_AXI_RD_bus_out;
            signal      clk_i           : in  std_logic
        );

        procedure FAULTS_TESTER
        (    
            signal      start_trig_o        : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o      : out std_logic;
            signal      coast_trim_trig_o   : out std_logic;
            signal      interlock_i         : in  std_logic;
            signal      function_data_i     : in  t_funct_data;
            signal      AXI_WR_bus_in_i     : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o    : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i     : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o    : out t_AXI_RD_bus_out;
            signal      clk_i               : in  std_logic;
            error_o     : out integer
        );

        procedure FUNCTIONS_TESTER
        (    
            signal      funct_busy_i        : in  std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      start_trig_o        : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o      : out std_logic;
            signal      inter_disable_o     : out std_logic;
            signal      cycle_cnt_o         : out integer;
            signal      function_data_i     : in  t_funct_data;
            signal      AXI_WR_bus_in_i     : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o    : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i     : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o    : out t_AXI_RD_bus_out;
            signal      clk_i               : in  std_logic;
            signal      m_axi_clk_i         : in  std_logic;
            error_o     : out integer
        );
        
        procedure COAST_TRIM_TESTER
        (    
            signal      start_trig_o                : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o              : out std_logic;
            signal      coast_trim_trig_o           : out std_logic;
            signal      coast_start_o               : out std_logic;
            signal      coast_stop_o                : out std_logic;
            signal      coast_mode_cycle_o          : out std_logic;
            signal      function_data_i             : in  t_funct_data;
            signal      AXI_WR_bus_in_i             : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o            : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i             : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o            : out t_AXI_RD_bus_out;
            signal      clk_i                       : in  std_logic;
            signal      m_axi_clk_i                 : in  std_logic;
            signal      coast_trim_cnt_o            : out integer;
            signal      coast_vector_i              : in  t_coast_table_array;
            
            coast_loading_error_o                   : out integer
        );  
        
        procedure SPECIAL_VECTOR_TESTER
        (    
            signal      start_trig_o                : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o              : out std_logic;
            signal      restart_trig_o              : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      special_vector_detected_i   : in  std_logic;
            signal      function_data_i             : in  t_funct_data;
            signal      AXI_WR_bus_in_i             : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o            : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i             : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o            : out t_AXI_RD_bus_out;
            signal      clk_i                       : in  std_logic;
            signal      m_axi_clk_i                 : in  std_logic
        );   
        
         procedure COAST_MEMORY_LOADING
        (
            signal      coast_vector_i              : t_coast_table_array;
            signal      AXI_WR_bus_in_i             : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o            : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i             : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o            : out t_AXI_RD_bus_out;
            signal      clk_i                       : in  std_logic;
            
            coast_buff_sel_i                        : in  integer;
            error_o                                 : out integer
        );    
           
     --=====================================================================
    -- function definition
    --=====================================================================   
        function FUNCT_FILE_READ(filename : string) return t_funct_data;
        
        function COAST_VECTOR_FILE_READ(filename : string) return t_coast_data_file_array;
        
        function COAST_DATA_FILE_READ(filename : string) return t_coast_data;
    
    end FGC_SIM_PACK;
    
--=====================================================================
-- procedure declaration
--=====================================================================
    
package body FGC_SIM_PACK is
        --=====================================================================================
        --  32-bit axi write procedure
        --=====================================================================================
        procedure AXI_WRITE (   addr_i                  : in std_logic_vector;
                                data_i                  : in integer;
                                signal AXI_bus_in_i     : in  t_AXI_WR_bus_in;
                                signal AXI_bus_out_o    : out t_AXI_WR_bus_out;
                                signal clk_i            : in  std_logic
                            ) is
            variable v_addr : std_logic_vector(addr_i'HIGH downto 0) := (others => '0');
        begin
            v_addr := addr_i & std_logic_vector(to_unsigned(0,addr_i'RIGHT));
--            report "Write to address:" & integer'image(to_integer(unsigned(v_addr)));
        
            --init
            AXI_bus_out_o.AWVALID   <='0';
            AXI_bus_out_o.WVALID    <='0';
            AXI_bus_out_o.BREADY    <='0';
        
            AXI_bus_out_o.AWADDR    <= std_logic_vector(to_unsigned(to_integer(unsigned(v_addr)),AXI_bus_out_o.AWADDR'length));
            AXI_bus_out_o.WDATA     <= std_logic_vector(to_unsigned(data_i, AXI_bus_out_o.WDATA'length));
            AXI_bus_out_o.WSTRB     <= (others => '1');
        
            --send write
            wait until clk_i = '0';
                AXI_bus_out_o.AWVALID   <= '1';
                AXI_bus_out_o.WVALID    <= '1';
            wait until (AXI_bus_in_i.AWREADY and AXI_bus_in_i.WREADY) = '1';  --Client ready to read address/data   
                AXI_bus_out_o.BREADY<='1';
            wait until AXI_bus_in_i.BVALID = '1';
                --assert AXI_bus_in_i.BRESP = "00" report "AXI data not written" severity failure;
                AXI_bus_out_o.AWVALID   <= '0';
                AXI_bus_out_o.WVALID    <= '0';
                AXI_bus_out_o.BREADY    <= '1';
            wait until AXI_bus_in_i.BVALID = '0';  --AXI Write finished
                AXI_bus_out_o.BREADY    <= '0';
                AXI_bus_out_o.WSTRB     <= (others => '0');
                AXI_bus_out_o.WDATA     <= (others => '0');
                AXI_bus_out_o.AWADDR    <= (others => '0');
            wait for 1us;
        end AXI_WRITE;
        
        --=====================================================================================
        --  32-bit axi read procedure
        --=====================================================================================
        procedure AXI_READ (    addr_i      : in std_logic_vector;
                                data_o      : out std_logic_vector (c_REG_AXI_DATA_WIDTH-1 downto 0);
                                signal      AXI_bus_in_i    : in  t_AXI_RD_bus_in;
                                signal      AXI_bus_out_o   : out t_AXI_RD_bus_out;
                                signal      clk_i           : in  std_logic
                             ) is
                             
            variable v_addr : std_logic_vector(addr_i'HIGH downto 0) := (others => '0');
        begin
            v_addr := addr_i & std_logic_vector(to_unsigned(0,addr_i'RIGHT));
--            report "Read at address:" & integer'image(to_integer(unsigned(v_addr)));
        
            --init
            AXI_bus_out_o.ARVALID   <='0';
            AXI_bus_out_o.RREADY    <='0';

            --send read 
            wait until clk_i = '0';
                AXI_bus_out_o.ARADDR    <= std_logic_vector(to_unsigned(to_integer(unsigned(v_addr)),AXI_bus_out_o.ARADDR'length));
                AXI_bus_out_o.ARVALID   <= '1';
                AXI_bus_out_o.RREADY    <= '1';
            wait until (AXI_bus_in_i.ARREADY = '1' and clk_i = '1');  --Client provided data 
            wait until clk_i = '1';
                AXI_bus_out_o.ARVALID   <= '0';
            wait until (AXI_bus_in_i.RVALID  = '1' and clk_i = '1');  --Client provided data  
            wait until clk_i = '1';
                AXI_bus_out_o.RREADY    <='0';
                data_o                  :=  AXI_bus_in_i.RDATA;
            wait for 1us;
        end AXI_READ;

        --=====================================================================================
        --  test the faults detection mechanism
        --=====================================================================================
        procedure FAULTS_TESTER (    
                                    signal      start_trig_o        : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
                                    signal      prepare_trig_o      : out std_logic;
                                    signal      coast_trim_trig_o   : out std_logic;
                                    signal      interlock_i         : in  std_logic;
                                    signal      function_data_i     : in  t_funct_data;
                                    signal      AXI_WR_bus_in_i     : in  t_AXI_WR_bus_in;
                                    signal      AXI_WR_bus_out_o    : out t_AXI_WR_bus_out;
                                    signal      AXI_RD_bus_in_i     : in  t_AXI_RD_bus_in;
                                    signal      AXI_RD_bus_out_o    : out t_AXI_RD_bus_out;
                                    signal      clk_i               : in  std_logic;
                                    error_o     : out integer
                                ) is    
            variable v_AXI_RD_data : std_logic_vector(c_REG_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
            variable v_funct_sel   : integer := 0;
            variable v_error       : integer := 0;
        begin
            --=======================================================================================================================================
            -- Initializing main FGC registers
            --=======================================================================================================================================
            AXI_WRITE(C_Reg_fgc_control,c_DEFAULT_CTRL_REG,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register

            -- control register readback test
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(unsigned(v_AXI_RD_data) /= c_DEFAULT_CTRL_REG)then
                report "Control register Init test failed!"  severity ERROR ;
                v_error := v_error +1;
            end if;
            
            -- Enable faults detection
            AXI_WRITE(C_Reg_fgc_faultsEnable,c_ENABLE_FAUTLS,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- fault enable register / set all enable to '1'
            
            -- configure all functions
            for F in 0 to c_FGC_CHANNELS-1 loop
                v_funct_sel := F*8192;
                wait for 10ns;
                -- select the function
                AXI_WRITE(C_Reg_fgc_functSel,v_funct_sel,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function selection register
                wait for 100ns;
                AXI_WRITE(C_Reg_fgc_functConfig,c_DEFAULT_FUNCT_CONFIG,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function config register
                wait for 10ns;
            end loop;
            
            --dummy register access to change the bus address value
            AXI_WRITE(C_Reg_fgc_faultsEnable,c_ENABLE_FAUTLS,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
            
            -- read value of function configuration register
            AXI_READ(C_Reg_fgc_functConfig,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            
            if(unsigned(v_AXI_RD_data) /= c_DEFAULT_FUNCT_CONFIG)then
                report "Control register Init test failed!"  severity ERROR ;
                v_error := v_error +1;
            end if;
            
            --=======================================================================================================================================
            -- test 1: empty fault detection
            -- start trigger becomes before prepare trigger
            --=======================================================================================================================================
            
            wait until clk_i = '1';
            start_trig_o <= (others => '1');
            wait until clk_i = '1';
            start_trig_o <= (others => '0');
            
            wait for 10 us;
            
            -- testing register and interlock status
            AXI_READ(C_Reg_fgc_faults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(v_AXI_RD_data(c_FIFO_EMPTY_FAULT) = '1' and interlock_i = '1')then
                AXI_READ(C_Reg_fgc_fifoFaults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
                if(v_AXI_RD_data(c_FGC_CHANNELS-1 downto 0) = c_ALL_ONE)then
                    report "FIFO empty fault correctly generated!"  severity NOTE ;
                else
                    report "FIFO empty fault not correctly generated!"  severity ERROR ; 
                    v_error := v_error +1;
                end if;
            else    
                report "FIFO empty fault not correctly generated!"  severity ERROR ; 
                v_error := v_error +1;
            end if;
            
            wait for 10 us;
            
            -- testing clear faults mechanism
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            -- setting clr faults bit to 1
            v_AXI_RD_data(c_CLRFAULTS_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- fault enable register / set all enable to '1'
            wait for 10 us;
            
            -- register bits must be cleared after clr faults was asserted
            AXI_READ(C_Reg_fgc_faults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(v_AXI_RD_data(c_FIFO_EMPTY_FAULT) = '0' and interlock_i = '0')then
                AXI_READ(C_Reg_fgc_fifoFaults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
                if(v_AXI_RD_data(c_FGC_CHANNELS-1 downto 0) = c_ALL_ZERO)then
                    report "fault clearing success!"  severity NOTE ;
                else
                    report "fault clearing failed!"  severity ERROR ; 
                    v_error := v_error +1;
                end if;
            else    
                report "fault clearing failed!"  severity ERROR ; 
                v_error := v_error +1;
            end if;
            
            --=======================================================================================================================================
            -- test 2: coast and function memory not ready 
            -- prepare trigger and coast trim generated before memory ready bits were asserted
            --=======================================================================================================================================
            wait until clk_i = '1';
            prepare_trig_o      <= '1';
            coast_trim_trig_o   <= '1';
            wait until clk_i = '1';
            prepare_trig_o      <= '0';
            coast_trim_trig_o   <= '0';
            
            wait for 10 us;
            
            AXI_READ(C_Reg_fgc_faults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(v_AXI_RD_data(c_FUNCT_MEM_READY_FAULT) = '1' and v_AXI_RD_data(c_COAST_MEM_READY_FAULT) = '1' and interlock_i = '1')then
                report "function and coast memory ready faults correctly generated!"  severity NOTE ;
            else    
                report "function and coast memory ready faults generation failed!"  severity ERROR;
            end if;
            
            wait for 10 us;
            
            -- testing clear faults mechanism
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            -- setting clr faults bit to 1
            v_AXI_RD_data(c_CLRFAULTS_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- fault enable register / set all enable to '1'
            wait for 10 us;
            
            -- register bits must be cleared after clr faults was asserted
            AXI_READ(C_Reg_fgc_faults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(v_AXI_RD_data(c_FUNCT_MEM_READY_FAULT) = '0' and v_AXI_RD_data(c_COAST_MEM_READY_FAULT) = '0' and interlock_i = '0')then
                report "fault clearing success!"  severity NOTE ;
            else    
                report "fault clearing failed!"  severity ERROR ; 
                v_error := v_error +1;
            end if;
            
            --=======================================================================================================================================
            -- test 3: testing function output saturation fault
            -- using function 0 of cycle 1 (function with wrapping)
            -- setting function 0 with wrapping mode disable 
            --=======================================================================================================================================

            -- reset all functions
            for F in 0 to c_FGC_CHANNELS-1 loop
                v_funct_sel := F*8192;
                wait for 10ns;
                -- select the function
                AXI_WRITE(C_Reg_fgc_functSel,v_funct_sel,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function selection register
                wait for 100ns;
                -- for the others functions, set the same configuration 
                AXI_WRITE(C_Reg_fgc_functConfig,16#0000#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function config register
                wait for 10ns;
            end loop;
            
            -- configure function 0
            AXI_WRITE(C_Reg_fgc_functSel,c_FUNCTION_TESTING*8192,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                                       -- function selection register
            AXI_WRITE(C_Reg_fgc_functConfig,c_DEFAULT_FUNCT_CONFIG,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function config register
            AXI_WRITE(C_Reg_fgc_offlineStartValue,function_data_i.start_value(c_CYCLE_SEL_FOR_COAST,c_FUNCTION_TESTING),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- function start value register
            AXI_WRITE(C_Reg_fgc_functionSize,c_VECTOR_NUMBER,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                                 -- function size register
            
            -- set the memory address where to read the function data 
            AXI_WRITE(C_Reg_fgc_bufferStartAddress,c_CYCLE_SEL_FOR_COAST*c_FUNCT_BUFFER_SIZE*c_FGC_CHANNELS,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);  -- buffer start address register
            
            -- set funct mem ready and coast trim mem ready
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            -- setting clr faults bit to 1
            v_AXI_RD_data(c_CLRFAULTS_BIT_FIELD)     := '1';
            v_AXI_RD_data(c_COASTMEMREADY_BIT_FIELD) := '1';
            v_AXI_RD_data(c_FUNCTMEMREADY_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register

            wait until clk_i = '1';
            prepare_trig_o      <= '1';
            wait until clk_i = '1';
            prepare_trig_o      <= '0';
            
            v_AXI_RD_data := (others => '0');
            
            -- wait that fifo preloading is finished 
            while (v_AXI_RD_data(14) = '0')loop
                AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);   -- status register
                wait for 1us;
            end loop;
           
            -- start function playing
            wait until clk_i = '1';
            start_trig_o(c_FUNCTION_TESTING) <= '1';
            wait until clk_i = '1';
            start_trig_o <= (others => '0');

            -- wait on interlock 
            wait until interlock_i = '1';
            
            AXI_READ(C_Reg_fgc_faults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(v_AXI_RD_data(c_ACC_SAT_FAULT) = '1')then
                report "saturation fault correctly generated!"  severity NOTE ;
            else    
                report "saturation fault generation failed!"  severity ERROR;
                v_error := v_error +1;
            end if;

            --reset function 0
            AXI_WRITE(C_Reg_fgc_functConfig,16#0004#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function config register

            -- testing clear faults mechanism
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            -- setting clr faults bit to 1
            v_AXI_RD_data(c_CLRFAULTS_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- fault enable register / set all enable to '1'
            wait for 10 us;

            AXI_READ(C_Reg_fgc_faults,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(v_AXI_RD_data(c_ACC_SAT_FAULT) = '0' and interlock_i = '0')then
                    report "fault clearing success!"  severity NOTE ;
            else    
                report "fault clearing failed!"  severity ERROR ; 
                v_error := v_error +1;
            end if;

            error_o := v_error;

        end FAULTS_TESTER;
        
        --=====================================================================================
        --  functions testing on multiple cycles 
        --=====================================================================================
        procedure FUNCTIONS_TESTER
        (    
            signal      funct_busy_i        : in  std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      start_trig_o        : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o      : out std_logic;
            signal      inter_disable_o     : out std_logic;
            signal      cycle_cnt_o         : out integer;
            signal      function_data_i     : in  t_funct_data;
            signal      AXI_WR_bus_in_i     : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o    : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i     : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o    : out t_AXI_RD_bus_out;
            signal      clk_i               : in  std_logic;
            signal      m_axi_clk_i         : in  std_logic;
            error_o     : out integer
        ) is
        
            variable v_AXI_RD_data                      : std_logic_vector(c_REG_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
            variable v_funct_sel                        : integer := 0;
            variable v_buff_sel_status                  : std_logic := '0';
            variable v_buffer_selection_status_error    : integer := 0;
            variable v_cycle_cnt                        : integer := 0;
        begin
        
        --------------------------------------------------------------------------------------------------------
        -- Init control register
        AXI_WRITE(C_Reg_fgc_control,c_DEFAULT_CTRL_REG,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register
        --------------------------------------------------------------------------------------------------------
        -- control register readback test
        AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
        if(unsigned(v_AXI_RD_data) /= c_DEFAULT_CTRL_REG)then
            report "Control register Init test failed!"  severity FAILURE ;
        end if;
        --------------------------------------------------------------------------------------------------------
        -- for each cycle, configure the function registers
        for C in 0 to c_CYCLES-1 loop
            for F in 0 to c_FGC_CHANNELS-1 loop
                v_funct_sel := F*8192;
                wait for 10ns;
                -- select the function
                AXI_WRITE(C_Reg_fgc_functSel,v_funct_sel,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                           -- function selection register
                wait for 100ns;
                AXI_WRITE(C_Reg_fgc_offlineStartValue,function_data_i.start_value(C,F),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);    -- function start value register
                AXI_WRITE(C_Reg_fgc_functionSize,c_VECTOR_NUMBER,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                 -- function size register
                
                -- for function 0, switch between interpolation disable mode and wrapping mode
                if(F = c_FUNCTION_TESTING)then
                    if((C mod 2) = 0)then
                        -- set in interpolation mode
                        inter_disable_o <= '1';
                        AXI_WRITE(C_Reg_fgc_functConfig,16#18004#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);          -- function config register
                    else
                        -- set in wrapping mode
                        inter_disable_o <= '0';
                        AXI_WRITE(C_Reg_fgc_functConfig,16#28004#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);          -- function config register
                    end if;
                else  
                -- for the others functions, set the same configuration 
                    AXI_WRITE(C_Reg_fgc_functConfig,16#8004#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);              -- function config register
                end if;
                wait for 10ns;
            end loop;
            
            -- set funct mem ready 
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            v_AXI_RD_data(c_FUNCTMEMREADY_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
            
            -- read buffer selection status
            AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i); -- control register
            v_buff_sel_status := v_AXI_RD_data(10);
            --AXI_WRITE(C_Reg_fgc_control,16#1330A0#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register

            -- set the memory address where to read the function data 
            AXI_WRITE(C_Reg_fgc_bufferStartAddress,C*c_FUNCT_BUFFER_SIZE*c_FGC_CHANNELS,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- buffer start address register
            

            wait for 5us;
            --------------------------------------------------------------------------------------------------------
            -- generate prepare trigger
            wait until m_axi_clk_i = '1';
            prepare_trig_o <= '1';
            wait until m_axi_clk_i = '1';
            prepare_trig_o <= '0';
            
            -- Ckeck that buffer selection status bit has changed after function memory ready bit was set '1'
            AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i); -- control register
            if(v_buff_sel_status = v_AXI_RD_data(10))then
                report "Buffer selection status not changed!"  severity FAILURE ;
                v_buffer_selection_status_error := v_buffer_selection_status_error + 1;
            end if;
            --------------------------------------------------------------------------------------------------------
             -- wait that fifo preloading is finished 
             v_AXI_RD_data := (others => '0');
            while (v_AXI_RD_data(c_PRELOADING_FINISHED_BIT_FIELD) = '0')loop
                AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);   -- status register
                wait for 1us;
            end loop;
            --------------------------------------------------------------------------------------------------------
            -- generate start trigger
            wait until clk_i = '1';
            start_trig_o <= (others => '1');
            wait until clk_i = '1';
            start_trig_o <= (others => '0');
            --------------------------------------------------------------------------------------------------------
            -- function generation finished
            wait until unsigned(funct_busy_i) = 0;
            wait for 1ms;
            --------------------------------------------------------------------------------------------------------
            -- next cyle
            v_cycle_cnt := v_cycle_cnt + 1 ;
            cycle_cnt_o <= v_cycle_cnt;
        end loop;
        
        end procedure FUNCTIONS_TESTER;
        
        --=====================================================================================
        --  coast trim testing
        --=====================================================================================
        procedure COAST_TRIM_TESTER
        (    
            signal      start_trig_o                : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o              : out std_logic;
            signal      coast_trim_trig_o           : out std_logic;
            signal      coast_start_o               : out std_logic;
            signal      coast_stop_o                : out std_logic;
            signal      coast_mode_cycle_o          : out std_logic;
            signal      function_data_i             : in  t_funct_data;
            signal      AXI_WR_bus_in_i             : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o            : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i             : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o            : out t_AXI_RD_bus_out;
            signal      clk_i                       : in  std_logic;
            signal      m_axi_clk_i                 : in  std_logic;
            signal      coast_trim_cnt_o            : out integer;
            signal      coast_vector_i              : in  t_coast_table_array;
            
            coast_loading_error_o                   : out integer
        ) is
        
            variable v_AXI_RD_data                      : std_logic_vector(c_REG_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
            variable v_funct_sel                        : integer := 0;
            variable v_buff_sel_status                  : std_logic := '0';
            variable v_buffer_selection_status_error    : integer := 0;
            variable v_coast_trim_cnt                   : integer := 0;
            variable v_coast_buff_sel                   : integer := 0;
            variable v_error                            : integer := 0;
            variable v_error_sum                        : integer := 0;
        begin
            v_error             := 0;
            v_error_sum         := 0;
            v_coast_buff_sel    := 0;
            -- setting up the function registers for the coast mode
            -- functions of cycle 0 are used 
            for F in 0 to c_FGC_CHANNELS-1 loop
                v_funct_sel := F*8192;
                wait for 10ns;
                AXI_WRITE(C_Reg_fgc_functSel,v_funct_sel,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                   -- function selection register
                wait for 100ns;
                AXI_WRITE(C_Reg_fgc_offlineStartValue,function_data_i.start_value(0,F),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function start value register
                AXI_WRITE(C_Reg_fgc_functionSize,c_VECTOR_NUMBER,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);         -- function size register
                AXI_WRITE(C_Reg_fgc_functConfig,16#48004#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                  -- function config register
                wait for 10ns;
            end loop;
            
            -- set the memory address where to read the function data 
            AXI_WRITE(C_Reg_fgc_bufferStartAddress,0,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- buffer start address register
            
            -- set funct mem ready 
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            v_AXI_RD_data(c_FUNCTMEMREADY_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
            --AXI_WRITE(C_Reg_fgc_control,16#1330A0#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register
            wait for 5us;
            
            --------------------------------------------------------------------------------------------------------
            -- generate prepare trigger
            wait until m_axi_clk_i = '1';
            prepare_trig_o <= '1';
            wait until m_axi_clk_i = '1';
            prepare_trig_o <= '0';
            
           -- wait that fifo preloading is finished 
           v_AXI_RD_data := (others => '0');
           while (v_AXI_RD_data(14) = '0')loop
               AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);   -- status register
               wait for 1us;
           end loop;
           
            coast_mode_cycle_o <= '1';
            
            -- generate start trigger
            wait until clk_i = '1';
            start_trig_o <= (others => '1');
            wait until clk_i = '1';
            start_trig_o <= (others => '0');
            --------------------------------------------------------------------------------------------------------
            wait for 400us;
            
            -- enter in coast mode
            wait until clk_i = '1';
            coast_start_o <= '1';
            wait until clk_i = '1';
            coast_start_o <= '0';
    
            --------------------------------------------------------------------------------------------------------
            wait for 40us;
            ------------------------------------------------------------------------------------------------------
            -- generate coast trim trigger
            for I in 0 to c_COAST_TRIM_NUMBER-1 loop
                COAST_MEMORY_LOADING(coast_vector_i,AXI_WR_bus_in_i,AXI_WR_bus_out_o,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i,v_coast_buff_sel,v_error);
                v_error_sum := v_error_sum + v_error;
                
                if(v_coast_buff_sel = 0)then
                    v_coast_buff_sel := 1;
                else
                    v_coast_buff_sel := 0;
                end if;
                
                -- set coast memory ready bit
                AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
                v_AXI_RD_data(c_COASTMEMREADY_BIT_FIELD) := '1';
                AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
                --AXI_WRITE(C_Reg_fgc_control,16#1328A0#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register
            
                wait for 1ms;    
                
                -- generate coast trim trigger
                wait until clk_i = '1';
                coast_trim_trig_o <= '1';
                wait until clk_i = '1';
                coast_trim_trig_o <= '0';
                
                wait for 1ms;   
                v_coast_trim_cnt := v_coast_trim_cnt + 1;
                coast_trim_cnt_o <= v_coast_trim_cnt;
            end loop;
            -------------------------------------------------------------------------------------------------------- 
            -- stop coast mode
            wait until clk_i = '1';
            coast_stop_o <= '1';
            wait until clk_i = '1';
            coast_stop_o <= '0';
        
            coast_loading_error_o := v_error_sum;
        end procedure COAST_TRIM_TESTER;
        
        --=====================================================================================
        --  special vector testing on function 0 during cycle 0
        --=====================================================================================
        procedure SPECIAL_VECTOR_TESTER
        (    
            signal      start_trig_o                : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      prepare_trig_o              : out std_logic;
            signal      restart_trig_o              : out std_logic_vector(c_FGC_CHANNELS-1 downto 0);
            signal      special_vector_detected_i   : in std_logic;
            signal      function_data_i             : in  t_funct_data;
            signal      AXI_WR_bus_in_i             : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o            : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i             : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o            : out t_AXI_RD_bus_out;
            signal      clk_i                       : in  std_logic;
            signal      m_axi_clk_i                 : in  std_logic
        ) is
        
            variable v_AXI_RD_data                      : std_logic_vector(c_REG_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
            variable v_buff_sel_status                  : std_logic := '0';
            variable v_buffer_selection_status_error    : integer := 0;
            variable v_funct_sel                        : integer := 0;
        begin
            -- reset all functions
            for F in 0 to c_FGC_CHANNELS-1 loop
                v_funct_sel := F*8192;
                wait for 10ns;
                -- select the function
                AXI_WRITE(C_Reg_fgc_functSel,v_funct_sel,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function selection register
                wait for 100ns;
                -- for the others functions, set the same configuration 
                AXI_WRITE(C_Reg_fgc_functConfig,16#0000#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i); -- function config register
                wait for 10ns;
            end loop;
            --------------------------------------------------------------------------------------------------------
            -- Init control register
            AXI_WRITE(C_Reg_fgc_control,c_DEFAULT_CTRL_REG,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register
            --------------------------------------------------------------------------------------------------------
            -- control register readback test
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            if(unsigned(v_AXI_RD_data) /= c_DEFAULT_CTRL_REG)then
                report "Control register Init test failed!"  severity FAILURE ;
            end if;
            --------------------------------------------------------------------------------------------------------
            -- for each cycle, configure the function registers
            -- select the function and cycle 

            -- select the function
            AXI_WRITE(C_Reg_fgc_functSel,c_FUNCTION_TESTING*8192,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                                      -- function selection
            AXI_WRITE(C_Reg_fgc_offlineStartValue,function_data_i.start_value(c_CYCLE_SEL_FOR_SPECIAL_VECTOR,c_FUNCTION_TESTING),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);    -- function start value register
            AXI_WRITE(C_Reg_fgc_functionSize,c_VECTOR_NUMBER+1,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);                              -- function size register + the special vector
            
            -- enable and coast enable set to '1'
            AXI_WRITE(C_Reg_fgc_functConfig,16#48014#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);           
    
            -- set the memory address where to read the function data 
            AXI_WRITE(C_Reg_fgc_bufferStartAddress,c_CYCLES*c_FUNCT_BUFFER_SIZE*c_FGC_CHANNELS,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- loaction of the function with the special vector
            
            -- read buffer selection status
            AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i); -- control register
            v_buff_sel_status := v_AXI_RD_data(10);
            
            -- set funct mem ready                 
            AXI_READ(C_Reg_fgc_control,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);-- control register
            v_AXI_RD_data(c_FUNCTMEMREADY_BIT_FIELD) := '1';
            AXI_WRITE(C_Reg_fgc_control,to_integer(unsigned(v_AXI_RD_data)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
            --AXI_WRITE(C_Reg_fgc_control,16#1330A0#,AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);     -- control register
    
            wait for 5us;
            --------------------------------------------------------------------------------------------------------
            -- generate prepare trigger
            wait until m_axi_clk_i = '1';
            prepare_trig_o <= '1';
            wait until m_axi_clk_i = '1';
            prepare_trig_o <= '0';
            
            -- Ckeck that buffer selection status bit has changed after function memory ready bit was set '1'
            AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i); -- control register
            if(v_buff_sel_status = v_AXI_RD_data(10))then
                report "Buffer selection status not changed!"  severity FAILURE ;
                v_buffer_selection_status_error := v_buffer_selection_status_error + 1;
            end if;
            --------------------------------------------------------------------------------------------------------
            -- wait that fifo preloading is finished 
            v_AXI_RD_data := (others => '0');
            while (v_AXI_RD_data(14) = '0')loop
               AXI_READ(C_Reg_fgc_status,v_AXI_RD_data,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);   -- status register
               wait for 1us;
            end loop;
            --------------------------------------------------------------------------------------------------------
            -- generate start trigger
            wait until clk_i = '1';
            start_trig_o <= (others => '1');
            wait until clk_i = '1';
            start_trig_o <= (others => '0');
            -- wait that the coast mode started 
            wait until special_vector_detected_i = '1';
            --------------------------------------------------------------------------------------------------------
            wait for 2ms;
            --------------------------------------------------------------------------------------------------------
            -- generate start trigger
            wait until clk_i = '1';
            restart_trig_o <= (others => '1');
            wait until clk_i = '1';
            restart_trig_o <= (others => '0');
        
        end procedure SPECIAL_VECTOR_TESTER;
        
        --=====================================================================================
        --  coast memory loading
        --=====================================================================================
                
        procedure COAST_MEMORY_LOADING
        (
            signal      coast_vector_i              : t_coast_table_array;
            signal      AXI_WR_bus_in_i             : in  t_AXI_WR_bus_in;
            signal      AXI_WR_bus_out_o            : out t_AXI_WR_bus_out;
            signal      AXI_RD_bus_in_i             : in  t_AXI_RD_bus_in;
            signal      AXI_RD_bus_out_o            : out t_AXI_RD_bus_out;
            signal      clk_i                       : in  std_logic;
            
            coast_buff_sel_i                        : in  integer;
            error_o                                 : out integer
        ) is 
            variable v_coast_loading_error : integer := 0;
            variable v_coast_AXI_rddata    : t_vector_format;
            variable v_coast_AXI_wrdata    : t_vector_format;
            variable v_addr                : integer := 0;
            variable v_index               : integer := 0;
            variable v_error               : integer := 0;
        begin
            v_addr  := 0;
            v_error := 0;
            v_index := 0;
            -- write coast vector in the memory
            -- WARNING! number of coast + 1 end vector
            for I in 0 to c_COAST_VECTOR_NUMBER loop   
                v_coast_AXI_wrdata.slope       :=  coast_vector_i(coast_buff_sel_i)(I).slope;
                v_coast_AXI_wrdata.interval    :=  coast_vector_i(coast_buff_sel_i)(I).interval;            
                
                -- depends of the number of function
                for j in 0 to (c_FGC_CHANNELS-1) loop
                    -- AXI write to memory
                    v_addr := (j*c_COAST_BUFFER_DEPTH_32BIT+v_index+to_integer(unsigned(C_Mem_fgc_coastMemory_Sta)))*4;
                    AXI_WRITE(std_logic_vector(to_unsigned(v_addr,32)),to_integer(unsigned(v_coast_AXI_wrdata.slope)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
                    AXI_WRITE(std_logic_vector(to_unsigned(v_addr+4,32)),to_integer(unsigned(v_coast_AXI_wrdata.interval)),AXI_WR_bus_in_i,AXI_WR_bus_out_o,clk_i);
                    wait for 100ns;
                    -- AXI read to memory
                    AXI_READ(std_logic_vector(to_unsigned(v_addr,32)),v_coast_AXI_rddata.slope,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);     
                    AXI_READ(std_logic_vector(to_unsigned(v_addr+4,32)),v_coast_AXI_rddata.interval,AXI_RD_bus_in_i,AXI_RD_bus_out_o,clk_i);
                    
                    -- verify the content of the memory
                    if(v_coast_AXI_wrdata /= v_coast_AXI_rddata)then
                        report "Coast memory init failed!"  severity ERROR ;
                        -- coount number of error
                        v_error := v_error + 1;
                    end if;        
                end loop;
                v_index := v_index + 2;
            end loop ;
            
            error_o := v_error;
        end procedure COAST_MEMORY_LOADING;
        
        
        --=====================================================================================
        --  read the file containing the data of the functions 
        --=====================================================================================
        function FUNCT_FILE_READ(filename : string) return t_funct_data is
            variable v_ILINE                   : line;
            variable v_SPACE                   : character;
            variable v_index                   : integer := 0;  
            variable v_max_length              : integer := 0;   
            variable v_funct_data              : t_funct_data;
            file     file_VECTORS              : text;
        begin
            report ">>Start reading " & filename & " file...";
            
            -- read function_data.txt file from matlab script
            file_open(file_VECTORS, filename,  read_mode);  
            
            --read functions for each cycle
            for C in 0 to c_CYCLES-1 loop
                -- cycle number text line
                readline(file_VECTORS, v_ILINE); 
                
                -- function max length text ling
                readline(file_VECTORS, v_ILINE); 
                
                -- function max length
                readline(file_VECTORS, v_ILINE); 
                read(v_ILINE, v_max_length);
                
                -- function length text line
                readline(file_VECTORS, v_ILINE); 
                
                --read function length for each function
                readline(file_VECTORS, v_ILINE);
                for F in 0 to (c_FGC_CHANNELS-1) loop 
                    read(v_ILINE, v_funct_data.length(C,F));
                    -- read in the space character
                    read(v_ILINE, v_SPACE);                 
                end loop;
                
                -- start value text line
                readline(file_VECTORS, v_ILINE); 
                
                --read function start value for each function
                readline(file_VECTORS, v_ILINE);
                for F in 0 to (c_FGC_CHANNELS-1) loop 
                    read(v_ILINE, v_funct_data.start_value(C,F));
                    -- read in the space character
                    read(v_ILINE, v_SPACE);                     
                end loop;
                
                -- function text line
                readline(file_VECTORS, v_ILINE); 
                
                -- read function contents
                for L in 0 to v_max_length-1 loop
                    readline(file_VECTORS, v_ILINE);
                    for F in 0 to (c_FGC_CHANNELS-1) loop 
                        read(v_ILINE, v_funct_data.data(C,F,L));
                        -- read in the space character
                        read(v_ILINE, v_SPACE);       
                    end loop;
                end loop;
            end loop;
            
            -- close file
            file_close(file_VECTORS);

            report ">>Finished reading " & filename & " file...";
            
            return v_funct_data;
        end FUNCT_FILE_READ;

        --=====================================================================================
        --  read the file containing the vector of the coast trims 
        --=====================================================================================
        function COAST_VECTOR_FILE_READ(filename : string) return t_coast_data_file_array is 
            variable v_ILINE               : line;
            variable v_SPACE               : character;
            variable v_coast_slope         : integer := 0;
            variable v_coast_interval      : integer := 0;
            variable v_coast_vector_data   : t_coast_data_file_array;
            file     file_COAST_VECTORS    : text;
        begin                
            report ">>Start reading " & filename & " file...";
            
            file_open(file_COAST_VECTORS, filename,  read_mode); 

             -- read coast slope and interval for first coast trim
             loop_1:for I in 0 to (c_COAST_BUFFER_DEPTH_64BIT-1) loop 
                 readline(file_COAST_VECTORS, v_ILINE);
                 -- read coast slope
                 read(v_ILINE, v_coast_slope);
                 -- read in the space character
                 read(v_ILINE, v_SPACE);     
                 -- read coast interval           
                 read(v_ILINE, v_coast_interval);
                 
                 -- fill the table with the slopes and intervals
                 v_coast_vector_data(I).slope       := std_logic_vector(to_signed(v_coast_slope,32));                               
                 v_coast_vector_data(I).interval    := std_logic_vector(to_unsigned(v_coast_interval,32));
                 
                 -- stop when end coast vector was reached
                 exit loop_1 when v_coast_interval = -1;
             end loop loop_1;
             
             -- close file
             file_close(file_COAST_VECTORS);
             
             report ">>Finished reading " & filename & " file...";
             
             return v_coast_vector_data;
        end COAST_VECTOR_FILE_READ;
        
        --=====================================================================================
        --  read the file containing the data of the coast trims 
        --=====================================================================================
        function COAST_DATA_FILE_READ(filename : string) return t_coast_data is
            variable v_ILINE               : line;
            variable v_SPACE               : character;
            variable v_coast_data          : t_coast_data;
            file     file_COAST_VECTORS    : text;
        begin 
           report ">>Start reading " & filename & " file...";
           -- open coast_trim_data.txt file from matlab script
           file_open(file_COAST_VECTORS, filename,  read_mode); 
           for c in 0 to (c_COAST_TRIM_NUMBER-1) loop 
              -- coast trim length text line
               readline(file_COAST_VECTORS, v_ILINE); 
               
               -- read coast trim length (in vectors)
               readline(file_COAST_VECTORS, v_ILINE);
               read(v_ILINE, v_coast_data.length(c));
               
               -- coast trim value text line
               readline(file_COAST_VECTORS, v_ILINE); 
               
               -- read the coast trim data (points)
               for I in 0 to v_coast_data.length(c)-1 loop
                  readline(file_COAST_VECTORS, v_ILINE);
                  read(v_ILINE, v_coast_data.data(c,I));
               end loop;
           end loop;
           
           --close file
           file_close(file_COAST_VECTORS);
           report ">>Finished reading " & filename & " file...";
           
           return v_coast_data;
        end COAST_DATA_FILE_READ;

end FGC_SIM_PACK;