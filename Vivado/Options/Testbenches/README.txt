# FGC project
# Testbenches folder

# Matlab script
1) 	the FunctionGenerator.m script generates the following files required for the simulation:
	- function_data.txt
	- coast_trim_data.txt
	- coast_trim_vector_data_0.txt
	- coast_trim_vector_data_1.txt
	- function_vector_data.coe
	
	WARNING! You must regenerate the functions_memory ip core sources before start the simulation
	
2) 	The simulation parameters must be set in the matlab script under "constant definition"

3) 	The script will create N functions for M cycles (configured by the simulation parameters) and will display the functions in the "Functions Waveform figure" after script execution. 
	It will also generate coast trim waveform for the coast mode testing (displayed in the "Coast Trim Waveform" figure).

Note: the function 0 is used to test the wrapping mode and the cartesian coordinate format.