-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, sim_tb_FGC --
-- --
-------------------------------------------------------------------------------
--
-- unit name: sim_tb_FGC
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 09/07/2018
--
-- version: 1.0
--
-- description: FGC simulation testbench
--
-- dependencies: FunctionGenerator.m
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

use ieee.math_real.all;

library std;
use std.env.stop;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;
use work.FGC_SIM_PACK.all;
use work.MemMap_fgc.all;
use work.MemMap_ipInfo.all;

entity sim_tb_FGC is
end sim_tb_FGC;

architecture testbench of sim_tb_FGC is
    --=============================================================================
    -- constant instantation 
    -- clock frequency
    constant c_CLK_PERIOD               : time      := 16ns;     --62.5MHz
    constant c_M_AXI_CLK_PERIOD         : time      := 5ns;      --200MHz
    constant c_INTERPOL_VALUE           : integer   := 10;       --interpol_clk_frequ = clk_freq/interpol_value
    -- reset time
    constant c_RESET_TIME       : time := 200ns;

    --=============================================================================
    -- type instantation
    type t_SIM_STATUS is (IDLE,FAULTS_TESTER,FUNCTIONS_TESTER,COAST_TRIM_TESTER,SPECIAL_VECTOR_TESTER);
    signal SIM_STATUS : t_SIM_STATUS;
    --=============================================================================
    -- signals instantation 
    -- timing
    signal s_clk_freq                   : time;
    signal s_axi_clk_freq               : time;
    signal s_intepolat_clk_freq         : time;
    
    -- general
    signal s_clk                        : std_logic := '0';
    signal s_m_axi_clk                  : std_logic := '0';
    signal s_reset                      : std_logic := '0';
    signal s_reset_n                    : std_logic := '1';
    signal s_inter_clk_en               : std_logic := '0';
    signal s_axi_traff_gen_reset_n      : std_logic := '1';
    signal s_m_axi_reset                : std_logic := '0';
    signal s_buffer_sel                 : std_logic := '0';
    signal s_coast_buff_sel             : integer := 0;
    signal s_funct_sel                  : integer := 0;
    signal s_cycle_cnt                  : integer := 0;
    signal s_interlock                  : std_logic := '0';
    signal s_axi_traffic_gen_done       : std_logic := '0';
    signal s_coast_memory_init_done     : std_logic := '0';
    signal s_buff_sel_status            : std_logic := '0';
    signal s_load_coast_memory          : std_logic := '0';
    signal s_funct_busy                 : std_logic_vector(c_MAX_FUNCTIONS-1 downto 0) := (others => '0');
    signal s_funct_out                  : t_function_data_std_array := (others => (others => '0'));
    signal s_funct_out_cnt              : t_function_data_int_array;
    signal s_funct0_inter_disable       : std_logic := '0';
    signal s_coast_mode_cycle           : std_logic := '0';
    signal s_coast_trim_cnt             : integer := 0;
    signal s_testing_on                 : std_logic := '0';
    signal s_special_vector_detected    : std_logic := '0';
    signal s_special_vector_error       : integer := 0;
    
    -- error counters
    signal s_coast_loading_error    : integer := 0;
    type   t_funct_error_array      is array (0 to c_FGC_CHANNELS-1) of integer;
    signal s_funct_error            : t_funct_error_array;
    signal s_coast_error            : t_funct_error_array;
    
    -- external memory master interface
    signal s_mem_m_AXI_araddr   : std_logic_vector ( 31 downto 0 )  := (others => '0');
    signal s_mem_m_AXI_arburst  : std_logic_vector ( 1 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arcache  : std_logic_vector ( 3 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arid     : std_logic_vector ( 1 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arlen    : std_logic_vector ( 7 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arlock   : STD_LOGIC                         := '0';
    signal s_mem_m_AXI_arprot   : std_logic_vector ( 2 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arqos    : std_logic_vector ( 3 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arready  : STD_LOGIC                         := '0';
    signal s_mem_m_AXI_arsize   : std_logic_vector ( 2 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_aruser   : std_logic_vector ( 1 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_arvalid  : STD_LOGIC                         := '0';
    signal s_mem_m_AXI_rdata    : std_logic_vector ( c_MEM_AXI_DATA_WIDTH-1 downto 0 )  := (others => '0');
    signal s_mem_m_AXI_rid      : std_logic_vector ( 1 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_rlast    : STD_LOGIC                         := '0';
    signal s_mem_m_AXI_rready   : STD_LOGIC                         := '0';
    signal s_mem_m_AXI_rresp    : std_logic_vector ( 1 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_ruser    : std_logic_vector ( 1 downto 0 )   := (others => '0');
    signal s_mem_m_AXI_rvalid   : std_logic                         := '0';

    -- cheburashka slave interface
    signal s_ch_addr           : std_logic_vector(31 downto 0) := (others => '0');
    signal s_ch_rd_data        : std_logic_vector(31 downto 0) := (others => '0');
    signal s_ch_wr_data        : std_logic_vector(31 downto 0) := (others => '0');
    signal s_ch_rd_mem         : std_logic := '0'; 
    signal s_ch_wr_mem         : std_logic := '0'; 
    signal s_ch_rd_done        : std_logic := '0'; 
    signal s_ch_wr_done        : std_logic := '0'; 
    
    -- trigger inputs
    signal s_ext_trig_start_0_i                 : std_logic_vector(c_FGC_CHANNELS-1 downto 0) := (others => '0');
    signal s_ext_trig_start_1_i                 : std_logic_vector(c_FGC_CHANNELS-1 downto 0) := (others => '0');
    signal s_ext_trig_restart_0_i               : std_logic_vector(c_FGC_CHANNELS-1 downto 0) := (others => '0');
    signal s_ext_trig_restart_1_i               : std_logic_vector(c_FGC_CHANNELS-1 downto 0) := (others => '0');
    signal s_ext_trig_trim_0_i                  : std_logic := '0'; 
    signal s_ext_trig_trim_1_i                  : std_logic := '0'; 
    signal s_ext_trig_prepare_0_i               : std_logic := '0'; 
    signal s_ext_trig_prepare_1_i               : std_logic := '0'; 
    signal s_ext_trig_coast_start_0_i           : std_logic := '0'; 
    signal s_ext_trig_coast_start_1_i           : std_logic := '0'; 
    signal s_ext_trig_coast_stop_0_i            : std_logic := '0'; 
    signal s_ext_trig_coast_stop_1_i            : std_logic := '0'; 
    
    -- AXI interface
--    signal sendIt : std_logic := '0';
--    signal readIt : std_logic := '0';
    signal s_reg_AXI_WR_out     : t_AXI_WR_bus_out := (AWVALID =>'0',BREADY => '0',WLAST => '0',WVALID => '0',AWADDR => (others=>'0'),WDATA => (others=>'0'),WSTRB => (others=>'0'));
    signal s_reg_AXI_WR_in      : t_AXI_WR_bus_in := (AWREADY => '0',BVALID => '0',WREADY => '0',BRESP => (others => '0'));
    signal s_reg_AXI_RD_out     : t_AXI_RD_bus_out := (ARVALID=>'0',RREADY => '0',ARADDR => (others=>'0'));
    signal s_reg_AXI_RD_in      : t_AXI_RD_bus_in := (ARREADY => '0',RLAST => '0',RVALID => '0',RRESP => (others => '0'),RDATA => (others => '0'));
    
    --file signals
    signal s_function_data     : t_funct_data;
    signal s_coast_vector      : t_coast_table_array := (others => (others => (slope => (others => '0'),interval => (others => '0'))));
    signal s_coast_data        : t_coast_data;
    --=============================================================================
    component axi4lite_cheburashka_bridge
    port (
        ACLK    : in std_logic;
        ARESETN : in std_logic;
        
        ARVALID : in  std_logic;
        AWVALID : in  std_logic;
        BREADY  : in  std_logic;
        RREADY  : in  std_logic;
        WLAST   : in  std_logic := '1';
        WVALID  : in  std_logic;
        ARADDR  : in  std_logic_vector (31 downto 0);
        AWADDR  : in  std_logic_vector (31 downto 0);
        WDATA   : in  std_logic_vector (31 downto 0);
        WSTRB   : in  std_logic_vector (3 downto 0);
        ARREADY : out std_logic;
        AWREADY : out std_logic;
        BVALID  : out std_logic;
        RLAST   : out std_logic;
        RVALID  : out std_logic;
        WREADY  : out std_logic;
        BRESP   : out std_logic_vector (1 downto 0);
        RRESP   : out std_logic_vector (1 downto 0);
        RDATA   : out std_logic_vector (31 downto 0);
        
        ch_addr_o    : out std_logic_vector(31 downto 0);
        ch_rd_data_i : in  std_logic_vector(31 downto 0);
        ch_wr_data_o : out std_logic_vector(31 downto 0);
        ch_rd_mem_o  : out std_logic;
        ch_wr_mem_o  : out std_logic;
        ch_rd_done_i : in  std_logic;
        ch_wr_done_i : in  std_logic
    
    );
    end component;
    ----------------------------------------------------------------------

    component functions_memory
    port (
        rsta_busy : out std_logic;
        rstb_busy : out std_logic;
        s_aclk : in std_logic;
        s_aresetn : in std_logic;
        s_axi_awid : in std_logic_vector(1 downto 0);
        s_axi_awaddr : in std_logic_vector(31 downto 0);
        s_axi_awlen : in std_logic_vector(7 downto 0);
        s_axi_awsize : in std_logic_vector(2 downto 0);
        s_axi_awburst : in std_logic_vector(1 downto 0);
        s_axi_awvalid : in std_logic;
        s_axi_awready : out std_logic;
        s_axi_wdata : in std_logic_vector(63 downto 0);
        s_axi_wstrb : in std_logic_vector(7 downto 0);
        s_axi_wlast : in std_logic;
        s_axi_wvalid : in std_logic;
        s_axi_wready : out std_logic;
        s_axi_bid : out std_logic_vector(1 downto 0);
        s_axi_bresp : out std_logic_vector(1 downto 0);
        s_axi_bvalid : out std_logic;
        s_axi_bready : in std_logic;
        s_axi_arid : in std_logic_vector(1 downto 0);
        s_axi_araddr : in std_logic_vector(31 downto 0);
        s_axi_arlen : in std_logic_vector(7 downto 0);
        s_axi_arsize : in std_logic_vector(2 downto 0);
        s_axi_arburst : in std_logic_vector(1 downto 0);
        s_axi_arvalid : in std_logic;
        s_axi_arready : out std_logic;
        s_axi_rid : out std_logic_vector(1 downto 0);
        s_axi_rdata : out std_logic_vector(63 downto 0);
        s_axi_rresp : out std_logic_vector(1 downto 0);
        s_axi_rlast : out std_logic;
        s_axi_rvalid : out std_logic;
        s_axi_rready : in std_logic
    );
    end component;

    ----------------------------------------------------------------------
    
    component FGC is
    generic (
            -- function parameters
            g_FGC_CHANNELS            : integer := 4; 
            -- AXI read interface width
            g_M_AXI_DATA_WIDTH        : integer := 64;
            -- AXI read interface address width
            g_M_AXI_ADDR_WIDTH        : integer := 32;
            -- function 0 output width
            g_FUNCT_0_OUT_WIDTH       : integer := 16;
            -- function 1 output width
            g_FUNCT_1_OUT_WIDTH       : integer := 16;
            -- function 2 output width
            g_FUNCT_2_OUT_WIDTH       : integer := 16;
            -- function 3 output width
            g_FUNCT_3_OUT_WIDTH       : integer := 16;
            -- function 4 output width
            g_FUNCT_4_OUT_WIDTH       : integer := 16;
            -- function 5 output width
            g_FUNCT_5_OUT_WIDTH       : integer := 16;
            -- function 6 output width
            g_FUNCT_6_OUT_WIDTH       : integer := 16;
            -- function 7 output width
            g_FUNCT_7_OUT_WIDTH       : integer := 16;
            -- function 8 output width
            g_FUNCT_8_OUT_WIDTH       : integer := 16;
            -- function 9 output width
            g_FUNCT_9_OUT_WIDTH       : integer := 16;
            -- function 10 output width
            g_FUNCT_10_OUT_WIDTH       : integer := 16;
            -- function 11 output width
            g_FUNCT_11_OUT_WIDTH       : integer := 16;
            -- function 12 output width
            g_FUNCT_12_OUT_WIDTH       : integer := 16;
            -- function 13 output width
            g_FUNCT_13_OUT_WIDTH       : integer := 16;
            -- function 14 output width
            g_FUNCT_14_OUT_WIDTH       : integer := 16;
            -- function 15 output width
            g_FUNCT_15_OUT_WIDTH       : integer := 16;
            -- size of buffer in bytes (per function)
            g_BUFFER_SIZE             : integer   := 4096;
            -- interpolation value (interpolation frequency = input clk frequency/ interpolation value)
            g_INTERPOLATION_VALUE     : integer := 625; -- 625 => 62.5MHz/625 = 100000Hz => 10us  
            -- for simulation (1 = simulation mode)
            g_SIMULATION              : natural := 0 
    );
    port (
            -- external memory master interface
            mem_m_AXI_araddr : out STD_LOGIC_VECTOR ( g_M_AXI_ADDR_WIDTH-1 downto 0 );
            mem_m_AXI_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
            mem_m_AXI_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
            mem_m_AXI_arid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            mem_m_AXI_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
            mem_m_AXI_arlock : out STD_LOGIC;
            mem_m_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
            mem_m_AXI_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
            mem_m_AXI_arready : in STD_LOGIC;
            mem_m_AXI_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
            mem_m_AXI_aruser : out STD_LOGIC_VECTOR ( 1 downto 0 );
            mem_m_AXI_arvalid : out STD_LOGIC;
            mem_m_AXI_rdata : in STD_LOGIC_VECTOR ( g_M_AXI_DATA_WIDTH-1 downto 0 );
            mem_m_AXI_rid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            mem_m_AXI_rlast : in STD_LOGIC;
            mem_m_AXI_rready : out STD_LOGIC;
            mem_m_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
            mem_m_AXI_ruser : in STD_LOGIC_VECTOR ( 1 downto 0 );
            mem_m_AXI_rvalid : in STD_LOGIC;
            
            -- register slave interface
            cheb_Addr : in std_logic_vector(12 downto 2);
            cheb_RdData : out std_logic_vector(31 downto 0);
            cheb_WrData : in std_logic_vector(31 downto 0);
            cheb_RdMem : in std_logic;
            cheb_WrMem : in std_logic;
            cheb_RdDone : out std_logic;
            cheb_WrDone : out std_logic;
            cheb_RdError : out std_logic;
            cheb_WrError : out std_logic;
        
            -- function output
            funct_out_0_o   : out std_logic_vector(g_FUNCT_0_OUT_WIDTH-1 downto 0);
            funct_out_1_o   : out std_logic_vector(g_FUNCT_1_OUT_WIDTH-1 downto 0);
            funct_out_2_o   : out std_logic_vector(g_FUNCT_2_OUT_WIDTH-1 downto 0);
            funct_out_3_o   : out std_logic_vector(g_FUNCT_3_OUT_WIDTH-1 downto 0);
            funct_out_4_o   : out std_logic_vector(g_FUNCT_4_OUT_WIDTH-1 downto 0);
            funct_out_5_o   : out std_logic_vector(g_FUNCT_5_OUT_WIDTH-1 downto 0);
            funct_out_6_o   : out std_logic_vector(g_FUNCT_6_OUT_WIDTH-1 downto 0);
            funct_out_7_o   : out std_logic_vector(g_FUNCT_7_OUT_WIDTH-1 downto 0);
            funct_out_8_o   : out std_logic_vector(g_FUNCT_8_OUT_WIDTH-1 downto 0);
            funct_out_9_o   : out std_logic_vector(g_FUNCT_9_OUT_WIDTH-1 downto 0);
            funct_out_10_o  : out std_logic_vector(g_FUNCT_10_OUT_WIDTH-1 downto 0);
            funct_out_11_o  : out std_logic_vector(g_FUNCT_11_OUT_WIDTH-1 downto 0);
            funct_out_12_o  : out std_logic_vector(g_FUNCT_12_OUT_WIDTH-1 downto 0);
            funct_out_13_o  : out std_logic_vector(g_FUNCT_13_OUT_WIDTH-1 downto 0);
            funct_out_14_o  : out std_logic_vector(g_FUNCT_14_OUT_WIDTH-1 downto 0);
            funct_out_15_o  : out std_logic_vector(g_FUNCT_15_OUT_WIDTH-1 downto 0);
            
            funct_busy_0_o  : out std_logic;
            funct_busy_1_o  : out std_logic;
            funct_busy_2_o  : out std_logic;
            funct_busy_3_o  : out std_logic;
            funct_busy_4_o  : out std_logic;
            funct_busy_5_o  : out std_logic;
            funct_busy_6_o  : out std_logic;
            funct_busy_7_o  : out std_logic;
            funct_busy_8_o  : out std_logic;
            funct_busy_9_o  : out std_logic;
            funct_busy_10_o : out std_logic;
            funct_busy_11_o : out std_logic;
            funct_busy_12_o : out std_logic;
            funct_busy_13_o : out std_logic;
            funct_busy_14_o : out std_logic;
            funct_busy_15_o : out std_logic;
            
            -- trigger intputs
            ext_trig_start_0_i              : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            ext_trig_start_1_i              : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            ext_trig_restart_0_i            : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            ext_trig_restart_1_i            : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            ext_trig_trim_0_i               : in std_logic;
            ext_trig_trim_1_i               : in std_logic;
            ext_trig_prepare_0_i            : in std_logic;
            ext_trig_prepare_1_i            : in std_logic;
            ext_trig_coast_start_0_i        : in std_logic;
            ext_trig_coast_start_1_i        : in std_logic;
            ext_trig_coast_stop_0_i         : in std_logic;
            ext_trig_coast_stop_1_i         : in std_logic;
        
            -- status/control
            buffer_sel_o : out std_logic; 
            interlock_o  : out std_logic;
            
            --debug
            inter_clk_en_o  : out std_logic;
        
            clk_i           : in std_logic;
            reset_i         : in std_logic;
            m_axi_clk_i     : in std_logic;
            m_axi_reset_i   : in std_logic
    );
    end component FGC;
    
begin
    --=============================================================================
    --                          Port mapping
    --=============================================================================
    functions_memory_0: functions_memory
    port map(
        rsta_busy => open,
        rstb_busy => open,
        s_aclk => s_m_axi_clk,
        s_aresetn => s_reset_n,
        s_axi_awid => (others => '0'),
        s_axi_awaddr => (others => '0'),
        s_axi_awlen => (others => '0'),
        s_axi_awsize => (others => '0'),
        s_axi_awburst => (others => '0'),
        s_axi_awvalid => '0',
        s_axi_awready => open,
        s_axi_wdata => (others => '0'),
        s_axi_wstrb => (others => '0'),
        s_axi_wlast => '0',
        s_axi_wvalid => '0',
        s_axi_wready => open,
        s_axi_bid => open,
        s_axi_bresp => open,
        s_axi_bvalid => open,
        s_axi_bready => '0',
        s_axi_arid => s_mem_m_AXI_arid,
        s_axi_araddr => s_mem_m_AXI_araddr,
        s_axi_arlen => s_mem_m_AXI_arlen,
        s_axi_arsize => s_mem_m_AXI_arsize,
        s_axi_arburst => s_mem_m_AXI_arburst,
        s_axi_arvalid => s_mem_m_AXI_arvalid,
        s_axi_arready => s_mem_m_AXI_arready,
        s_axi_rid => s_mem_m_AXI_rid,
        s_axi_rdata => s_mem_m_AXI_rdata,
        s_axi_rresp => s_mem_m_AXI_rresp,
        s_axi_rlast => s_mem_m_AXI_rlast,
        s_axi_rvalid => s_mem_m_AXI_rvalid,
        s_axi_rready => s_mem_m_AXI_rready
    );

    -----------------------------------------------------------------------  
    u_FGC: FGC
    generic map(
        g_FGC_CHANNELS => c_FGC_CHANNELS,
        g_FUNCT_0_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_1_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_2_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_3_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_4_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_5_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_6_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_7_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_8_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_9_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_10_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_11_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_12_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_13_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_14_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_FUNCT_15_OUT_WIDTH => c_FUNCT_OUT_WIDTH,
        g_INTERPOLATION_VALUE => c_INTERPOL_VALUE,
        g_BUFFER_SIZE => c_FUNCT_BUFFER_SIZE
    )
    Port map( 
        -- external memory master interface
        mem_m_AXI_araddr => s_mem_m_AXI_araddr,
        mem_m_AXI_arburst => s_mem_m_AXI_arburst,
        mem_m_AXI_arcache => s_mem_m_AXI_arcache,
        mem_m_AXI_arid => s_mem_m_AXI_arid,
        mem_m_AXI_arlen => s_mem_m_AXI_arlen,
        mem_m_AXI_arlock => s_mem_m_AXI_arlock,
        mem_m_AXI_arprot => s_mem_m_AXI_arprot,
        mem_m_AXI_arqos => s_mem_m_AXI_arqos,
        mem_m_AXI_arready => s_mem_m_AXI_arready,
        mem_m_AXI_arsize => s_mem_m_AXI_arsize,
        mem_m_AXI_aruser => s_mem_m_AXI_aruser,
        mem_m_AXI_arvalid => s_mem_m_AXI_arvalid,
        mem_m_AXI_rdata => s_mem_m_AXI_rdata,
        mem_m_AXI_rid => s_mem_m_AXI_rid,
        mem_m_AXI_rlast => s_mem_m_AXI_rlast,
        mem_m_AXI_rready => s_mem_m_AXI_rready,
        mem_m_AXI_rresp => s_mem_m_AXI_rresp,
        mem_m_AXI_ruser => s_mem_m_AXI_ruser,
        mem_m_AXI_rvalid => s_mem_m_AXI_rvalid,
        
        -- register slave interface
        cheb_Addr => s_ch_addr(12 downto 2),
        cheb_RdData => s_ch_rd_data,
        cheb_WrData => s_ch_wr_data,
        cheb_RdMem => s_ch_rd_mem,
        cheb_WrMem => s_ch_wr_mem,
        cheb_RdDone => s_ch_rd_done,
        cheb_WrDone => s_ch_wr_done,

        -- function output
        funct_out_0_o   => s_funct_out(0),
        funct_out_1_o   => s_funct_out(1),
        funct_out_2_o   => s_funct_out(2),
        funct_out_3_o   => s_funct_out(3),
        funct_out_4_o   => s_funct_out(4),
        funct_out_5_o   => s_funct_out(5),
        funct_out_6_o   => s_funct_out(6),
        funct_out_7_o   => s_funct_out(7),
        funct_out_8_o   => s_funct_out(8),
        funct_out_9_o   => s_funct_out(9),
        funct_out_10_o  => s_funct_out(10),
        funct_out_11_o  => s_funct_out(11),
        funct_out_12_o  => s_funct_out(12),
        funct_out_13_o  => s_funct_out(13),
        funct_out_14_o  => s_funct_out(14),
        funct_out_15_o  => s_funct_out(15),
        
        funct_busy_0_o  => s_funct_busy(0),
        funct_busy_1_o  => s_funct_busy(1),
        funct_busy_2_o  => s_funct_busy(2),
        funct_busy_3_o  => s_funct_busy(3),
        funct_busy_4_o  => s_funct_busy(4),
        funct_busy_5_o  => s_funct_busy(5),
        funct_busy_6_o  => s_funct_busy(6),
        funct_busy_7_o  => s_funct_busy(7),
        funct_busy_8_o  => s_funct_busy(8),
        funct_busy_9_o  => s_funct_busy(9),
        funct_busy_10_o => s_funct_busy(10),
        funct_busy_11_o => s_funct_busy(11),
        funct_busy_12_o => s_funct_busy(12),
        funct_busy_13_o => s_funct_busy(13),
        funct_busy_14_o => s_funct_busy(14),
        funct_busy_15_o => s_funct_busy(15),
    
        -- trigger intputs
        ext_trig_start_0_i       => s_ext_trig_start_0_i,
        ext_trig_start_1_i       => s_ext_trig_start_1_i,
        ext_trig_restart_0_i     => s_ext_trig_restart_0_i,
        ext_trig_restart_1_i     => s_ext_trig_restart_1_i,
        ext_trig_trim_0_i        => s_ext_trig_trim_0_i,
        ext_trig_trim_1_i        => s_ext_trig_trim_1_i,
        ext_trig_prepare_0_i     => s_ext_trig_prepare_0_i,
        ext_trig_prepare_1_i     => s_ext_trig_prepare_1_i,
        ext_trig_coast_start_0_i => s_ext_trig_coast_start_0_i,
        ext_trig_coast_start_1_i => s_ext_trig_coast_start_1_i,
        ext_trig_coast_stop_0_i  => s_ext_trig_coast_stop_0_i,
        ext_trig_coast_stop_1_i  => s_ext_trig_coast_stop_1_i,
    
        -- status/control
        buffer_sel_o => s_buffer_sel,
        interlock_o => s_interlock,
    
        -- debug    
        inter_clk_en_o => s_inter_clk_en,
    
        clk_i => s_clk,
        reset_i => s_reset,
        m_axi_clk_i => s_m_axi_clk,
        m_axi_reset_i => s_m_axi_reset
    );
    
    FGC_axi4lite_cheburashka_bridge: axi4lite_cheburashka_bridge
    port map(
        ACLK => s_clk,
        ARESETN => s_reset_n,
        ARVALID => s_reg_AXI_RD_out.ARVALID,
        AWVALID => s_reg_AXI_WR_out.AWVALID,
        BREADY => s_reg_AXI_WR_out.BREADY,
        RREADY => s_reg_AXI_RD_out.RREADY,
        WLAST => s_reg_AXI_WR_out.WLAST,
        WVALID => s_reg_AXI_WR_out.WVALID,
        ARADDR => s_reg_AXI_RD_out.ARADDR,
        AWADDR => s_reg_AXI_WR_out.AWADDR,
        WDATA => s_reg_AXI_WR_out.WDATA,
        WSTRB => s_reg_AXI_WR_out.WSTRB,
        ARREADY => s_reg_AXI_RD_in.ARREADY,
        AWREADY => s_reg_AXI_WR_in.AWREADY,
        BVALID => s_reg_AXI_WR_in.BVALID,
        RLAST => s_reg_AXI_RD_in.RLAST,
        RVALID => s_reg_AXI_RD_in.RVALID,
        WREADY => s_reg_AXI_WR_in.WREADY,
        BRESP => s_reg_AXI_WR_in.BRESP,
        RRESP => s_reg_AXI_RD_in.RRESP,
        RDATA => s_reg_AXI_RD_in.RDATA,      
        ch_addr_o => s_ch_addr,
        ch_rd_data_i => s_ch_rd_data,
        ch_wr_data_o => s_ch_wr_data,
        ch_rd_mem_o => s_ch_rd_mem,
        ch_wr_mem_o => s_ch_wr_mem,
        ch_rd_done_i => s_ch_rd_done,
        ch_wr_done_i => s_ch_wr_done
    );
    
    --=============================================================================
    -- clock generation
    --=============================================================================
    s_clk       <= not s_clk after c_CLK_PERIOD/2;
    s_m_axi_clk <= not s_m_axi_clk after c_M_AXI_CLK_PERIOD/2;
      
    s_reset         <= '1', '0' after c_RESET_TIME;
    s_m_axi_reset   <= '1', '0' after c_RESET_TIME;
    s_reset_n       <= not s_reset;

     --=============================================================================
     -- interpolation clock frequency measurement
     --=============================================================================
    intepolation_clk_frequ: PROCESS
        variable v_TIME : time := 0 ns;
    BEGIN
        wait until s_reset = '1';
        wait until s_reset = '0';
    
        -- interpol clock
        wait until s_inter_clk_en = '1';
        v_TIME := now;
        wait until s_inter_clk_en = '1';
        v_TIME := now - v_TIME;
        s_intepolat_clk_freq <= v_TIME;  -- is in ps
        report ">> intepolatolation clock period" & time'image(v_TIME)severity NOTE;
        
        --clock period
        wait until s_clk = '1';
        v_TIME := now;
        wait until s_clk = '1';
        v_TIME := now - v_TIME;
        s_clk_freq <= v_TIME;  -- is in ps
        
        report ">> clock period" & time'image(v_TIME)severity NOTE;
        
        -- axi clock period
        wait until s_m_axi_clk = '1';
        v_TIME := now;
        wait until s_m_axi_clk = '1';
        v_TIME := now - v_TIME;
        s_axi_clk_freq <= v_TIME;  -- is in ps

        report ">> AXI clock period" & time'image(v_TIME)severity NOTE;

        wait;
    END PROCESS;

     --=============================================================================
     -- function output testing
     --=============================================================================
     -- multiple process creation / one per function
     gen_function_testing: for F in 0 to (c_FGC_CHANNELS-1) generate
        function_output_testing: PROCESS
            variable v_cnt          : integer := 0; 
            variable v_funct_error  : integer := 0;    
        BEGIN
            wait until s_reset = '1';
            wait until s_reset = '0';
            
            wait until s_testing_on = '1';
            loop
                -- wait on function generation start if not in coast mode

                wait until s_funct_busy(F) = '1' and s_coast_mode_cycle = '0';
                v_funct_error := 0;
                -- compare the generated data with the matlab data file content 
                for I in 0 to (s_function_data.length(s_cycle_cnt,F)-1) loop 
                    -- sychronization on the interpolation clock
                    wait until s_inter_clk_en = '1';
                    wait until s_inter_clk_en = '0';
                    -- for function 0, compare only if function interpolation is enabled
                    if(I = 0 and s_funct0_inter_disable = '0')then
                        -- comparing the start value
                        if(s_function_data.start_value(s_cycle_cnt,F) /= to_integer(signed(s_funct_out(F))))then
                            report  lf & ">> Start value data mismatch for function : " & integer'image(F) & lf &
                                         ">> Start value data mismatch during cycle : " & integer'image(s_cycle_cnt) severity ERROR;
                                         
                            -- mismatch error counter
                            v_funct_error := v_funct_error + 1;
                        end if;
                    else
                        -- comparing the data
                        if(s_function_data.data(s_cycle_cnt,F,I) /= to_integer(signed(s_funct_out(F))))then
                            report  lf & ">> Function data mismatch for function      : " & integer'image(F) & lf &
                                         ">> Function data mismatch during cycle      : " & integer'image(s_cycle_cnt) & lf & 
                                         ">> Function data mismatch at function point : " & integer'image(I)  severity ERROR ;
                            -- mismatch error counter
                            v_funct_error := v_funct_error + 1;
                        end if;
                    end if;
                end loop;
                
                -- print data comparison final result
                if(v_funct_error = 0)then 
                    report  lf & ">> Function testing success for function : " & integer'image(F) & lf &
                                 ">> Function data mismatch                : " & integer'image(v_funct_error) & " errors" & lf &
                                 ">> Cycle number                          : " & integer'image(s_cycle_cnt)  severity NOTE ;
                else
                    report  lf & ">> Function testing failed for function : " & integer'image(F) & lf &
                                 ">> Function data mismatch                : " & integer'image(v_funct_error) & " errors" & lf &
                                 ">> Cycle number                          : " & integer'image(s_cycle_cnt)  severity NOTE ;
                end if;
                
                -- make sum of all error
                s_funct_error(F) <= s_funct_error(F) + v_funct_error;
            end loop; 
            wait;
        END PROCESS function_output_testing;
    end generate gen_function_testing;
    
    --=============================================================================
    -- coast trim testing
    --=============================================================================
    -- multiple process creation / one per function
    gen_coast_testing: for F in 0 to (c_FGC_CHANNELS-1) generate
       coast_output_testing: PROCESS
           variable v_cnt          : integer := 0; 
           variable v_coast_error  : integer := 0;    
           variable v_start_value  : std_logic_vector(c_FUNCT_OUT_WIDTH-1 downto 0);
       BEGIN
           
           wait until s_reset = '1';
           wait until s_reset = '0';
           
           wait until s_testing_on = '1';
           loop
               -- wait trim trigger
               wait until s_ext_trig_trim_0_i = '1';
               wait for 2ns;
               -- copy current function value
               v_start_value := s_funct_out(F);
               v_coast_error := 0;
               
               -- wait two interpolation clock cycle to be synchronized 
               wait until s_inter_clk_en = '0' and s_clk = '1';
               wait until s_inter_clk_en = '1';
               wait until s_inter_clk_en = '0';
               
               wait until s_inter_clk_en = '1';
               wait until s_inter_clk_en = '0';

               -- compare the data of the coast trim
               for I in 0 to (s_coast_data.length(s_coast_trim_cnt)-1) loop 
                   -- interpolation clock sychronization
                   wait until s_inter_clk_en = '1';
                   wait until s_inter_clk_en = '0';
                   
                   -- comparing data
                   if(s_coast_data.data(s_coast_trim_cnt,I) + to_integer(signed(v_start_value)) /= to_integer(signed(s_funct_out(F))))then
                       
                        report  lf & ">> Coast trim data mismatch for function        : " & integer'image(F) & lf &
                                     ">> Coast trim data mismatch during coast trim   : " & integer'image(s_coast_trim_cnt) & lf & 
                                     ">> Coast trim difference: " & integer'image(s_coast_data.data(s_coast_trim_cnt,I) + to_integer(signed(v_start_value))-to_integer(signed(s_funct_out(F)))) & lf & 
                                     ">> Coast trim data mismatch at coast trim point : " & integer'image(I)  severity ERROR ;
                       
                       v_coast_error := v_coast_error + 1;
                   end if;
               end loop;
               
               -- print data comparison final result
               if(v_coast_error = 0)then 
                    report  lf & ">> Coast trim testing success for function : " & integer'image(F) & lf &
                                 ">> Coast trim data mismatch                : " & integer'image(v_coast_error) & " errors" & lf &
                                 ">> Coast trim number                       : " & integer'image(s_coast_trim_cnt)  severity NOTE ;
               else
                    report  lf & ">> Coastr trim testing failed for function : " & integer'image(F) & lf &
                                 ">> Coast trim data mismatch                : " & integer'image(v_coast_error) & " errors" & lf &
                                 ">> Coast trim number                       : " & integer'image(s_coast_trim_cnt)  severity ERROR ;
               end if;
               -- make sum of all error
               s_coast_error(F) <= s_coast_error(F) + v_coast_error;
           end loop; 
           wait;
       END PROCESS coast_output_testing;
   end generate gen_coast_testing;
   
    --=============================================================================
   -- special vector testing
   --=============================================================================
    special_vector_testing: PROCESS
        variable v_old_value          : integer := 0; 
    BEGIN
        s_special_vector_detected <= '0';
        s_special_vector_error <= 0;
        
        wait until s_reset = '1';
        wait until s_reset = '0';
        
        wait until s_testing_on = '1';
        wait until s_testing_on = '0';

        -- wait start of function trigger
        wait until s_funct_busy(c_FUNCTION_TESTING) = '1';

        v_old_value := 0;
        while s_special_vector_detected = '0' loop
            wait until s_inter_clk_en = '1';
            wait until s_inter_clk_en = '0';
            
            if(v_old_value = to_integer(signed(s_funct_out(c_FUNCTION_TESTING))))then
                s_special_vector_detected <= '1';
            end if;
            
            v_old_value := to_integer(signed(s_funct_out(c_FUNCTION_TESTING)));
        end loop;

        wait until s_ext_trig_restart_0_i(c_FUNCTION_TESTING) = '1';
        
        wait until s_inter_clk_en = '1';
        wait until s_inter_clk_en = '0';
        wait until s_inter_clk_en = '1';
        wait until s_inter_clk_en = '0';

        if(v_old_value = to_integer(signed(s_funct_out(c_FUNCTION_TESTING))))then
            s_special_vector_error <= s_special_vector_error + 1;
        end if;

        wait;
    END PROCESS special_vector_testing;

    --=============================================================================
    -- Testbench
    --=============================================================================
    testbench: PROCESS
        variable v_buffer_selection_status_error    : integer := 0;
        variable v_coast_tot_error                  : integer := 0;
        variable v_funct_tot_error                  : integer := 0;
        variable v_AXI_RD_data                      : std_logic_vector(c_REG_AXI_DATA_WIDTH-1 downto 0);
        variable v_fault_tester_error               : integer := 0;
        variable v_coast_loading_error              : integer := 0;
        variable v_data                             : std_logic_vector (31 downto 0) := (others => '0');
    BEGIN
        SIM_STATUS <= IDLE;
        wait until s_reset = '1';
        --=======================================================================================================================================
        -- Read IP core information registers
        --=======================================================================================================================================
        report lf & "*******************************************" & lf &
                    "Reading the IP core information registers..."; -- severity note
        AXI_READ(C_Reg_ipInfo_firmwareVersion,v_data,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk); 
        if(unsigned(v_data) /= unsigned(c_FIRMWARE_VERSION))then
            report "IP firmware version error!" severity error;
            stop;
        else
            report "IP firmware version:" & integer'image(to_integer(unsigned(c_FIRMWARE_VERSION)));
        end if;
        
        AXI_READ(C_Reg_ipInfo_memMapVersion,v_data,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk); 
        if(unsigned(v_data) /= unsigned(c_MEM_MAP_VESRION))then
            report "IP memory map version error!" severity error;
            stop;
        else
            report "IP memory map version:" & integer'image(to_integer(unsigned(c_MEM_MAP_VESRION)));
        end if;
        
        --=======================================================================================================================================
        -- Read files 
        --=======================================================================================================================================
        s_function_data     <= FUNCT_FILE_READ("function_data.txt");                    -- this file contains all values of the generated functions
        s_coast_vector(0)   <= COAST_VECTOR_FILE_READ("coast_trim_vector_data_0.txt");  -- this file contains vector (slope,interval) values for coast trim 0
        s_coast_vector(1)   <= COAST_VECTOR_FILE_READ("coast_trim_vector_data_1.txt");  -- this file contains vector (slope,interval) values for coast trim 0
        s_coast_data        <= COAST_DATA_FILE_READ("coast_trim_data.txt");             -- this file contains all values of the generated coast trim
    
        -- wait end of reset
        wait until s_reset = '0' and s_clk = '1';
        
        --=======================================================================================================================================
        -- Faults tester 
        --=======================================================================================================================================
        SIM_STATUS <= FAULTS_TESTER;
        FAULTS_TESTER(s_ext_trig_start_0_i,s_ext_trig_prepare_0_i,s_ext_trig_trim_0_i,s_interlock,s_function_data,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk,v_fault_tester_error); -- fault enable register / set all enable to '1'
        
        --=======================================================================================================================================
        -- x Cycles testing
        --=======================================================================================================================================
        --enable testing processes
        s_testing_on <= '1';
        SIM_STATUS <= FUNCTIONS_TESTER;
        FUNCTIONS_TESTER(s_funct_busy(c_FGC_CHANNELS-1 downto 0),s_ext_trig_start_0_i,s_ext_trig_prepare_0_i,s_funct0_inter_disable,s_cycle_cnt,s_function_data,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk,s_m_axi_clk,v_buffer_selection_status_error);

        --=======================================================================================================================================
        -- Coast trim testing
        --=======================================================================================================================================
        SIM_STATUS <= COAST_TRIM_TESTER;
        COAST_TRIM_TESTER(s_ext_trig_start_0_i,s_ext_trig_prepare_0_i,s_ext_trig_trim_0_i,s_ext_trig_coast_start_0_i,s_ext_trig_coast_stop_0_i,s_coast_mode_cycle,s_function_data,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk,s_m_axi_clk,s_coast_trim_cnt,s_coast_vector,v_coast_loading_error);
        s_testing_on <= '0';
        
        --=======================================================================================================================================
        -- special vector testing
        --=======================================================================================================================================
        SIM_STATUS <= SPECIAL_VECTOR_TESTER;
        SPECIAL_VECTOR_TESTER(s_ext_trig_start_0_i,s_ext_trig_prepare_0_i,s_ext_trig_restart_0_i,s_special_vector_detected,s_function_data,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk,s_m_axi_clk);
        
        --=======================================================================================================================================
        -- SIMULATION RESULTS
        --=======================================================================================================================================
        -- perfom the error sum of all functions
        for F in 0 to c_FGC_CHANNELS-1 loop
            v_coast_tot_error := v_coast_tot_error + s_coast_error(F);
            v_funct_tot_error := v_funct_tot_error + s_funct_error(F);
        end loop;
        
        report  lf & ">>**************************************************************" & lf &
                ">> *** SIMULATION RESULTS ***" & lf &
                ">>**************************************************************" & lf &
                ">>  Intepolatolation clock period  : " & time'image(s_intepolat_clk_freq)& lf &
                ">>  Clock period                   : " & time'image(s_clk_freq)& lf &
                ">>  AXI clock period               : " & time'image(s_axi_clk_freq)& lf &
                ">>  Functions number               : "  & INTEGER'IMAGE(c_FGC_CHANNELS) & lf &
                ">>  Cycles number                  : "  & INTEGER'IMAGE(c_CYCLES+2) & lf &                  -- contains two coast mode cycles
                ">>" & lf &
                ">>  End of the simulation with : " & INTEGER'IMAGE(v_fault_tester_error+v_buffer_selection_status_error+
                                                                  v_coast_loading_error+
                                                                  v_coast_tot_error+
                                                                  v_funct_tot_error+
                                                                  s_special_vector_error) & " errors" & lf &
                ">>" & lf &
                ">>  Faults generation test     : " & INTEGER'IMAGE(v_fault_tester_error) & " errors" & lf &
                ">>  Buffer selection status    : " & INTEGER'IMAGE(v_buffer_selection_status_error) & " errors" & lf &
                ">>  Coast memory loading       : " & INTEGER'IMAGE(v_coast_loading_error) & " errors" & lf &
                ">>  Functions generation       : " & INTEGER'IMAGE(v_funct_tot_error) & " errors" & lf &
                ">>  Coast trims generation     : " & INTEGER'IMAGE(v_coast_tot_error) & " errors" & lf &
                ">>  Coast with special vector  : " & INTEGER'IMAGE(s_special_vector_error) & " errors" & lf &
                ">>**************************************************************";
        stop;        
                
    END PROCESS testbench;
 
end testbench;
