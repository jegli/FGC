-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, FGC --
-- --
-------------------------------------------------------------------------------
--
-- unit name: FGC
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 09/07/2018
--
-- version: 1.0
--
-- description: FGC IP core 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
-- 2018.10.03
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity FGC is
  generic (
      -- function parameters
      g_FGC_CHANNELS            : integer := 4; 
      -- AXI read interface width
      g_M_AXI_DATA_WIDTH        : integer := 64;
      -- AXI read interface address width
      g_M_AXI_ADDR_WIDTH        : integer := 32;
      -- function 0 output width
      g_FUNCT_0_OUT_WIDTH       : integer := 16;
      -- function 1 output width
      g_FUNCT_1_OUT_WIDTH       : integer := 16;
      -- function 2 output width
      g_FUNCT_2_OUT_WIDTH       : integer := 16;
      -- function 3 output width
      g_FUNCT_3_OUT_WIDTH       : integer := 16;
      -- function 4 output width
      g_FUNCT_4_OUT_WIDTH       : integer := 16;
      -- function 5 output width
      g_FUNCT_5_OUT_WIDTH       : integer := 16;
      -- function 6 output width
      g_FUNCT_6_OUT_WIDTH       : integer := 16;
      -- function 7 output width
      g_FUNCT_7_OUT_WIDTH       : integer := 16;
      -- function 8 output width
      g_FUNCT_8_OUT_WIDTH       : integer := 16;
      -- function 9 output width
      g_FUNCT_9_OUT_WIDTH       : integer := 16;
      -- function 10 output width
      g_FUNCT_10_OUT_WIDTH       : integer := 16;
      -- function 11 output width
      g_FUNCT_11_OUT_WIDTH       : integer := 16;
      -- function 12 output width
      g_FUNCT_12_OUT_WIDTH       : integer := 16;
      -- function 13 output width
      g_FUNCT_13_OUT_WIDTH       : integer := 16;
      -- function 14 output width
      g_FUNCT_14_OUT_WIDTH       : integer := 16;
      -- function 15 output width
      g_FUNCT_15_OUT_WIDTH       : integer := 16;
      -- size of buffer in bytes (per function)
      g_BUFFER_SIZE             : integer   := 4096;
      -- interpolation value (interpolation frequency = input clk frequency/ interpolation value)
      g_INTERPOLATION_VALUE     : integer := 625; -- 625 => 62.5MHz/625 = 100000Hz => 10us  
      -- for simulation (1 = simulation mode)
      g_SIMULATION              : natural := 0 
  );
  port (
    -- external memory master interface
    mem_m_AXI_araddr : out STD_LOGIC_VECTOR ( g_M_AXI_ADDR_WIDTH-1 downto 0 );
    mem_m_AXI_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_AXI_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_AXI_arid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_AXI_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_AXI_arlock : out STD_LOGIC;
    mem_m_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_AXI_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_AXI_arready : in STD_LOGIC;
    mem_m_AXI_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_AXI_aruser : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_AXI_arvalid : out STD_LOGIC;
    mem_m_AXI_rdata : in STD_LOGIC_VECTOR ( g_M_AXI_DATA_WIDTH-1 downto 0 );
    mem_m_AXI_rid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_AXI_rlast : in STD_LOGIC;
    mem_m_AXI_rready : out STD_LOGIC;
    mem_m_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_AXI_ruser : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_AXI_rvalid : in STD_LOGIC;
    
    -- register slave interface
    cheb_Addr : in std_logic_vector(12 downto 2);
    cheb_RdData : out std_logic_vector(31 downto 0);
    cheb_WrData : in std_logic_vector(31 downto 0);
    cheb_RdMem : in std_logic;
    cheb_WrMem : in std_logic;
    cheb_RdDone : out std_logic;
    cheb_WrDone : out std_logic;
    cheb_RdError : out std_logic;
    cheb_WrError : out std_logic;

    -- function output
    funct_out_0_o   : out std_logic_vector(g_FUNCT_0_OUT_WIDTH-1 downto 0);
    funct_out_1_o   : out std_logic_vector(g_FUNCT_1_OUT_WIDTH-1 downto 0);
    funct_out_2_o   : out std_logic_vector(g_FUNCT_2_OUT_WIDTH-1 downto 0);
    funct_out_3_o   : out std_logic_vector(g_FUNCT_3_OUT_WIDTH-1 downto 0);
    funct_out_4_o   : out std_logic_vector(g_FUNCT_4_OUT_WIDTH-1 downto 0);
    funct_out_5_o   : out std_logic_vector(g_FUNCT_5_OUT_WIDTH-1 downto 0);
    funct_out_6_o   : out std_logic_vector(g_FUNCT_6_OUT_WIDTH-1 downto 0);
    funct_out_7_o   : out std_logic_vector(g_FUNCT_7_OUT_WIDTH-1 downto 0);
    funct_out_8_o   : out std_logic_vector(g_FUNCT_8_OUT_WIDTH-1 downto 0);
    funct_out_9_o   : out std_logic_vector(g_FUNCT_9_OUT_WIDTH-1 downto 0);
    funct_out_10_o  : out std_logic_vector(g_FUNCT_10_OUT_WIDTH-1 downto 0);
    funct_out_11_o  : out std_logic_vector(g_FUNCT_11_OUT_WIDTH-1 downto 0);
    funct_out_12_o  : out std_logic_vector(g_FUNCT_12_OUT_WIDTH-1 downto 0);
    funct_out_13_o  : out std_logic_vector(g_FUNCT_13_OUT_WIDTH-1 downto 0);
    funct_out_14_o  : out std_logic_vector(g_FUNCT_14_OUT_WIDTH-1 downto 0);
    funct_out_15_o  : out std_logic_vector(g_FUNCT_15_OUT_WIDTH-1 downto 0);
    
    funct_busy_0_o  : out std_logic;
    funct_busy_1_o  : out std_logic;
    funct_busy_2_o  : out std_logic;
    funct_busy_3_o  : out std_logic;
    funct_busy_4_o  : out std_logic;
    funct_busy_5_o  : out std_logic;
    funct_busy_6_o  : out std_logic;
    funct_busy_7_o  : out std_logic;
    funct_busy_8_o  : out std_logic;
    funct_busy_9_o  : out std_logic;
    funct_busy_10_o : out std_logic;
    funct_busy_11_o : out std_logic;
    funct_busy_12_o : out std_logic;
    funct_busy_13_o : out std_logic;
    funct_busy_14_o : out std_logic;
    funct_busy_15_o : out std_logic;
    
    -- trigger intputs
    ext_trig_start_0_i              : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    ext_trig_start_1_i              : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    ext_trig_restart_0_i            : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    ext_trig_restart_1_i            : in std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    ext_trig_trim_0_i               : in std_logic;
    ext_trig_trim_1_i               : in std_logic;
    ext_trig_prepare_0_i            : in std_logic;
    ext_trig_prepare_1_i            : in std_logic;
    ext_trig_coast_start_0_i        : in std_logic;
    ext_trig_coast_start_1_i        : in std_logic;
    ext_trig_coast_stop_0_i         : in std_logic;
    ext_trig_coast_stop_1_i         : in std_logic;

    -- status/control
    buffer_sel_o : out std_logic; 
    interlock_o  : out std_logic;
    
    --debug
    inter_clk_en_o  : out std_logic;

    clk_i           : in std_logic;
    reset_i         : in std_logic;
    m_axi_clk_i     : in std_logic;
    m_axi_reset_i   : in std_logic
  );
end FGC;

architecture arch_imp of FGC is    

    -- attributes declaration
    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    
    ATTRIBUTE X_INTERFACE_INFO of reset_i: SIGNAL is "xilinx.com:signal:reset:1.0 reset_i RST";

    ATTRIBUTE X_INTERFACE_PARAMETER of reset_i: SIGNAL is "POLARITY ACTIVE_HIGH";
    
    ATTRIBUTE X_INTERFACE_INFO of m_axi_reset_i: SIGNAL is "xilinx.com:signal:reset:1.0 m_axi_reset_i RST";
    ATTRIBUTE X_INTERFACE_PARAMETER of m_axi_reset_i: SIGNAL is "POLARITY ACTIVE_HIGH";
    ----------------------------------------------------------------------
    -- constant instantation 
    constant c_MAX_FGC_CHANNELS : positive := 16;
    constant c_SYNC_TIME        : positive := 4;
   
    ----------------------------------------------------------------------
    -- type instantation
    type t_funct_config_sel_array is array (g_FGC_CHANNELS-1 downto 0) of std_logic_vector(1 downto 0);
    -------------------------------------------------------------------------------
    -- signal instantation
    signal s_cheb_WrData        : std_logic_vector(cheb_WrData'range);

    signal s_soft_trig_prepare  : std_logic;
    signal s_prepare            : std_logic;
    signal s_prepare_trig_sel   : std_logic_vector(1 downto 0);
    
    signal s_control_clearFunctsAfterCoast  : std_logic;
    signal s_coast_trig_sel                 : std_logic_vector(1 downto 0);    
    signal s_soft_trig_trim                 : std_logic;
    signal s_coast_trim                     : std_logic;
    signal s_coast_trim_sync                : std_logic;
    signal s_coast_next_vector              : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_coast_vector_valid             : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_coast_slope                    : std_logic_vector(c_SLOPE_WIDTH-1 downto 0);
    signal s_coast_interval                 : std_logic_vector(c_INTERVAL_WIDTH-1 downto 0);
    signal s_coast_mem_ready                : std_logic;
    signal s_coast_buff_sel                 : std_logic;
    signal s_coast_enable                   : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_coast_start_trig_sel           : std_logic_vector(1 downto 0);
    signal s_coast_stop_trig_sel            : std_logic_vector(1 downto 0);
    signal s_soft_trig_coast_start          : std_logic;
    signal s_soft_trig_coast_stop           : std_logic;
    signal s_coast_start_mux                : std_logic;
    signal s_coast_stop_mux                 : std_logic;
    
	signal s_coastMemory_Addr       : std_logic_vector(11 downto 2);
    signal s_coastMemory_RdData     : std_logic_vector(31 downto 0);
    signal s_coastMemory_WrData     : std_logic_vector(31 downto 0);
    signal s_coastMemory_RdMem      : std_logic;
    signal s_coastMemory_WrMem      : std_logic;
    signal s_coastMemory_RdDone     : std_logic;
    signal s_coastMemory_WrDone     : std_logic;

    signal s_RD_cmd                 : t_AXI_RD_command;
    signal s_AXI_RD_done            : std_logic;
    signal s_AXI_RD_error           : std_logic;
    signal s_AXI_RD_busy            : std_logic;
    signal s_AXI_RD_data_valid      : std_logic;
    signal s_AXI_RD_data            : std_logic_vector (g_M_AXI_DATA_WIDTH-1 downto 0);
    signal s_fifo_man_en            : std_logic;
    signal s_funct_mem_ready        : std_logic;
    
    signal s_FIFO_wr                : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_wrdata            : std_logic_vector (g_M_AXI_DATA_WIDTH-1 downto 0); 
    signal s_FIFO_wrinfo            : std_logic_vector (c_INFO_WIDTH-1 downto 0); 
    signal s_FIFO_prog_empty        : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_prog_full         : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_full              : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_empty             : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_wr_rst_busy       : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_reset             : std_logic;
            
    signal s_funct_busy_OR              : std_logic;    
    signal s_funct_busy                 : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_coast_busy                 : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_coast_mode                 : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_fifo_man_busy              : std_logic;
    signal s_fifo_man_busy_1            : std_logic;
    signal s_fifo_man_busy_2            : std_logic;
    signal s_FIFO_preloading_finished   : std_logic;
    signal s_function_output            : t_FCG_ARRAY_32_STD(g_FGC_CHANNELS-1 downto 0);  

    signal s_soft_reset                 : std_logic;
    signal s_m_axi_reset_sync           : std_logic;
    signal s_m_axi_reset_sync_n         : std_logic;
    signal s_m_axi_reset                : std_logic;
    signal s_general_reset              : std_logic;
    signal s_clr_faults                 : std_logic;
    signal s_sat_fault_en               : std_logic;
    signal s_fifo_empty_fault_en        : std_logic;
    signal s_fifo_full_fault_en         : std_logic;
    signal s_cycle_mismatch_fault_en    : std_logic;
    signal s_funct_mem_ready_fault_en   : std_logic;
    signal s_coast_mem_ready_fault_en   : std_logic;
    
    signal s_acc_sat_fault              : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fifo_empty_fault           : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fifo_full_fault            : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fifo_full_fault_1          : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fifo_full_fault_2          : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_cycle_mismatch_fault       : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    
    signal s_acc_sat_fault_reg          : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_fifo_empty_fault_reg       : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_fifo_full_fault_reg        : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_fifo_full_fault_1_reg      : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_fifo_full_fault_2_reg      : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_cycle_mismatch_fault_reg   : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);

    signal s_funct_mem_ready_fault      : std_logic;
    signal s_funct_mem_ready_fault_1    : std_logic;
    signal s_funct_mem_ready_fault_2    : std_logic;
    signal s_coast_mem_ready_fault      : std_logic;
    
    signal s_acc_sat_fault_OR           : std_logic;
    signal s_fifo_empty_fault_OR        : std_logic;
    signal s_fifo_full_fault_OR         : std_logic;
    signal s_cycle_mismatch_fault_OR    : std_logic;
    
    signal s_buffer_start_addr          : std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
    signal s_buffer_enable              : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_buffer_sel                 : std_logic;
    signal s_buffer_sel_1               : std_logic;
    signal s_buffer_sel_2               : std_logic;
    signal s_fgc_buffer_sel_update      : std_logic;
    signal s_fgc_buffer_sel             : std_logic;
    signal s_coast_start                : std_logic;
    signal s_coast_stop                 : std_logic;
    signal s_soft_trig_start            : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_soft_trig_restart          : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);

    signal s_ch_addr                    : std_logic_vector(31 downto 0);
    signal s_ch_rd_data                 : std_logic_vector(c_REG_WIDTH-1 downto 0);
    signal s_ch_wr_data                 : std_logic_vector(c_REG_WIDTH-1 downto 0);
    signal s_ch_rd_mem                  : std_logic;
    signal s_ch_wr_mem                  : std_logic;
    signal s_ch_rd_done                 : std_logic;
    signal s_ch_wr_done                 : std_logic;
    
    signal s_inter_clk_en               : std_logic;
    signal s_interlock                  : std_logic;
    signal s_overflow                   : std_logic;
    
    signal s_funct_config_reg           : std_logic_vector(c_REG_WIDTH-1 downto 0);
    
    signal s_offline_start_value_reg    : std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);
    signal s_online_start_value_reg     : std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);
    signal s_function_size_reg          : std_logic_vector(15 downto 0);
    signal s_current_value_reg          : std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);
    signal s_faults_SRFF                : std_logic_vector(31 downto 0);
    signal s_coast_mode_reg             : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_coast_busy_reg             : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);
    signal s_funct_busy_reg             : std_logic_vector(c_MAX_FGC_CHANNELS-1 downto 0);

    signal s_funct_config_WrStrobe         : std_logic;
	signal s_funct_config_RdStrobe         : std_logic;
	signal s_funct_config_Sel              : std_logic;
	signal s_offline_start_value_WrStrobe  : std_logic;
    signal s_offline_start_value_RdStrobe  : std_logic;
    signal s_offline_start_value_Sel       : std_logic;
    signal s_function_size_WrStrobe        : std_logic;
    signal s_function_size_RdStrobe        : std_logic;
    signal s_function_size_Sel             : std_logic;
    signal s_current_value_RdStrobe        : std_logic;

    signal s_sync_cnt                       : unsigned(4 downto 0);
    signal s_sync_cnt1                      : unsigned(4 downto 0);
    signal s_function_size_ready            : std_logic;
    signal s_bufferStartAddress_WrStrobe    : std_logic;
    signal s_bufferStartAddress_ready       : std_logic;

    -- multiplexed signals
    signal s_funct_sel                  : unsigned(3 downto 0);
    signal s_funct_config               : t_functConfig_array(g_FGC_CHANNELS-1 downto 0);
    signal s_start_value                : t_FCG_ARRAY_32_STD(g_FGC_CHANNELS-1 downto 0);
    signal s_start_value_0              : t_FCG_ARRAY_32_STD(g_FGC_CHANNELS-1 downto 0);
    signal s_start_value_1              : t_FCG_ARRAY_32_STD(g_FGC_CHANNELS-1 downto 0);
    signal s_function_size              : t_FCG_ARRAY_16_STD(g_FGC_CHANNELS-1 downto 0);
    signal s_current_value              : t_FCG_ARRAY_32_STD(g_FGC_CHANNELS-1 downto 0);  

  -----------------------------------------------------------------------
  -- component instantation   
  component AXI_MM_read_interface is
  generic(
    -- AXI read interface width
    g_M_AXI_ADDR_WIDTH    : integer    := 32;
    -- Width of Data Bus
    g_M_AXI_DATA_WIDTH    : integer    := 64
  );
  port (
    AXI_RD_start_i : in STD_LOGIC;
    AXI_RD_start_address_i : in STD_LOGIC_VECTOR ( g_M_AXI_ADDR_WIDTH-1 downto 0 );
    AXI_RD_data_size_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_RD_burst_length_i : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_RD_done_o : out STD_LOGIC;
    AXI_RD_error_o : out STD_LOGIC;
    AXI_RD_busy_o : out STD_LOGIC;
    AXI_RD_data_valid_o : out STD_LOGIC;
    AXI_RD_data_o : out STD_LOGIC_VECTOR ( g_M_AXI_DATA_WIDTH-1 downto 0 );
    M_AXI_ACLK : in STD_LOGIC;
    M_AXI_ARESETN : in STD_LOGIC;
    M_AXI_ARID : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_ARADDR : out STD_LOGIC_VECTOR ( g_M_AXI_ADDR_WIDTH-1 downto 0 );
    M_AXI_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXI_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_ARLOCK : out STD_LOGIC;
    M_AXI_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARUSER : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_ARVALID : out STD_LOGIC;
    M_AXI_ARREADY : in STD_LOGIC;
    M_AXI_RID : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_RDATA : in STD_LOGIC_VECTOR ( g_M_AXI_DATA_WIDTH-1 downto 0 );
    M_AXI_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_RLAST : in STD_LOGIC;
    M_AXI_RUSER : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_RVALID : in STD_LOGIC;
    M_AXI_RREADY : out STD_LOGIC
  );
  end component AXI_MM_read_interface;
  -----------------------------------------------------------------------
  component FGC_chan is
  generic (
          -- Function parameters
          g_FGC_CHANNELS          : integer  := 16; 
          -- AXI interface width
          g_M_AXI_DATA_WIDTH      : integer  := 64;
          -- FIFO info data width
          g_FIFO_INFO_DATA_WIDTH  : integer := 8
  );
  Port ( 
          -- FIFO interfaces to fifo manager block
          FIFO_wr_i                   : in        std_logic;                                              -- fifo write
          FIFO_wrdata_i               : in        std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);        -- fifo write data 
          FIFO_wrinfo_i               : in        std_logic_vector(g_FIFO_INFO_DATA_WIDTH-1 downto 0);    -- fifo write data information 
          FIFO_prog_empty_o           : out       std_logic;                                              -- fifo programmable empty
          FIFO_prog_full_o            : out       std_logic;                                              -- fifo programmable full
          FIFO_full_o                 : out       std_logic;                                              -- fifo full
          FIFO_wr_rst_busy_o          : out       std_logic;                                              -- fifo wr reset is busy
          FIFO_reset_i                : in        std_logic;                                              -- fifo reset 
          
          -- RAM interface to RAM access manager block
          next_vector_o               : out       std_logic;
          vector_valid_i              : in        std_logic;
          coast_vector_i              : in        t_vector_format;                
          
          -- control interface
          start_value_i                 : in        std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);
          enable_i                      : in        std_logic;
          coast_enable_i                : in        std_logic;
          coast_start_i                 : in        std_logic;
          coast_stop_i                  : in        std_logic;
          interpol_disable_i            : in        std_logic;
          wrap_en_i                     : in        std_logic;
          clear_function_after_coast_i  : in        std_logic;
          
          -- triggers
          start_trig_sel_i            : in        std_logic_vector(1 downto 0);
          soft_trig_start_i           : in        std_logic;
          ext_trig_start_0_i          : in        std_logic;
          ext_trig_start_1_i          : in        std_logic;
          
          restart_trig_sel_i          : in        std_logic_vector(1 downto 0);
          soft_trig_restart_i         : in        std_logic;
          ext_trig_restart_0_i        : in        std_logic;
          ext_trig_restart_1_i        : in        std_logic;
          
          coast_trim_i                : in        std_logic;
          
          --buffer selection
          fgc_buffer_sel_update_i     : in    std_logic;                      -- buffer selection update from fifo manager
          fgc_buffer_sel_i            : in    std_logic;                      -- buffer selection value
          
          -- fault interface         
          acc_sat_fault_o             : out       std_logic;
          fifo_empty_fault_o          : out       std_logic;
          cycle_mismatch_fault_o      : out       std_logic;
          funct_busy_o                : out       std_logic;
          coast_busy_o                : out       std_logic;
          coast_mode_o                : out       std_logic;
          
          -- function
          function_output_o           : out       std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);

          inter_clk_en_i              : in        std_logic;
          clk_i                       : in        std_logic;  -- general clock
          wr_clk_i                    : in        std_logic;  -- fifo wr clock
          reset_i                     : in        std_logic   -- general reset
         ); 
  end component FGC_chan;
  -----------------------------------------------------------------------
  component multiplexer is
      Port ( 
              sel_i   : in    std_logic_vector(1 downto 0);
              --input
              in0_i    : in    std_logic;
              in1_i    : in    std_logic;
              in2_i    : in    std_logic;
              --ouput
              out_o   : out   std_logic
            );
  end component;
  -----------------------------------------------------------------------
  component prescaler is
      generic (
                -- interpolation value (interpolation frequency = input clk frequency/ interpolation value)
                g_PRESCALER_VALUE     : integer := 625 -- 625 => 62.5MHz/625 = 100000Hz => 10us  
            );
      Port ( 
              inter_clk_en_o              : out   std_logic;
  
              clk_i                       : in    std_logic;
              reset_i                     : in    std_logic  
            );
  end component prescaler;
  -----------------------------------------------------------------------
  component sync is
      Port ( 
  
              in_i                : in    std_logic;  --input
              out_o               : out   std_logic;  --output
              
              -- general
              inter_clk_en_i      : in    std_logic;  -- accumulator interpolation clock
              clk_i               : in    std_logic;  -- general clock
              reset_i             : in    std_logic   -- general reset
            );
  end component sync;
  -----------------------------------------------------------------------
  component reset_synchronizer is
      Port ( clk_i        : in STD_LOGIC;     -- clock 
             rst_a_i      : in STD_LOGIC;     -- asynchronous reset
             rst_sync_o   : out STD_LOGIC);   -- synchronized reset
  end component;
  -----------------------------------------------------------------------
  component fault_manager is
      Generic (
                  g_FGC_CHANNELS : integer := 16
          );
      Port ( 
                  --clear faults
                  clr_faults_i    : in    std_logic;
                  -- fault enable 
                  fault_en_0_i    : in    std_logic;
                  fault_en_1_i    : in    std_logic;
                  fault_en_2_i    : in    std_logic;
                  fault_en_3_i    : in    std_logic;
                  fault_en_4_i    : in    std_logic;
                  fault_en_5_i    : in    std_logic;
                  
                  -- fault inputs
                  fault_0_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                  fault_1_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                  fault_2_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                  fault_3_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                  fault_4_i       : in    std_logic;
                  fault_5_i       : in    std_logic;
                  
                  --fault output
                  fault_output_o  : out   std_logic;
                  
                  clk_i           : in    std_logic;
                  reset_i         : in    std_logic 
            );
  end component fault_manager;
  -----------------------------------------------------------------------
  component coast_memory is
      generic (
          -- number of functions
          g_FGC_CHANNELS        : integer   := 16;
          -- Width of S_AXI data bus
          g_S_AXI_DATA_WIDTH    : integer   := 64;
          -- Width of S_AXI address bus
          g_S_AXI_ADDR_WIDTH    : integer   := 32;
          -- Width of RAM address bus
          g_RAM_ADDR_WIDTH      : integer   := 9
      );
      port (
          -- COAST MEMORY INTERFACE
          coast_next_vector_i     : in  std_logic_vector(g_FGC_CHANNELS-1 downto 0);
          coast_vector_valid_o    : out std_logic_vector(g_FGC_CHANNELS-1 downto 0);
          coast_slope_o           : out std_logic_vector(c_SLOPE_WIDTH-1 downto 0);
          coast_interval_o        : out std_logic_vector(c_INTERVAL_WIDTH-1 downto 0);
          coast_mem_ready_i       : in  std_logic;
          coast_mem_ready_fault_o : out std_logic;
          coast_trim_i            : in  std_logic;
          coast_buff_sel_o        : out std_logic;
          coast_enable_i          : in  std_logic_vector(g_FGC_CHANNELS-1 downto 0);
          
          -- RAM INTERFACE FROM CHEBURASHKA
          coastMemory_Addr_i        : in    std_logic_vector(11 downto 2);
          coastMemory_RdData_o      : out   std_logic_vector(31 downto 0);
          coastMemory_WrData_i      : in    std_logic_vector(31 downto 0);
          coastMemory_RdMem_i       : in    std_logic;
          coastMemory_WrMem_i       : in    std_logic;
          coastMemory_RdDone_o      : out   std_logic;
          coastMemory_WrDone_o      : out   std_logic;
          
          --General Clock Signal
          clk_i                   : in  std_logic;
          reset_i                 : in  std_logic
      );
  end component coast_memory;
  -----------------------------------------------------------------------
  component FIFO_manager is
      generic (
              -- Function parameters
              g_FGC_CHANNELS          : integer   := 16; 
              -- size of buffer in bytes (per function)
              g_BUFFER_SIZE           : integer   := 4096; 
              -- AXI read interface width
              g_M_AXI_DATA_WIDTH      : integer   := 64;
              -- FIFO burst 
              g_FIFO_BURST            : integer   := 256
      );
      Port ( 
              -- read interfaces
              RD_cmd_o                : out   t_AXI_RD_command;
              RD_error_o              : out   std_logic;
        
              AXI_RD_data_valid_i     : in    std_logic;
              AXI_RD_data_i           : in    std_logic_vector (g_M_AXI_DATA_WIDTH-1 downto 0);
              AXI_RD_done_i           : in    std_logic;
              AXI_RD_busy_i           : in    std_logic;
              
              -- fifo interface
              FIFO_wr_o               : out   std_logic_vector(g_FGC_CHANNELS-1 downto 0);
              FIFO_wrdata_o           : out   std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
              FIFO_wrinfo_o           : out   std_logic_vector(c_INFO_WIDTH-1 downto 0);
              FIFO_prog_full_i        : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
              --FIFO_empty_i            : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
              FIFO_full_i             : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
              FIFO_wr_rst_busy_i      : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
              FIFO_reset_o            : out   std_logic; --unused
              
              -- status interface
              FIFO_full_fault_o           : out    std_logic; 
              FIFO_loading_finished_o     : out   std_logic;
              busy_o                      : out   std_logic;
              FIFO_preloading_finished_o  : out   std_logic;
              funct_mem_ready_fault_o     : out   std_logic;
              
              -- buffer interfaces
              buffer_enable_i           : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
              buffer_start_addr_i       : in    std_logic_vector(31 downto 0);
              buffer_start_addr_ready_i : in    std_logic;
              
              buffer_sel                : out   std_logic;
              fgc_buffer_sel_update_o   : out   std_logic;
              fgc_buffer_sel_o          : out   std_logic;
              function_size_i           : in    t_FCG_ARRAY_16_STD(g_FGC_CHANNELS-1 downto 0);
              function_size_ready_i     : in    std_logic;
              
              -- control interface
              funct_mem_ready_i       : in    std_logic;
              prepare_i               : in    std_logic;
              funct_busy_i            : in    std_logic;
              enable_i                : in    std_logic;
              
              clk_i                   : in    std_logic;
              reset_i                 : in    std_logic
             ); 
  end component FIFO_manager;
  -----------------------------------------------------------------------
  component RegCtrl_fgc is
      port (
              Clk : in std_logic;
              Rst : in std_logic;
              VMEAddr : in std_logic_vector(12 downto 2);
              VMERdData : out std_logic_vector(31 downto 0);
              VMEWrData : in std_logic_vector(31 downto 0);
              VMERdMem : in std_logic;
              VMEWrMem : in std_logic;
              VMERdDone : out std_logic;
              VMEWrDone : out std_logic;
              VMERdError : out std_logic;
              VMEWrError : out std_logic;
              ipInfo_ident : in std_logic_vector(63 downto 0);
              ipInfo_firmwareVersion : in std_logic_vector(31 downto 0);
              ipInfo_memMapVersion : in std_logic_vector(31 downto 0);
              ipInfo_sysControl : out std_logic_vector(15 downto 0);
              control_clearFunctsAfterCoast : out std_logic;
              control_softReset : out std_logic;
              control_clrFaults : out std_logic;
              control_fifoManEnable : out std_logic;
              control_functMemReady : out std_logic;
              control_coastMemReady : out std_logic;
              control_softTrigPrepare : out std_logic;
              control_softTrigTrim : out std_logic;
              control_softTrigCoastStart : out std_logic;
              control_softTrigCoastStop : out std_logic;
              control_prepareTrigSel : out std_logic_vector(7 downto 6);
              control_coastTrigSel : out std_logic_vector(5 downto 4);
              control_coastStartTrigSel : out std_logic_vector(3 downto 2);
              control_coastStopTrigSel : out std_logic_vector(1 downto 0);
              status_fifoManBusy : in std_logic;
              status_fifoManPreloadingFinished : in std_logic;
              status_noFaults : in std_logic;
              status_noOverflow : in std_logic;
              status_coastBuffSel : in std_logic;
              status_buffSel : in std_logic;
              status_coastMode : in std_logic_vector(31 downto 16);
              busy_functBusy : in std_logic_vector(15 downto 0);
              busy_coastTrimtBusy : in std_logic_vector(31 downto 16);
              faults_accSatFault : in std_logic;
              faults_fifoEmptyFault : in std_logic;
              faults_fifoFullFault : in std_logic;
              faults_cycleMismatchFault : in std_logic;
              faults_functMemReadyFault : in std_logic;
              faults_coastMemReadyFault : in std_logic;
              faults_SRFF : out std_logic_vector(31 downto 0);
              faults_ClrSRFF : in std_logic;
              faultsEnable_accSatFaultEnable : out std_logic;
              faultsEnable_fifoEmptyFaultEnable : out std_logic;
              faultsEnable_fifoFullFaultEnable : out std_logic;
              faultsEnable_cycleMismatchFaultEnable : out std_logic;
              faultsEnable_functMemReadyFaultEnable : out std_logic;
              faultsEnable_coastMemReadyFaultEnable : out std_logic;
              fifoFaults_fifoEmptyFault0 : in std_logic;
              fifoFaults_fifoEmptyFault1 : in std_logic;
              fifoFaults_fifoEmptyFault2 : in std_logic;
              fifoFaults_fifoEmptyFault3 : in std_logic;
              fifoFaults_fifoEmptyFault4 : in std_logic;
              fifoFaults_fifoEmptyFault5 : in std_logic;
              fifoFaults_fifoEmptyFault6 : in std_logic;
              fifoFaults_fifoEmptyFault7 : in std_logic;
              fifoFaults_fifoEmptyFault8 : in std_logic;
              fifoFaults_fifoEmptyFault9 : in std_logic;
              fifoFaults_fifoEmptyFault10 : in std_logic;
              fifoFaults_fifoEmptyFault11 : in std_logic;
              fifoFaults_fifoEmptyFault12 : in std_logic;
              fifoFaults_fifoEmptyFault13 : in std_logic;
              fifoFaults_fifoEmptyFault14 : in std_logic;
              fifoFaults_fifoEmptyFault15 : in std_logic;
              fifoFaults_fifoFullFault0 : in std_logic;
              fifoFaults_fifoFullFault1 : in std_logic;
              fifoFaults_fifoFullFault2 : in std_logic;
              fifoFaults_fifoFullFault3 : in std_logic;
              fifoFaults_fifoFullFault4 : in std_logic;
              fifoFaults_fifoFullFault5 : in std_logic;
              fifoFaults_fifoFullFault6 : in std_logic;
              fifoFaults_fifoFullFault7 : in std_logic;
              fifoFaults_fifoFullFault8 : in std_logic;
              fifoFaults_fifoFullFault9 : in std_logic;
              fifoFaults_fifoFullFault10 : in std_logic;
              fifoFaults_fifoFullFault11 : in std_logic;
              fifoFaults_fifoFullFault12 : in std_logic;
              fifoFaults_fifoFullFault13 : in std_logic;
              fifoFaults_fifoFullFault14 : in std_logic;
              fifoFaults_fifoFullFault15 : in std_logic;
              fifoFaults_SRFF : out std_logic_vector(31 downto 0);
              fifoFaults_ClrSRFF : in std_logic;
              functionFaults_accSatFault0 : in std_logic;
              functionFaults_accSatFault1 : in std_logic;
              functionFaults_accSatFault2 : in std_logic;
              functionFaults_accSatFault3 : in std_logic;
              functionFaults_accSatFault4 : in std_logic;
              functionFaults_accSatFault5 : in std_logic;
              functionFaults_accSatFault6 : in std_logic;
              functionFaults_accSatFault7 : in std_logic;
              functionFaults_accSatFault8 : in std_logic;
              functionFaults_accSatFault9 : in std_logic;
              functionFaults_accSatFault10 : in std_logic;
              functionFaults_accSatFault11 : in std_logic;
              functionFaults_accSatFault12 : in std_logic;
              functionFaults_accSatFault13 : in std_logic;
              functionFaults_accSatFault14 : in std_logic;
              functionFaults_accSatFault15 : in std_logic;
              functionFaults_cycleMismatchFault0 : in std_logic;
              functionFaults_cycleMismatchFault1 : in std_logic;
              functionFaults_cycleMismatchFault2 : in std_logic;
              functionFaults_cycleMismatchFault3 : in std_logic;
              functionFaults_cycleMismatchFault4 : in std_logic;
              functionFaults_cycleMismatchFault5 : in std_logic;
              functionFaults_cycleMismatchFault6 : in std_logic;
              functionFaults_cycleMismatchFault7 : in std_logic;
              functionFaults_cycleMismatchFault8 : in std_logic;
              functionFaults_cycleMismatchFault9 : in std_logic;
              functionFaults_cycleMismatchFault10 : in std_logic;
              functionFaults_cycleMismatchFault11 : in std_logic;
              functionFaults_cycleMismatchFault12 : in std_logic;
              functionFaults_cycleMismatchFault13 : in std_logic;
              functionFaults_cycleMismatchFault14 : in std_logic;
              functionFaults_cycleMismatchFault15 : in std_logic;
              functionFaults_SRFF : out std_logic_vector(31 downto 0);
              functionFaults_ClrSRFF : in std_logic;
              functSel_functionSelect : out std_logic_vector(16 downto 13);
              functConfig : in std_logic_vector(31 downto 0);
              functConfig_Sel : out std_logic;
              functConfig_WrStrobe : out std_logic;
              functConfig_RdStrobe : out std_logic;
              offlineStartValue : in std_logic_vector(31 downto 0);
              offlineStartValue_Sel : out std_logic;
              offlineStartValue_WrStrobe : out std_logic;
              offlineStartValue_RdStrobe : out std_logic;
              onlineStartValue : in std_logic_vector(31 downto 0);
              functionSize : in std_logic_vector(31 downto 0);
              functionSize_Sel : out std_logic;
              functionSize_WrStrobe : out std_logic;
              functionSize_RdStrobe : out std_logic;
              currentValue : in std_logic_vector(31 downto 0);
              softTrig_functStartTrig0 : out std_logic;
              softTrig_functStartTrig1 : out std_logic;
              softTrig_functStartTrig2 : out std_logic;
              softTrig_functStartTrig3 : out std_logic;
              softTrig_functStartTrig4 : out std_logic;
              softTrig_functStartTrig5 : out std_logic;
              softTrig_functStartTrig6 : out std_logic;
              softTrig_functStartTrig7 : out std_logic;
              softTrig_functStartTrig8 : out std_logic;
              softTrig_functStartTrig9 : out std_logic;
              softTrig_functStartTrig10 : out std_logic;
              softTrig_functStartTrig11 : out std_logic;
              softTrig_functStartTrig12 : out std_logic;
              softTrig_functStartTrig13 : out std_logic;
              softTrig_functStartTrig14 : out std_logic;
              softTrig_functStartTrig15 : out std_logic;
              softTrig_functRestartTrig0 : out std_logic;
              softTrig_functRestartTrig1 : out std_logic;
              softTrig_functRestartTrig2 : out std_logic;
              softTrig_functRestartTrig3 : out std_logic;
              softTrig_functRestartTrig4 : out std_logic;
              softTrig_functRestartTrig5 : out std_logic;
              softTrig_functRestartTrig6 : out std_logic;
              softTrig_functRestartTrig7 : out std_logic;
              softTrig_functRestartTrig8 : out std_logic;
              softTrig_functRestartTrig9 : out std_logic;
              softTrig_functRestartTrig10 : out std_logic;
              softTrig_functRestartTrig11 : out std_logic;
              softTrig_functRestartTrig12 : out std_logic;
              softTrig_functRestartTrig13 : out std_logic;
              softTrig_functRestartTrig14 : out std_logic;
              softTrig_functRestartTrig15 : out std_logic;
              bufferStartAddress : out std_logic_vector(31 downto 0);
              bufferStartAddress_WrStrobe : out std_logic;
              bufferSize : in std_logic_vector(31 downto 0);
              coastMemory_Sel : out std_logic;
              coastMemory_Addr : out std_logic_vector(11 downto 2);
              coastMemory_RdData : in std_logic_vector(31 downto 0);
              coastMemory_WrData : out std_logic_vector(31 downto 0);
              coastMemory_RdMem : out std_logic;
              coastMemory_WrMem : out std_logic;
              coastMemory_RdDone : in std_logic;
              coastMemory_WrDone : in std_logic;
              coastMemory_RdError : in std_logic := '0';
              coastMemory_WrError : in std_logic := '0'
      );
  end component;
  -----------------------------------------------------------------------
  component axi4lite_cheburashka_bridge is
    port (
      ACLK    : in std_logic;
      ARESETN : in std_logic;
  
      ARVALID : in  std_logic;
      AWVALID : in  std_logic;
      BREADY  : in  std_logic;
      RREADY  : in  std_logic;
      WLAST   : in  std_logic := '1';
      WVALID  : in  std_logic;
      ARADDR  : in  std_logic_vector (31 downto 0);
      AWADDR  : in  std_logic_vector (31 downto 0);
      WDATA   : in  std_logic_vector (31 downto 0);
      WSTRB   : in  std_logic_vector (3 downto 0);
      ARREADY : out std_logic;
      AWREADY : out std_logic;
      BVALID  : out std_logic;
      RLAST   : out std_logic;
      RVALID  : out std_logic;
      WREADY  : out std_logic;
      BRESP   : out std_logic_vector (1 downto 0);
      RRESP   : out std_logic_vector (1 downto 0);
      RDATA   : out std_logic_vector (31 downto 0);
  
      ch_addr_o    : out std_logic_vector(31 downto 0);
      ch_rd_data_i : in  std_logic_vector(31 downto 0);
      ch_wr_data_o : out std_logic_vector(31 downto 0);
      ch_rd_mem_o  : out std_logic;
      ch_wr_mem_o  : out std_logic;
      ch_rd_done_i : in  std_logic;
      ch_wr_done_i : in  std_logic
  
      );
  
  end component axi4lite_cheburashka_bridge;
begin
--=======================================================================================================
    -- general reset
    s_general_reset <= s_soft_reset or reset_i;
    s_m_axi_reset   <= s_soft_reset or m_axi_reset_i;

    s_m_axi_reset_sync_n <= not s_m_axi_reset_sync;

    axi_reset_synchronizer: reset_synchronizer
        port map(
                    clk_i=>m_axi_clk_i,
                    rst_a_i=>s_m_axi_reset,
                    rst_sync_o=>s_m_axi_reset_sync
        );

    -- default value
    cheb_RdError <= '0';
    cheb_WrError <= '0';

    --buffer selection
    inter_clk_en_o <= s_inter_clk_en;
    buffer_sel_o <= s_buffer_sel_2;

    -- connect function output to current value register
    s_current_value <= s_function_output;

    -- function output value truncation
    funct_out_0_o   <=  s_function_output(0)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_0_OUT_WIDTH))    when g_FGC_CHANNELS > 0 else (OTHERS => '0');
    funct_out_1_o   <=  s_function_output(1)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_1_OUT_WIDTH))    when g_FGC_CHANNELS > 1 else (OTHERS => '0');
    funct_out_2_o   <=  s_function_output(2)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_2_OUT_WIDTH))    when g_FGC_CHANNELS > 2 else (OTHERS => '0');
    funct_out_3_o   <=  s_function_output(3)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_3_OUT_WIDTH))    when g_FGC_CHANNELS > 3 else (OTHERS => '0');
    funct_out_4_o   <=  s_function_output(4)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_4_OUT_WIDTH))    when g_FGC_CHANNELS > 4 else (OTHERS => '0');
    funct_out_5_o   <=  s_function_output(5)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_5_OUT_WIDTH))    when g_FGC_CHANNELS > 5 else (OTHERS => '0');
    funct_out_6_o   <=  s_function_output(6)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_6_OUT_WIDTH))    when g_FGC_CHANNELS > 6 else (OTHERS => '0');
    funct_out_7_o   <=  s_function_output(7)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_7_OUT_WIDTH))    when g_FGC_CHANNELS > 7 else (OTHERS => '0');
    funct_out_8_o   <=  s_function_output(8)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_8_OUT_WIDTH))    when g_FGC_CHANNELS > 8 else (OTHERS => '0');
    funct_out_9_o   <=  s_function_output(9)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_9_OUT_WIDTH))    when g_FGC_CHANNELS > 9 else (OTHERS => '0');
    funct_out_10_o  <=  s_function_output(10)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_10_OUT_WIDTH))  when g_FGC_CHANNELS > 10 else (OTHERS => '0');
    funct_out_11_o  <=  s_function_output(11)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_11_OUT_WIDTH))  when g_FGC_CHANNELS > 11 else (OTHERS => '0');
    funct_out_12_o  <=  s_function_output(12)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_12_OUT_WIDTH))  when g_FGC_CHANNELS > 12 else (OTHERS => '0');
    funct_out_13_o  <=  s_function_output(13)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_13_OUT_WIDTH))  when g_FGC_CHANNELS > 13 else (OTHERS => '0');
    funct_out_14_o  <=  s_function_output(14)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_14_OUT_WIDTH))  when g_FGC_CHANNELS > 14 else (OTHERS => '0');
    funct_out_15_o  <=  s_function_output(15)(c_FUNCTION_WIDTH-1 downto (c_FUNCTION_WIDTH-g_FUNCT_15_OUT_WIDTH))  when g_FGC_CHANNELS > 15 else (OTHERS => '0');
    
    funct_busy_0_o  <=  s_funct_busy_reg(0);
    funct_busy_1_o  <=  s_funct_busy_reg(1);
    funct_busy_2_o  <=  s_funct_busy_reg(2);
    funct_busy_3_o  <=  s_funct_busy_reg(3);
    funct_busy_4_o  <=  s_funct_busy_reg(4);
    funct_busy_5_o  <=  s_funct_busy_reg(5);
    funct_busy_6_o  <=  s_funct_busy_reg(6);
    funct_busy_7_o  <=  s_funct_busy_reg(7);
    funct_busy_8_o  <=  s_funct_busy_reg(8);
    funct_busy_9_o  <=  s_funct_busy_reg(9);
    funct_busy_10_o <=  s_funct_busy_reg(10);
    funct_busy_11_o <=  s_funct_busy_reg(11);
    funct_busy_12_o <=  s_funct_busy_reg(12);
    funct_busy_13_o <=  s_funct_busy_reg(13);
    funct_busy_14_o <=  s_funct_busy_reg(14);
    funct_busy_15_o <=  s_funct_busy_reg(15);

    -----------------------------------------------------------------------
    process(s_fifo_empty_fault,s_fifo_full_fault_2,s_cycle_mismatch_fault,s_acc_sat_fault,s_coast_mode,s_coast_busy,s_funct_busy)
    begin
        for I in 0 to c_MAX_FGC_CHANNELS-1 loop
            if(I < g_FGC_CHANNELS)then
                s_fifo_empty_fault_reg(I)       <= s_fifo_empty_fault(I);
                s_fifo_full_fault_reg(I)        <= s_fifo_full_fault_2(I);
                s_cycle_mismatch_fault_reg(I)   <= s_cycle_mismatch_fault(I);
                s_acc_sat_fault_reg(I)          <= s_acc_sat_fault(I);     
                s_coast_mode_reg(I)             <= s_coast_mode(I);
                s_coast_busy_reg(I)             <= s_coast_busy(I);
                s_funct_busy_reg(I)             <= s_funct_busy(I);
            else
                s_fifo_empty_fault_reg(I)       <= '0';
                s_fifo_full_fault_reg(I)        <= '0';
                s_cycle_mismatch_fault_reg(I)   <= '0';
                s_acc_sat_fault_reg(I)          <= '0';
                s_coast_mode_reg(I)             <= '0';
                s_coast_busy_reg(I)             <= '0';
                s_funct_busy_reg(I)             <= '0';
            end if;
        end loop;
    end process;
    -----------------------------------------------------------------------
    coast_start_sync:sync
    Port map( 
        in_i => s_coast_start_mux,
        out_o => s_coast_start,
        inter_clk_en_i => s_inter_clk_en,
        clk_i => clk_i,
        reset_i => s_general_reset
    );
    -----------------------------------------------------------------------
    coast_stop_sync:sync
    Port map( 
        in_i => s_coast_stop_mux,
        out_o => s_coast_stop,
        inter_clk_en_i => s_inter_clk_en,
        clk_i => clk_i,
        reset_i => s_general_reset
    );
    -----------------------------------------------------------------------
    coast_trim_sync:sync
    Port map( 
        in_i => s_coast_trim,
        out_o => s_coast_trim_sync,
        inter_clk_en_i => s_inter_clk_en,
        clk_i => clk_i,
        reset_i => s_general_reset
    );   
    
    -----------------------------------------------------------------------
    coast_start_multiplexer:multiplexer
    Port map( 
        sel_i=>s_coast_start_trig_sel,
        in0_i=>s_soft_trig_coast_start,
        in1_i=>ext_trig_coast_start_0_i,
        in2_i=>ext_trig_coast_start_1_i,
        out_o=>s_coast_start_mux
    );
    -----------------------------------------------------------------------
    coast_stop_multiplexer:multiplexer
    Port map( 
        sel_i=>s_coast_stop_trig_sel,
        in0_i=>s_soft_trig_coast_stop,
        in1_i=>ext_trig_coast_stop_0_i,
        in2_i=>ext_trig_coast_stop_1_i,
        out_o=>s_coast_stop_mux
    ); 
    -----------------------------------------------------------------------
    prepare_multiplexer:multiplexer
    Port map( 
        sel_i=>s_prepare_trig_sel,
        in0_i=>s_soft_trig_prepare,
        in1_i=>ext_trig_prepare_0_i,
        in2_i=>ext_trig_prepare_1_i,
        out_o=>s_prepare
    );
    -----------------------------------------------------------------------
    coast_multiplexer:multiplexer
    Port map( 
        sel_i=>s_coast_trig_sel,
        in0_i=>s_soft_trig_trim,
        in1_i=>ext_trig_trim_0_i,
        in2_i=>ext_trig_trim_1_i,
        out_o=>s_coast_trim
    );
    -----------------------------------------------------------------------
    FGC_prescaler: prescaler
    Generic map(
           g_PRESCALER_VALUE => g_INTERPOLATION_VALUE
        )
    Port map( 
        inter_clk_en_o => s_inter_clk_en,
        clk_i => clk_i,
        reset_i => s_general_reset
    );
    -----------------------------------------------------------------------
    
    FGC_coast_memory: coast_memory
    generic map(
        g_FGC_CHANNELS => g_FGC_CHANNELS,
        g_S_AXI_DATA_WIDTH => g_M_AXI_DATA_WIDTH
    )
    port map(
        coast_next_vector_i => s_coast_next_vector,
        coast_vector_valid_o => s_coast_vector_valid,
        coast_slope_o => s_coast_slope,
        coast_interval_o => s_coast_interval,
        coast_mem_ready_i => s_coast_mem_ready,
        coast_mem_ready_fault_o => s_coast_mem_ready_fault,
        coast_trim_i => s_coast_trim_sync,
        coast_buff_sel_o => s_coast_buff_sel,
        coast_enable_i => s_coast_enable,
        coastMemory_Addr_i => s_coastMemory_Addr,
        coastMemory_RdData_o => s_coastMemory_RdData,
        coastMemory_WrData_i => s_coastMemory_WrData,
        coastMemory_RdMem_i => s_coastMemory_RdMem,
        coastMemory_WrMem_i => s_coastMemory_WrMem,
        coastMemory_RdDone_o => s_coastMemory_RdDone,
        coastMemory_WrDone_o => s_coastMemory_WrDone,
        clk_i => clk_i,
        reset_i => s_general_reset
    );
    -----------------------------------------------------------------------
    
    -- component instantation   
    FGC_AXI_MM_read_interface: AXI_MM_read_interface
    generic map(
        g_M_AXI_ADDR_WIDTH => g_M_AXI_ADDR_WIDTH,
        g_M_AXI_DATA_WIDTH => g_M_AXI_DATA_WIDTH
    )
    port map(
        AXI_RD_start_i => s_RD_cmd.start,
        AXI_RD_start_address_i => std_logic_vector(s_RD_cmd.address),
        AXI_RD_data_size_i => std_logic_vector(s_RD_cmd.data_size),
        AXI_RD_burst_length_i => std_logic_vector(s_RD_cmd.burst_length),
        AXI_RD_done_o => s_AXI_RD_done,
        AXI_RD_error_o => s_AXI_RD_error,
        AXI_RD_busy_o => s_AXI_RD_busy,
        AXI_RD_data_valid_o => s_AXI_RD_data_valid,
        AXI_RD_data_o => s_AXI_RD_data,
        
        M_AXI_ACLK => m_axi_clk_i,
        M_AXI_ARESETN => s_m_axi_reset_sync_n,
        M_AXI_ARID => mem_m_AXI_arid,
        M_AXI_ARADDR => mem_m_AXI_araddr,
        M_AXI_ARLEN => mem_m_AXI_arlen,
        M_AXI_ARSIZE => mem_m_AXI_arsize,
        M_AXI_ARBURST => mem_m_AXI_arburst,
        M_AXI_ARLOCK => mem_m_AXI_arlock,
        M_AXI_ARCACHE => mem_m_AXI_arcache,
        M_AXI_ARPROT => mem_m_AXI_arprot,
        M_AXI_ARQOS => mem_m_AXI_arqos,
        M_AXI_ARUSER => mem_m_AXI_aruser,
        M_AXI_ARVALID => mem_m_AXI_arvalid,
        M_AXI_ARREADY => mem_m_AXI_arready,
        M_AXI_RID => mem_m_AXI_rid,
        M_AXI_RDATA => mem_m_AXI_rdata,
        M_AXI_RRESP => mem_m_AXI_rresp,
        M_AXI_RLAST => mem_m_AXI_rlast,
        M_AXI_RUSER => mem_m_AXI_ruser,
        M_AXI_RVALID => mem_m_AXI_rvalid,
        M_AXI_RREADY => mem_m_AXI_rready
    );
    -----------------------------------------------------------------------
    process(s_funct_config)
    begin
        for I in 0 to (g_FGC_CHANNELS-1) loop
            s_buffer_enable(I) <= s_funct_config(I).enable(15);
            s_coast_enable(I)  <= s_funct_config(I).coastEnable(18);
        end loop;
    end process;
    -----------------------------------------------------------------------

    FGC_FIFO_manager: FIFO_manager
    generic map(
        g_FGC_CHANNELS => g_FGC_CHANNELS,
        g_M_AXI_DATA_WIDTH => g_M_AXI_DATA_WIDTH,
        g_BUFFER_SIZE => g_BUFFER_SIZE
    )
    Port map( 
        RD_cmd_o => s_RD_cmd,
        RD_error_o => s_AXI_RD_error,
        AXI_RD_data_valid_i => s_AXI_RD_data_valid,
        AXI_RD_data_i => s_AXI_RD_data,
        AXI_RD_done_i => s_AXI_RD_done,
        AXI_RD_busy_i => s_AXI_RD_busy,
        FIFO_wr_o => s_FIFO_wr,
        FIFO_wrdata_o => s_FIFO_wrdata,
        FIFO_wrinfo_o => s_FIFO_wrinfo,
        FIFO_prog_full_i => s_FIFO_prog_full,
        --FIFO_empty_i => s_FIFO_empty,
        FIFO_full_i => s_FIFO_full,
        FIFO_full_fault_o => open,          -- fifo full signal is not used, the used one is coming out from FGC_chan block
        FIFO_loading_finished_o => open,    -- unused
        FIFO_wr_rst_busy_i=> s_FIFO_wr_rst_busy,
        FIFO_reset_o => s_FIFO_reset,
        busy_o => s_fifo_man_busy,
        FIFO_preloading_finished_o => s_FIFO_preloading_finished,
        funct_mem_ready_fault_o => s_funct_mem_ready_fault,
        buffer_enable_i => s_buffer_enable,
        buffer_sel => s_buffer_sel,
        fgc_buffer_sel_update_o => s_fgc_buffer_sel_update,
        fgc_buffer_sel_o => s_fgc_buffer_sel,
        buffer_start_addr_i => s_buffer_start_addr,        
        buffer_start_addr_ready_i => s_bufferStartAddress_ready,
        function_size_i => s_function_size,
        function_size_ready_i => s_function_size_ready,
        funct_mem_ready_i => s_funct_mem_ready,
        prepare_i => s_prepare,
        funct_busy_i => s_funct_busy_OR,
        enable_i => s_fifo_man_en,
        clk_i => m_axi_clk_i,
        reset_i => s_m_axi_reset_sync
    ); 
    -----------------------------------------------------------------------
    interlock_o       <= s_interlock;

    FGC_fault_manager:fault_manager
    Generic map(
        g_FGC_CHANNELS => g_FGC_CHANNELS
    )
    Port map( 
        clr_faults_i => s_clr_faults,
        fault_en_0_i => s_sat_fault_en,
        fault_en_1_i => s_fifo_empty_fault_en,
        fault_en_2_i => s_fifo_full_fault_en,
        fault_en_3_i => s_cycle_mismatch_fault_en,
        fault_en_4_i => s_funct_mem_ready_fault_en,
        fault_en_5_i => s_coast_mem_ready_fault_en,
        fault_0_i => s_acc_sat_fault(g_FGC_CHANNELS-1 downto 0),
        fault_1_i => s_fifo_empty_fault(g_FGC_CHANNELS-1 downto 0),
        fault_2_i => s_fifo_full_fault_2(g_FGC_CHANNELS-1 downto 0),
        fault_3_i => s_cycle_mismatch_fault(g_FGC_CHANNELS-1 downto 0),
        fault_4_i => s_funct_mem_ready_fault_2,
        fault_5_i => s_coast_mem_ready_fault,
        fault_output_o => s_interlock,
        clk_i => clk_i,
        reset_i => s_general_reset
    );
    -----------------------------------------------------------------------
    cell_array: for FGCIndex in 0 to (g_FGC_CHANNELS-1) generate
    begin
        FGC_chan_0: FGC_chan
        generic map(
            g_FGC_CHANNELS => g_FGC_CHANNELS,
            g_M_AXI_DATA_WIDTH => g_M_AXI_DATA_WIDTH
        )
        Port map( 
            FIFO_wr_i => s_FIFO_wr(FGCIndex),
            FIFO_wrdata_i => s_FIFO_wrdata,
            FIFO_wrinfo_i => s_FIFO_wrinfo,
            FIFO_prog_empty_o => open,
            FIFO_prog_full_o => s_FIFO_prog_full(FGCIndex),
            FIFO_full_o => s_FIFO_full(FGCIndex),
            FIFO_wr_rst_busy_o=> s_FIFO_wr_rst_busy(FGCIndex),
            FIFO_reset_i => s_FIFO_reset,
            next_vector_o => s_coast_next_vector(FGCIndex),
            vector_valid_i => s_coast_vector_valid(FGCIndex),
            coast_vector_i.slope => s_coast_slope,
            coast_vector_i.interval => s_coast_interval,         
            start_value_i => s_start_value(FGCIndex),
            clear_function_after_coast_i => s_control_clearFunctsAfterCoast, 
            enable_i => s_funct_config(FGCIndex).enable(15),
            coast_enable_i => s_funct_config(FGCIndex).coastEnable(18),
            coast_start_i => s_coast_start,
            coast_stop_i => s_coast_stop,
            interpol_disable_i => s_funct_config(FGCIndex).interpolDisable(16),
            wrap_en_i => s_funct_config(FGCIndex).wrapEn(17),
            start_trig_sel_i => s_funct_config(FGCIndex).startTrigSel,
            soft_trig_start_i => s_soft_trig_start(FGCIndex),
            ext_trig_start_0_i => ext_trig_start_0_i(FGCIndex),
            ext_trig_start_1_i => ext_trig_start_1_i(FGCIndex),
            restart_trig_sel_i => s_funct_config(FGCIndex).restartTrigSel,
            soft_trig_restart_i => s_soft_trig_restart(FGCIndex),
            ext_trig_restart_0_i => ext_trig_restart_0_i(FGCIndex),
            ext_trig_restart_1_i => ext_trig_restart_1_i(FGCIndex),
            fgc_buffer_sel_update_i => s_fgc_buffer_sel_update,
            fgc_buffer_sel_i => s_fgc_buffer_sel,
            coast_trim_i => s_coast_trim,                      
            acc_sat_fault_o => s_acc_sat_fault(FGCIndex),
            fifo_empty_fault_o => s_fifo_empty_fault(FGCIndex),
            cycle_mismatch_fault_o => s_cycle_mismatch_fault(FGCIndex),
            funct_busy_o => s_funct_busy(FGCIndex),
            coast_busy_o => s_coast_busy(FGCIndex),
            coast_mode_o => s_coast_mode(FGCIndex),
            function_output_o => s_function_output(FGCIndex),          
            inter_clk_en_i => s_inter_clk_en,
            clk_i => clk_i,
            wr_clk_i => m_axi_clk_i,
            reset_i => s_general_reset
        ); 
    end generate cell_array;

    -----------------------------------------------------------------------
    s_acc_sat_fault_OR          <= '1' when unsigned(s_acc_sat_fault) /= 0 else '0';
    s_fifo_empty_fault_OR       <= '1' when unsigned(s_fifo_empty_fault) /= 0 else '0';
    s_fifo_full_fault_OR        <= '1' when unsigned(s_fifo_full_fault_2) /= 0 else '0';
    s_cycle_mismatch_fault_OR   <= '1' when unsigned(s_cycle_mismatch_fault) /= 0 else '0';
    
    s_overflow <= s_faults_SRFF(0);
    
    FGC_registers:RegCtrl_fgc 
    port map(
        Clk => clk_i,
        Rst => reset_i,
        VMEAddr => cheb_Addr,
        VMERdData => cheb_RdData,
        VMEWrData => cheb_WrData,
        VMERdMem => cheb_RdMem,
        VMEWrMem => cheb_WrMem,
        VMERdDone => cheb_RdDone,
        VMEWrDone => cheb_WrDone,
		ipInfo_ident => c_IDENT,
        ipInfo_firmwareVersion => c_FIRMWARE_VERSION,
        ipInfo_memMapVersion => c_MEM_MAP_VESRION,
        ipInfo_sysControl => open,
        control_clearFunctsAfterCoast => s_control_clearFunctsAfterCoast,
        control_softReset => s_soft_reset,
        control_clrFaults => s_clr_faults,
        control_fifoManEnable => s_fifo_man_en,
        control_functMemReady => s_funct_mem_ready,
        control_coastMemReady => s_coast_mem_ready,
        control_softTrigPrepare => s_soft_trig_prepare,
        control_softTrigTrim => s_soft_trig_trim,
        control_softTrigCoastStart => s_soft_trig_coast_start,
        control_softTrigCoastStop => s_soft_trig_coast_stop,
        control_prepareTrigSel => s_prepare_trig_sel,
        control_coastTrigSel => s_coast_trig_sel,
        control_coastStartTrigSel => s_coast_start_trig_sel,
        control_coastStopTrigSel => s_coast_stop_trig_sel,
        status_fifoManBusy => s_fifo_man_busy_2,
        status_fifoManPreloadingFinished => s_FIFO_preloading_finished,
        status_noFaults => s_interlock,
        status_noOverflow => s_overflow,
        status_coastBuffSel => s_coast_buff_sel,
        status_buffSel => s_buffer_sel,
        status_coastMode => s_coast_mode_reg,
        busy_functBusy => s_funct_busy,
        busy_coastTrimtBusy => s_coast_busy,
        faults_accSatFault => s_acc_sat_fault_OR,
        faults_fifoEmptyFault => s_fifo_empty_fault_OR,
        faults_fifoFullFault => s_FIFO_full_fault_OR,
        faults_cycleMismatchFault => s_cycle_mismatch_fault_OR,
        faults_functMemReadyFault => s_funct_mem_ready_fault_2,
        faults_coastMemReadyFault => s_coast_mem_ready_fault,
        faults_SRFF => s_faults_SRFF,
        faults_ClrSRFF => s_clr_faults,
        faultsEnable_accSatFaultEnable => s_sat_fault_en,
        faultsEnable_fifoEmptyFaultEnable => s_fifo_empty_fault_en,
        faultsEnable_fifoFullFaultEnable => s_fifo_full_fault_en,
        faultsEnable_cycleMismatchFaultEnable => s_cycle_mismatch_fault_en,
        faultsEnable_functMemReadyFaultEnable => s_funct_mem_ready_fault_en,
        faultsEnable_coastMemReadyFaultEnable => s_coast_mem_ready_fault_en,
        fifoFaults_fifoEmptyFault0 => s_fifo_empty_fault_reg(0),
        fifoFaults_fifoEmptyFault1 => s_fifo_empty_fault_reg(1),
        fifoFaults_fifoEmptyFault2 => s_fifo_empty_fault_reg(2),
        fifoFaults_fifoEmptyFault3 => s_fifo_empty_fault_reg(3),
        fifoFaults_fifoEmptyFault4 => s_fifo_empty_fault_reg(4),
        fifoFaults_fifoEmptyFault5 => s_fifo_empty_fault_reg(5),
        fifoFaults_fifoEmptyFault6 => s_fifo_empty_fault_reg(6),
        fifoFaults_fifoEmptyFault7 => s_fifo_empty_fault_reg(7),
        fifoFaults_fifoEmptyFault8 => s_fifo_empty_fault_reg(8),
        fifoFaults_fifoEmptyFault9 => s_fifo_empty_fault_reg(9),
        fifoFaults_fifoEmptyFault10 => s_fifo_empty_fault_reg(10),
        fifoFaults_fifoEmptyFault11 => s_fifo_empty_fault_reg(11),
        fifoFaults_fifoEmptyFault12 => s_fifo_empty_fault_reg(12),
        fifoFaults_fifoEmptyFault13 => s_fifo_empty_fault_reg(13),
        fifoFaults_fifoEmptyFault14 => s_fifo_empty_fault_reg(14),
        fifoFaults_fifoEmptyFault15 => s_fifo_empty_fault_reg(15),
        fifoFaults_fifoFullFault0 => s_FIFO_full_fault_reg(0),
        fifoFaults_fifoFullFault1 => s_FIFO_full_fault_reg(1),
        fifoFaults_fifoFullFault2 => s_FIFO_full_fault_reg(2),
        fifoFaults_fifoFullFault3 => s_FIFO_full_fault_reg(3),
        fifoFaults_fifoFullFault4 => s_FIFO_full_fault_reg(4),
        fifoFaults_fifoFullFault5 => s_FIFO_full_fault_reg(5),
        fifoFaults_fifoFullFault6 => s_FIFO_full_fault_reg(6),
        fifoFaults_fifoFullFault7 => s_FIFO_full_fault_reg(7),
        fifoFaults_fifoFullFault8 => s_FIFO_full_fault_reg(8),
        fifoFaults_fifoFullFault9 => s_FIFO_full_fault_reg(9),
        fifoFaults_fifoFullFault10 => s_FIFO_full_fault_reg(10),
        fifoFaults_fifoFullFault11 => s_FIFO_full_fault_reg(11),
        fifoFaults_fifoFullFault12 => s_FIFO_full_fault_reg(12),
        fifoFaults_fifoFullFault13 => s_FIFO_full_fault_reg(13),
        fifoFaults_fifoFullFault14 => s_FIFO_full_fault_reg(14),
        fifoFaults_fifoFullFault15 => s_FIFO_full_fault_reg(15),
        fifoFaults_SRFF => open,
        fifoFaults_ClrSRFF => s_clr_faults,
        functionFaults_accSatFault0 => s_acc_sat_fault_reg(0),
        functionFaults_accSatFault1 => s_acc_sat_fault_reg(1),
        functionFaults_accSatFault2 => s_acc_sat_fault_reg(2),
        functionFaults_accSatFault3 => s_acc_sat_fault_reg(3),
        functionFaults_accSatFault4 => s_acc_sat_fault_reg(4),
        functionFaults_accSatFault5 => s_acc_sat_fault_reg(5),
        functionFaults_accSatFault6 => s_acc_sat_fault_reg(6),
        functionFaults_accSatFault7 => s_acc_sat_fault_reg(7),
        functionFaults_accSatFault8 => s_acc_sat_fault_reg(8),
        functionFaults_accSatFault9 => s_acc_sat_fault_reg(9),
        functionFaults_accSatFault10 => s_acc_sat_fault_reg(10),
        functionFaults_accSatFault11 => s_acc_sat_fault_reg(11),
        functionFaults_accSatFault12 => s_acc_sat_fault_reg(12),
        functionFaults_accSatFault13 => s_acc_sat_fault_reg(13),
        functionFaults_accSatFault14 => s_acc_sat_fault_reg(14),
        functionFaults_accSatFault15 => s_acc_sat_fault_reg(15),
        functionFaults_cycleMismatchFault0 => s_cycle_mismatch_fault_reg(0),
        functionFaults_cycleMismatchFault1 => s_cycle_mismatch_fault_reg(1),
        functionFaults_cycleMismatchFault2 => s_cycle_mismatch_fault_reg(2),
        functionFaults_cycleMismatchFault3 => s_cycle_mismatch_fault_reg(3),
        functionFaults_cycleMismatchFault4 => s_cycle_mismatch_fault_reg(4),
        functionFaults_cycleMismatchFault5 => s_cycle_mismatch_fault_reg(5),
        functionFaults_cycleMismatchFault6 => s_cycle_mismatch_fault_reg(6),
        functionFaults_cycleMismatchFault7 => s_cycle_mismatch_fault_reg(7),
        functionFaults_cycleMismatchFault8 => s_cycle_mismatch_fault_reg(8),
        functionFaults_cycleMismatchFault9 => s_cycle_mismatch_fault_reg(9),
        functionFaults_cycleMismatchFault10 => s_cycle_mismatch_fault_reg(10),
        functionFaults_cycleMismatchFault11 => s_cycle_mismatch_fault_reg(11),
        functionFaults_cycleMismatchFault12 => s_cycle_mismatch_fault_reg(12),
        functionFaults_cycleMismatchFault13 => s_cycle_mismatch_fault_reg(13),
        functionFaults_cycleMismatchFault14 => s_cycle_mismatch_fault_reg(14),
        functionFaults_cycleMismatchFault15 => s_cycle_mismatch_fault_reg(15),
        functionFaults_SRFF => open,
        functionFaults_ClrSRFF => s_clr_faults,
        unsigned(functSel_functionSelect) => s_funct_sel,
        functConfig => s_funct_config_reg,
        functConfig_Sel         => s_funct_config_sel,
        functConfig_WrStrobe => s_funct_config_WrStrobe,
        functConfig_RdStrobe => s_funct_config_RdStrobe,
        offlineStartValue => s_offline_start_value_reg,
        offlineStartValue_Sel => s_offline_start_value_Sel,
        offlineStartValue_WrStrobe => s_offline_start_value_WrStrobe,
        offlineStartValue_RdStrobe => s_offline_start_value_RdStrobe,
        onlineStartValue => s_online_start_value_reg,
        functionSize(31 downto 16) => (others => '0'),
        functionSize(15 downto 0) => s_function_size_reg,
        functionSize_Sel => s_function_size_sel,
        functionSize_WrStrobe => s_function_size_WrStrobe,
        functionSize_RdStrobe => s_function_size_RdStrobe,
        currentValue => s_current_value_reg,
        softTrig_functStartTrig0 => s_soft_trig_start(0),
        softTrig_functStartTrig1 => s_soft_trig_start(1),
        softTrig_functStartTrig2 => s_soft_trig_start(2),
        softTrig_functStartTrig3 => s_soft_trig_start(3),
        softTrig_functStartTrig4 => s_soft_trig_start(4),
        softTrig_functStartTrig5 => s_soft_trig_start(5),
        softTrig_functStartTrig6 => s_soft_trig_start(6),
        softTrig_functStartTrig7 => s_soft_trig_start(7),
        softTrig_functStartTrig8 => s_soft_trig_start(8),
        softTrig_functStartTrig9 => s_soft_trig_start(9),
        softTrig_functStartTrig10 => s_soft_trig_start(10),
        softTrig_functStartTrig11 => s_soft_trig_start(11),
        softTrig_functStartTrig12 => s_soft_trig_start(12),
        softTrig_functStartTrig13 => s_soft_trig_start(13),
        softTrig_functStartTrig14 => s_soft_trig_start(14),
        softTrig_functStartTrig15 => s_soft_trig_start(15),
        softTrig_functRestartTrig0 => s_soft_trig_restart(0),
        softTrig_functRestartTrig1 => s_soft_trig_restart(1),
        softTrig_functRestartTrig2 => s_soft_trig_restart(2),
        softTrig_functRestartTrig3 => s_soft_trig_restart(3),
        softTrig_functRestartTrig4 => s_soft_trig_restart(4),
        softTrig_functRestartTrig5 => s_soft_trig_restart(5),
        softTrig_functRestartTrig6 => s_soft_trig_restart(6),
        softTrig_functRestartTrig7 => s_soft_trig_restart(7),
        softTrig_functRestartTrig8 => s_soft_trig_restart(8),
        softTrig_functRestartTrig9 => s_soft_trig_restart(9),
        softTrig_functRestartTrig10 => s_soft_trig_restart(10),
        softTrig_functRestartTrig11 => s_soft_trig_restart(11),
        softTrig_functRestartTrig12 => s_soft_trig_restart(12),
        softTrig_functRestartTrig13 => s_soft_trig_restart(13),
        softTrig_functRestartTrig14 => s_soft_trig_restart(14),
        softTrig_functRestartTrig15 => s_soft_trig_restart(15),
        bufferStartAddress => s_buffer_start_addr,
        bufferStartAddress_WrStrobe => s_bufferStartAddress_WrStrobe,
        bufferSize => std_logic_vector(to_unsigned(g_BUFFER_SIZE,32)),
        coastMemory_Sel => open,
        coastMemory_Addr => s_coastMemory_Addr,
        coastMemory_RdData => s_coastMemory_RdData,
        coastMemory_WrData => s_coastMemory_WrData,
        coastMemory_RdMem => s_coastMemory_RdMem,
        coastMemory_WrMem => s_coastMemory_WrMem,
        coastMemory_RdDone => s_coastMemory_RdDone,
        coastMemory_WrDone => s_coastMemory_WrDone,
        coastMemory_RdError => '0',
        coastMemory_WrError => '0'
    );
    --=============================================================================
    -- clock domain crossing
    --=============================================================================
    p_cdc: process(clk_i,s_general_reset)
    begin
        if(s_general_reset = '1')then
            s_sync_cnt                  <= (others => '0');
            s_sync_cnt1                 <= (others => '0');
            s_function_size_ready       <= '0';
            s_bufferStartAddress_ready  <= '0';
            s_buffer_sel_1              <= '0';
            s_buffer_sel_2              <= '0';
        elsif (rising_edge (clk_i))then
            -- clock domain synchronizer
            if(s_function_size_ready = '0')then
                s_function_size_ready <= s_function_size_WrStrobe;
            else
                if(s_sync_cnt < c_SYNC_TIME)then
                   s_sync_cnt <= s_sync_cnt + 1;
                else
                    s_function_size_ready <= '0';
                end if;                
            end if;
            
            -----------------------------------------------------------------------
            -- clock domain synchronizer
            if(s_bufferStartAddress_ready = '0')then
                s_bufferStartAddress_ready <= s_bufferStartAddress_WrStrobe;
            else
                if(s_sync_cnt1 < c_SYNC_TIME)then
                   s_sync_cnt1 <= s_sync_cnt1 + 1;
                else
                    s_bufferStartAddress_ready <= '0';
                end if;                
            end if;
            
            -----------------------------------------------------------------------
            
            s_buffer_sel_1 <= s_buffer_sel;
            s_buffer_sel_2 <= s_buffer_sel_1;
        end if;
    end process;
    --=============================================================================
    -- multiplexers for registers 
    --=============================================================================
    s_cheb_WrData <= cheb_WrData;
    
    -- start value depends of buffer selection
    -- buffer sel is inverted for the reading of the start value
    p_off_on_start_value_mux: process(s_start_value_0,s_start_value_1,s_buffer_sel_2)
    begin
        if(s_buffer_sel_2 = '0')then 
            s_offline_start_value_reg <= s_start_value_0(to_integer(s_funct_sel));
            s_online_start_value_reg  <= s_start_value_1(to_integer(s_funct_sel));
            s_start_value             <= s_start_value_1;
        else
            s_offline_start_value_reg <= s_start_value_1(to_integer(s_funct_sel));
            s_online_start_value_reg  <= s_start_value_0(to_integer(s_funct_sel));
            s_start_value             <= s_start_value_0;
        end if;
    end process;
    --=============================================================================
    
    p_mux: process(clk_i,s_general_reset)
    begin
        if(s_general_reset = '1')then
            for i in 0 to (g_FGC_CHANNELS-1) loop
                s_funct_config(i) <= c_functConfig_default;
            end loop;
            
            s_start_value_0                     <= (others => (others => '0'));
            s_start_value_1                     <= (others => (others => '0'));
            s_function_size                     <= (others => (others => '0'));
            s_funct_config_reg                  <= (others => '0');
            s_function_size_reg                 <= (others => '0');
            s_current_value_reg                 <= (others => '0');
            
        elsif (rising_edge (clk_i))then
        
            -----------------------------------------------
            -- function configuration register multiplexing
            -----------------------------------------------
            -- write access
            if(s_funct_config_WrStrobe = '1')then
                if(s_funct_sel <= g_FGC_CHANNELS-1)then -- for compiler
                    s_funct_config(to_integer(s_funct_sel)) <= f_std_to_functConfig(s_cheb_WrData);
                end if;
            end if;

            -- read access
            if(s_funct_config_Sel = '1')then
                  s_funct_config_reg <= f_functConfig_to_std(s_funct_config(to_integer(s_funct_sel)));
            end if;
            
            --------------------------------------
            -- start value register multiplexing
            --------------------------------------
            
            -- write access
            if(s_offline_start_value_WrStrobe = '1')then
                if(s_funct_sel <= g_FGC_CHANNELS-1)then -- for compiler
                    if(s_buffer_sel_2 = '0')then
                        s_start_value_0(to_integer(s_funct_sel)) <= s_cheb_WrData;
                    else
                        s_start_value_1(to_integer(s_funct_sel)) <= s_cheb_WrData;
                    end if;
                end if;
            end if;
                      
            --------------------------------------
            -- function size register multiplexing
            --------------------------------------
            
            -- write access
            if(s_function_size_WrStrobe = '1')then
                if(s_funct_sel <= g_FGC_CHANNELS-1)then -- for compiler
                    s_function_size(to_integer(s_funct_sel)) <= s_cheb_WrData(s_function_size(0)'range);
                end if;
            end if;
            
            -- read access
            if(s_function_size_sel = '1')then
                s_function_size_reg <= s_function_size(to_integer(s_funct_sel));
            end if;
            
            --------------------------------------
            -- current value register multiplexing
            --------------------------------------
            -- read access
            if(s_funct_sel <= g_FGC_CHANNELS-1)then -- for compiler
                s_current_value_reg <= s_current_value(to_integer(s_funct_sel));
            end if;
            
        end if;
    end process p_mux;
    
    --=============================================================================
    -- resynch
    --=============================================================================
    s_fifo_full_fault <= s_fifo_full;
    
    p_resynch: process(clk_i,s_general_reset)
    begin
        if(s_general_reset = '1')then
            s_funct_mem_ready_fault_1   <= '0';
            s_funct_mem_ready_fault_2   <= '0';
            s_fifo_full_fault_1         <= (others => '0');
            s_fifo_full_fault_2         <= (others => '0');
            s_fifo_man_busy_1           <= '0';
            s_fifo_man_busy_2           <= '0';
        elsif (rising_edge (clk_i))then
            s_funct_mem_ready_fault_1 <= s_funct_mem_ready_fault;
            s_funct_mem_ready_fault_2 <= s_funct_mem_ready_fault_1;
            s_fifo_full_fault_1       <= s_fifo_full_fault;
            s_fifo_full_fault_2       <= s_fifo_full_fault_1;
            s_fifo_man_busy_1         <= s_fifo_man_busy;
            s_fifo_man_busy_2         <= s_fifo_man_busy_1;
        end if;
    end process p_resynch;

    --=============================================================================
    -- OR gate 
    --=============================================================================
    p_OR_gate: process(s_funct_busy)
        variable v_funct_busy : std_logic := '0';
    begin
        v_funct_busy := '0';
        
        for I in 0 to g_FGC_CHANNELS-1 loop
            if(s_funct_busy(I) = '1')then
                v_funct_busy := '1';
            end if;
        end loop;
        
        s_funct_busy_OR <= v_funct_busy;
    end process p_OR_gate;

end arch_imp;

