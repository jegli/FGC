-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, FGC FIFO manager --
-- --
-------------------------------------------------------------------------------
--
-- unit name: FGC FIFO manager
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 11/04/2018
--
-- version: 1.0
--
-- description: the aim of this module is to load the function generator channel fifos 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

--=============================================================================
-- Entity declaration for my_entity
--=============================================================================
  entity FIFO_manager is
    generic (
            -- Function parameters
            g_FGC_CHANNELS          : integer   := 16; 
            -- size of buffer in bytes (per function)
            g_BUFFER_SIZE           : integer   := 4096; 
            -- AXI read interface width
            g_M_AXI_DATA_WIDTH      : integer   := 64;
            -- FIFO burst 
            g_FIFO_BURST            : integer   := 256
    );
    Port ( 
            -- read interfaces
            RD_cmd_o                : out   t_AXI_RD_command;
            RD_error_o              : out   std_logic;
      
            AXI_RD_data_valid_i     : in    std_logic;
            AXI_RD_data_i           : in    std_logic_vector (g_M_AXI_DATA_WIDTH-1 downto 0);
            AXI_RD_done_i           : in    std_logic;
            AXI_RD_busy_i           : in    std_logic;
            
            -- fifo interface
            FIFO_wr_o               : out   std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            FIFO_wrdata_o           : out   std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
            FIFO_wrinfo_o           : out   std_logic_vector(c_INFO_WIDTH-1 downto 0);
            FIFO_prog_full_i        : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            --FIFO_empty_i            : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            FIFO_full_i             : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            FIFO_wr_rst_busy_i      : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            FIFO_reset_o            : out   std_logic; --unused
            
            -- status interface
            FIFO_full_fault_o           : out    std_logic; 
            FIFO_loading_finished_o     : out   std_logic;
            busy_o                      : out   std_logic;
            FIFO_preloading_finished_o  : out   std_logic;
            funct_mem_ready_fault_o     : out   std_logic;
            
            -- buffer interfaces
            buffer_enable_i           : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
            buffer_start_addr_i       : in    std_logic_vector(31 downto 0);
            buffer_start_addr_ready_i : in    std_logic;
            
            buffer_sel                : out   std_logic;
            fgc_buffer_sel_update_o   : out   std_logic;
            fgc_buffer_sel_o          : out   std_logic;
            function_size_i           : in    t_FCG_ARRAY_16_STD(g_FGC_CHANNELS-1 downto 0);
            function_size_ready_i     : in    std_logic;
            
            -- control interface
            funct_mem_ready_i       : in    std_logic;
            prepare_i               : in    std_logic;
            funct_busy_i            : in    std_logic;
            enable_i                : in    std_logic;
            
            clk_i                   : in    std_logic;
            reset_i                 : in    std_logic
           ); 
end FIFO_manager;
--=============================================================================
-- architecture declaration
--=============================================================================
architecture arch_imp of FIFO_manager is

    -- internal signals definition
    signal s_FIFO_wr_data               : std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
    signal s_FIFO_info                  : std_logic_vector (c_INFO_WIDTH-1 downto 0);
    signal s_FIFO_wr                    : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_cnt                   : unsigned(f_log2(g_FGC_CHANNELS)-1 downto 0); --up to 256 functions (add generic)
    signal s_FIFO_preloading_finished   : std_logic;
    signal s_FIFO_full_fault            : std_logic;
    --signal s_FIFO_empty                 : std_logic_vector(FIFO_empty_i'range);
    
    signal s_RD_cmd                     : t_AXI_RD_command;
    signal s_RD_req                     : std_logic;
    signal s_RD_done                    : std_logic;
    signal s_RD_busy                    : std_logic;
    signal s_RD_error                   : std_logic;
  
    signal s_AXI_RD_done                : std_logic;
    
    signal s_funct_busy                 : std_logic;
    signal s_buffer_start_addr          : std_logic_vector(buffer_start_addr_i'range);
    signal s_buffer_data_cnt            : t_FCG_ARRAY_19 (g_FGC_CHANNELS-1 downto 0);
    signal s_buffer                     : t_buffer_array (g_FGC_CHANNELS-1 downto 0);
    signal s_buffer_cnt_init            : std_logic;
    signal s_buffer_end_reached         : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_buff_sel                   : std_logic;
    signal s_fgc_buffer_sel_update      : std_logic;
    signal s_fgc_buffer_sel             : std_logic;
    signal s_mem_ready                  : std_logic;
    signal s_busy                       : std_logic;
    signal s_funct_mem_ready_fault      : std_logic;
    signal s_funct_mem_ready_fault_cdc  : std_logic;
    signal s_funct_mem_ready_fault_cnt  : unsigned(5 downto 0);
    signal s_fgc_buffer_sel_update_cdc  : std_logic;
    signal s_fgc_buffer_sel_update_cnt  : unsigned(5 downto 0);
    signal s_fgc_buffer_sel_cdc         : std_logic;
    signal s_first_data                 : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_FIFO_loading_finished      : std_logic;
    
    signal s_buffer_start_addr_i        : std_logic_vector(buffer_start_addr_i'range);
    signal s_function_size_i            : t_FCG_ARRAY_16_STD(g_FGC_CHANNELS-1 downto 0);
    signal s_buffer_start_addr_ready    : std_logic;
    signal s_buffer_start_addr_ready1   : std_logic;
    signal s_function_size_ready        : std_logic;
    signal s_function_size_ready1       : std_logic;
    signal s_funct_mem_ready_1          : std_logic;
    signal s_funct_mem_ready_2          : std_logic;
    signal s_prepare_1                  : std_logic;
    signal s_prepare_2                  : std_logic;
    signal s_prepare_3                  : std_logic;
    signal s_enable_1                   : std_logic;
    signal s_enable_2                   : std_logic;
    signal s_buffer_enable_1            : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_buffer_enable_2            : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_preloading_status          : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    ----------------------------------------------------------------------
    -- constant instantation 
    constant c_ALL_REACHED                  : std_logic_vector(s_buffer_end_reached'range) := (OTHERS => '1');
    constant c_M_AXI_DATA_WIDTH_BYTES       : integer    := g_M_AXI_DATA_WIDTH/8;                           -- [byte]
    constant c_M_AXI_DATA_WIDTH_MAX         : integer    := 512;                                            -- [bit]
    constant c_M_AXI_DATA_WIDTH_MAX_SIZE    : integer    := f_log2(c_M_AXI_DATA_WIDTH_MAX);                 -- [bit]
    constant c_FIFO_BURST_TRESH_VALUE_SIZE  : integer    := f_log2(g_FIFO_BURST*c_M_AXI_DATA_WIDTH_BYTES);  -- [bit]
    constant c_MAX_FIFO_BURST_REQUEST_BYTES : integer    := g_FIFO_BURST*c_M_AXI_DATA_WIDTH_BYTES;          -- [bytes]
    constant c_CDC_VALUE                    : integer    := 10;                                             -- maximum value 63

    ----------------------------------------------------------------------
    -- MAIN STATE MACHINE
    type t_state is (   IDLE, 
                        FIFO_RESET,       
                        SCANNING,
                        WAITING
                    );
    
    signal s_main_state  : t_state; 
    ----------------------------------------------------------------------   
    -- FIFO STATE MACHINE
    type t_fifo_state is (  IDLE,        
                            REQUEST,
                            LOADING,
                            DONE
                         );
    
    signal s_fifo_state  : t_fifo_state; 
    ----------------------------------------------------------------------
    -- component instantation
    component CDC_block is
    generic(
            g_WIDTH     : positive := 1    
    );
    port(
        clk_i       : IN std_logic;
        reset_i     : IN std_logic;
        data_in_i   : IN std_logic_vector( g_WIDTH-1 downto 0);
        data_out_o  : OUT std_logic_vector( g_WIDTH-1 downto 0)
     );
    end component;
    
begin
    --=============================================================================
    -- Asynchronous
    --=============================================================================
    -- convert unsigned in std_logic_vector
    RD_cmd_o.start          <= s_RD_cmd.start;
    RD_cmd_o.burst_length   <= s_RD_cmd.burst_length;
    RD_cmd_o.address        <= s_RD_cmd.address;
    RD_cmd_o.data_size      <= s_RD_cmd.data_size;
    
    -- RD error
    RD_error_o <= s_RD_error;
    
    -- preloading finshined status bit
    FIFO_preloading_finished_o <= s_FIFO_preloading_finished;
    -- fifo loading finished
    FIFO_loading_finished_o <= s_FIFO_loading_finished;

    -- unused
    FIFO_reset_o <= '0';

    -- busy
    busy_o <= s_busy;
    
    -- buffer selection
    buffer_sel <= s_buff_sel;
    
    -- fgc buffer selection update
    fgc_buffer_sel_update_o <= s_fgc_buffer_sel_update_cdc;
    fgc_buffer_sel_o        <=  s_fgc_buffer_sel_cdc;

    -- write in fifo
    FIFO_wr_o       <= s_FIFO_wr;
    FIFO_wrdata_o   <= s_FIFO_wr_data;
    FIFO_wrinfo_o   <= s_FIFO_info;
    
    -- function memory ready fault
    funct_mem_ready_fault_o         <= s_funct_mem_ready_fault_cdc;
    -- fifo full fault
    FIFO_full_fault_o <= s_FIFO_full_fault;
    
    --=============================================================================
    -- clock domain crossing
    --=============================================================================
    -----------------------------------------------------------------------    
    CDC_start:CDC_block
     generic map(
             g_WIDTH => 1  
     )
     port map(
         clk_i          => clk_i,
         reset_i        => reset_i,
         data_in_i(0)   => funct_busy_i,
         data_out_o(0)  => s_funct_busy
      ); 
    -----------------------------------------------------------------------   
    p_CDC: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_funct_mem_ready_fault_cdc     <= '0';
            s_funct_mem_ready_fault_cnt     <= (others => '0');
            s_fgc_buffer_sel_update_cdc     <= '0';
            s_fgc_buffer_sel_cdc            <= '0';
            s_fgc_buffer_sel_update_cnt     <= (others => '0');
        elsif (rising_edge (clk_i))then
            -- function memory ready fault --
            -- fault detected
            if(s_funct_mem_ready_fault_cdc = '1')then
                --increment counter
                s_funct_mem_ready_fault_cnt <= s_funct_mem_ready_fault_cnt + 1;
                
                -- end of fault generation
                if(s_funct_mem_ready_fault_cnt >= c_CDC_VALUE)then
                    s_funct_mem_ready_fault_cdc <= '0';
                end if;
            else
                --reset cnt
                s_funct_mem_ready_fault_cnt <= (others => '0');
                -- fault detection
                s_funct_mem_ready_fault_cdc <= s_funct_mem_ready_fault;
            end if;
            
            if(s_fgc_buffer_sel_update_cdc = '1')then
                --increment counter
                s_fgc_buffer_sel_update_cnt <= s_fgc_buffer_sel_update_cnt + 1;
                
                -- end of fault generation
                if(s_fgc_buffer_sel_update_cnt >= c_CDC_VALUE)then
                    s_fgc_buffer_sel_update_cdc <= '0';
                end if;
            else
                --reset cnt
                s_fgc_buffer_sel_update_cnt <= (others => '0');
                -- fault detection
                s_fgc_buffer_sel_update_cdc <= s_fgc_buffer_sel_update;
                s_fgc_buffer_sel_cdc        <= s_fgc_buffer_sel;
            end if;
        end if;
    end process p_CDC;
    
    
    --=============================================================================
    -- resync
    --=============================================================================
    p_resync: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_AXI_RD_done <= '0';
            s_buffer_start_addr_ready   <= '0';
            s_buffer_start_addr_ready1  <= '0';
            s_function_size_ready       <= '0';
            s_function_size_ready1      <= '0';
            s_buffer_start_addr_i       <= (others => '0');
            s_function_size_i           <= (others => (others => '0'));
            s_funct_mem_ready_1         <= '0';
            s_funct_mem_ready_2         <= '0';
            s_prepare_1                 <= '0';
            s_prepare_2                 <= '0';
            s_prepare_3                 <= '0';
            s_enable_1                  <= '0';
            s_enable_2                  <= '0';
            s_buffer_enable_1           <= (others => '0');
            s_buffer_enable_2           <= (others => '0');
        elsif (rising_edge (clk_i))then
            s_AXI_RD_done <= AXI_RD_done_i;
            
            s_buffer_start_addr_ready <= buffer_start_addr_ready_i;
            s_buffer_start_addr_ready1 <= s_buffer_start_addr_ready;
            
            -- on rising edge
            if(s_buffer_start_addr_ready1 = '0' and buffer_start_addr_ready_i = '1')then
                s_buffer_start_addr_i <= buffer_start_addr_i;                       -- not resources usage optimized!
            end if;
            
            s_function_size_ready  <= function_size_ready_i;
            s_function_size_ready1 <= s_function_size_ready;
            
            -- on rising edge
            if(s_function_size_ready1 = '0' and s_function_size_ready = '1')then
                s_function_size_i <= function_size_i;                               -- not resources usage optimized!
            end if;
            
            s_funct_mem_ready_1 <= funct_mem_ready_i;
            s_funct_mem_ready_2 <= s_funct_mem_ready_1;
            
            s_prepare_1 <= prepare_i;
            s_prepare_2 <= s_prepare_1;
            s_prepare_3 <= s_prepare_2;
            
            s_enable_1 <= enable_i;
            s_enable_2 <= s_enable_1;
            
            s_buffer_enable_1 <= buffer_enable_i;
            s_buffer_enable_2 <= s_buffer_enable_1;
        end if;
    end process p_resync;
    --=============================================================================
    -- buffer selection
    --=============================================================================
    s_fgc_buffer_sel <= not s_buff_sel;
    
    p_buffer_selection: process(clk_i,reset_i,s_funct_mem_ready_2,s_prepare_2)
    begin
        if(reset_i = '1')then
            s_buff_sel                  <= '0';
            s_mem_ready                 <= '0';
            s_fgc_buffer_sel_update     <= '0';
        elsif (rising_edge (clk_i))then
            --default 
            s_fgc_buffer_sel_update   <= '0';
            
            -- buffer selection is changed on rising edge 
            if(s_funct_mem_ready_1 = '1' and s_funct_mem_ready_2 = '0')then
                s_mem_ready   <= '1';
            elsif(s_prepare_2 = '1' and s_prepare_3 = '0')then  
                s_mem_ready               <= '0';
                -- transmit buffer selection to FGC_chan blocks
                s_fgc_buffer_sel_update   <= '1';
                s_buff_sel                <= not s_buff_sel; -- changed buffer selection when funct_mem_ready is asserted
            end if;
        end if;
    end process p_buffer_selection;
    
    --=============================================================================
    -- Begin of the main state machine
    -- (state transitions)
    --=============================================================================
    
    p_main_state_machine: process(clk_i,reset_i)
        variable v_fifo_rst_busy : std_logic := '0';
    begin
        if(reset_i = '1')then
            s_main_state                <= IDLE;
            s_FIFO_cnt                  <= (OTHERS => '0');
            s_rd_req                    <= '0';
            s_buffer_cnt_init           <= '0';
            --irq_err_fifo_full_o       <= '0';
            s_busy                      <= '0';
            s_FIFO_loading_finished     <= '0';
            s_FIFO_preloading_finished  <= '0';
            s_funct_mem_ready_fault     <= '0';
            s_buffer_start_addr         <= (others => '0');
            s_first_data                <= (others => '0');
            s_preloading_status         <= (others => '0');
        elsif (rising_edge (clk_i))then
            --default
            s_rd_req                <= '0';
            s_buffer_cnt_init       <= '0';
            s_FIFO_loading_finished <= '0';
            s_funct_mem_ready_fault <= '0';
            v_fifo_rst_busy         := '0';

            -- when first data is written in the FIFO, first data flag is reset
            s_first_data            <= s_first_data and (not s_FIFO_wr);
            
            if(s_enable_2 = '1')then
                -- STATE MACHINE
                case s_main_state is
                    -- state "IDLE"
                    when IDLE =>    
                        -- default value 
                        s_FIFO_cnt          <= (others => '0');
                        s_first_data        <= (others => '0');
                        s_preloading_status <= (others => '0');
                        
                        -- rising edge of trigger
                        if(s_prepare_2 = '1' and s_prepare_3 = '0')then   
                        
                            if(s_mem_ready = '1')then
                                -- copy buffer start address and buffer size values
                                s_buffer_start_addr <= s_buffer_start_addr_i;
                                
                                -- module is busy
                                s_busy                      <= '1';
                                
                                -- initialize buffer counter value;
                                s_buffer_cnt_init   <= '1';
        
                                -- first data in fifo flag
                                s_first_data <= (others => '1');
        
                                -- change state
                                s_main_state    <= FIFO_RESET;
                            else 
                                -- fault: functions are not loaded in the buffers
                                s_funct_mem_ready_fault <= '1';
                            end if;
    
                        end if;
                    -- state "FIFO RESET"
                    when FIFO_RESET => 
                    
                        for I in 0 to (g_FGC_CHANNELS-1) loop
                            if(FIFO_wr_rst_busy_i(I) = '1' and s_buffer_enable_2(I) = '1')then
                                v_fifo_rst_busy := '1';
                            end if;
                        end loop;
    
                        -- waiting fifo reset is finished (in case FGC_chan is performing a reset)
                        if(v_fifo_rst_busy = '0')then
                            -- change state
                            s_main_state <= SCANNING;
                        end if;
                        
                    -- state "SCANNING"
                    when SCANNING =>
                        -- checking the state of each fifo, if prog full occurs -> need to refill the fifo
                        if(FIFO_prog_full_i(to_integer(s_FIFO_cnt)) = '0' AND s_buffer_enable_2(to_integer(s_FIFO_cnt)) = '1' AND s_buffer_end_reached(to_integer(s_FIFO_cnt)) = '0')then
                            -- if state machine is busy -> waits for sending request
                            if(s_RD_busy = '0')then
                                -- send request 
                                s_rd_req <= '1';
                                -- preloading status
                                s_preloading_status(to_integer(s_FIFO_cnt)) <= '1'; 
                                -- change state
                                s_main_state <= WAITING;
                            end if;
                        else    
                            if(s_FIFO_cnt >= g_FGC_CHANNELS-1)then
                                s_FIFO_cnt <= (others => '0'); 
                            else
                                s_FIFO_cnt <= s_FIFO_cnt+1; 
                            end if;    
                        end if;
                    -- state "WAITING"
                    when WAITING =>    
                        -- waiting loading is finished
                        if(s_RD_done = '1')then
                            -- return to scanning state
                            s_main_state <= SCANNING;
                            -- check next function  
                            if(s_FIFO_cnt >= g_FGC_CHANNELS-1)then
                                s_FIFO_cnt <= (others => '0'); 
                            else
                                s_FIFO_cnt <= s_FIFO_cnt+1; 
                            end if;    
    
                            -- detect when preloading is finished
                            -- preloading => it means that all buffers were preloaded one time
                            if(s_preloading_status = s_buffer_enable_2)then
                                s_FIFO_preloading_finished <= '1';
                            end if;
    
                            -- all buffer are empty 
                            if(s_buffer_end_reached = s_buffer_enable_2)then
                                s_main_state            <= IDLE;
                                s_busy                  <= '0';
                                s_FIFO_loading_finished <= '1';
                            end if;
                        end if;
                    -- other states 
                    when others =>
                        --jump to save state (ERROR?!)
                        s_main_state <= IDLE;
                    end case;
                    
                    if(s_funct_busy = '1')then
                        -- reset preloading finished status bit
                        s_FIFO_preloading_finished  <= '0';
                    end if;
                else --enable_n
                    -- reset
                    s_main_state                <= IDLE;
                    s_FIFO_cnt                  <= (OTHERS => '0');
                    s_rd_req                    <= '0';
                    s_buffer_cnt_init           <= '0';
                    --irq_err_fifo_full_o       <= '0';
                    s_busy                      <= '0';
                    s_FIFO_loading_finished     <= '0';
                    s_FIFO_preloading_finished  <= '0';
                    s_funct_mem_ready_fault     <= '0';
                    s_buffer_start_addr         <= (others => '0');
                    s_first_data                <= (others => '0');
                    s_preloading_status         <= (others => '0');
                end if;
                
                -- reset state machine when an error is detected
                if(s_FIFO_full_fault = '1' OR s_RD_error = '1')then-- OR s_FIFO_empty_fault = '1')then
                    s_main_state <= IDLE;
                end if;
        end if;
    end process p_main_state_machine;
        
    --=============================================================================
    -- Begin of the load state machine
    -- (state transitions)
    --=============================================================================         
    p_fifo_state_machine: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_RD_cmd.start          <= '0';
            s_RD_cmd.burst_length   <= (OTHERS => '0');
            s_RD_cmd.address        <= (OTHERS => '0');
            s_RD_cmd.data_size      <= (OTHERS => '0');
            s_RD_done               <= '0';
            s_RD_busy               <= '0';
            s_RD_error              <= '0';
            s_fifo_state            <= IDLE;
            s_FIFO_wr_data          <= (OTHERS => '0');
            s_FIFO_info             <= (OTHERS => '0');
            s_FIFO_wr               <= (OTHERS => '0');
            s_buffer_end_reached    <= (OTHERS => '0');
            s_FIFO_full_fault       <= '0';
        elsif (rising_edge (clk_i))then
            -- default
            s_RD_cmd.start          <= '0';
            s_RD_done               <= '0';
            s_RD_error              <= '0';
            s_FIFO_wr               <= (OTHERS => '0');
            s_FIFO_info             <= (OTHERS => '0');
            s_FIFO_wr_data          <= (OTHERS => '0');
            s_FIFO_full_fault       <= '0';
            
            if(s_enable_2 = '1')then
                case s_fifo_state is
                    --state "IDLE"
                    when IDLE =>
                        -- waiting request for main state machine
                        if(s_rd_req = '1')then
                            -- change state
                            s_fifo_state        <= REQUEST;
                            s_RD_busy           <= '1';
                        else
                            -- reset signals
                            s_RD_busy               <= '0';
                            s_fifo_state            <= IDLE;
                        end if;
                        
                    -- state "REQUEST"
                    when REQUEST =>
                            -- calculate max burst length
                            -- 256Bursts x 8Bytes = 2048bytes
                            if(s_buffer_data_cnt(to_integer(s_FIFO_cnt)) < to_unsigned(c_MAX_FIFO_BURST_REQUEST_BYTES,32) )then
                                -- prepare burst length and size of the next command
                                s_RD_cmd.burst_length     <= resize(s_buffer_data_cnt(to_integer(s_FIFO_cnt))/to_unsigned(c_M_AXI_DATA_WIDTH_BYTES,32),s_RD_cmd.burst_length'length); --max burst: 256 => 9bits
                                s_RD_cmd.data_size        <= resize(s_buffer_data_cnt(to_integer(s_FIFO_cnt)),s_RD_cmd.data_size'length); --buffer_data_cnt is smaller than (512bits/8)Bytes x 256Bursts => 16384 (15 bits needed);
                            else
                                -- max burst length(256x)
                                s_RD_cmd.burst_length     <= to_unsigned(g_FIFO_BURST,s_RD_cmd.burst_length'length);
                                s_RD_cmd.data_size        <= to_unsigned(c_MAX_FIFO_BURST_REQUEST_BYTES,s_RD_cmd.data_size'length); 
                            end if;
                            
                            s_RD_cmd.address        <= unsigned(s_buffer_start_addr) + to_unsigned(g_BUFFER_SIZE,16)*s_FIFO_cnt + s_buffer(to_integer(s_FIFO_cnt)).index;
                            
                            -- starts loading
                            s_RD_cmd.start  <= '1';
                            -- change to state "LOADING"
                            s_fifo_state <= LOADING;
                            
                    -- state "LOADING"  
                    when LOADING =>
                        -- waiting on AXI RD busy signal
                        if(AXI_RD_busy_i = '1')then
--                            if(FIFO_full_i(to_integer(s_FIFO_cnt)) = '0')then
                                s_FIFO_wr_data                      <= AXI_RD_data_i; 
                                s_FIFO_wr(to_integer(s_FIFO_cnt))   <= AXI_RD_data_valid_i;
                                s_FIFO_info                         <= (OTHERS => '0');
                                s_FIFO_info(c_CYCLE_NBR)            <= not s_buff_sel; -- !!buffer selection is switched at the beginning of the process!!
                                s_FIFO_info(c_FUNCT_START)          <= s_first_data(to_integer(s_FIFO_cnt));
--                            else
--                                --fifo full error interrupt
--                                s_FIFO_full_fault <= '1';
                                
--                                -- change to state "LOADING"
--                                s_fifo_state        <= IDLE;
--                            end if;
                        end if;
                        -- on rising edge of AXI RD_done input    
                        if(s_AXI_RD_done = '0' AND AXI_RD_done_i = '1')then
                            -- check the status of the counter
                            if(unsigned(s_buffer_data_cnt(to_integer(s_FIFO_cnt))) = 0)then
                                -- if 0, all vectors were read in the buffer
                                s_buffer_end_reached(to_integer(s_FIFO_cnt)) <= '1';
                            end if;
                            
                            -- change to state "DONE"
                            s_fifo_state        <= DONE;
                        end if;
                        
                    -- state "DONE"  
                    when DONE =>
                        if(s_RD_busy = '1')then
                            -- back to initial state
                            s_fifo_state        <= IDLE;
                            s_RD_busy           <= '0';
                            s_RD_done           <= '1';
                            
                            -- if all buffer were loaded in fifos -> write a info data in fifos to indicate the end of the buffer (function)
                            if(s_buffer_end_reached = s_buffer_enable_2)then
                                s_FIFO_wr_data              <= (OTHERS => '0');
                                s_FIFO_info                 <= (OTHERS => '0');
                                s_FIFO_info(c_CYCLE_NBR)    <= not s_buff_sel; -- !!buffer selection is switched at the beginning of the process!!
                                s_FIFO_info(c_FUNCT_END)    <= '1';
                                s_FIFO_wr                   <= (OTHERS => '1');
                            end if;
                        else
                            s_RD_error          <= '1';
                            s_fifo_state        <= IDLE;
                        end if;
                        
                    -- others states
                    when others =>
                        s_fifo_state        <= IDLE;
                end case;
            else --enable_n
                -- reset
                s_RD_cmd.start          <= '0';
                s_RD_cmd.burst_length   <= (OTHERS => '0');
                s_RD_cmd.address        <= (OTHERS => '0');
                s_RD_cmd.data_size      <= (OTHERS => '0');
                s_RD_done               <= '0';
                s_RD_busy               <= '0';
                s_RD_error              <= '0';
                s_fifo_state            <= IDLE;
                s_FIFO_wr_data          <= (OTHERS => '0');
                s_FIFO_info             <= (OTHERS => '0');
                s_FIFO_wr               <= (OTHERS => '0');
                s_buffer_end_reached    <= (OTHERS => '0');
                s_FIFO_full_fault       <= '0';
            end if;
            
            -- when loading finished -> reset buffer end reached
            if(s_FIFO_loading_finished = '1')then
                s_buffer_end_reached        <= (OTHERS => '0');
            end if; 
            
            --fifo full error detection mechanism
            if(unsigned(FIFO_full_i) /= 0)then
                s_FIFO_full_fault <= '1';
                s_fifo_state    <= IDLE;
            end if;
        end if;
    end process p_fifo_state_machine;
    
    --=============================================================================
    -- Begin of the process "Buffer index updater"
    -- Update the buffer counter 
    --=============================================================================
    p_buffer_cnt_updater: process(clk_i,reset_i, s_RD_cmd.start, s_RD_cmd.data_size)
    begin
        if(reset_i = '1')then
            s_buffer_data_cnt   <= (OTHERS => (OTHERS => '0'));
            
            for I in 0 to (g_FGC_CHANNELS-1) loop
                s_buffer(I).index   <= (OTHERS => '0');
            end loop;
            
        elsif (rising_edge (clk_i))then
            if(s_enable_2 = '1')then
                -----------------------------------------------------------------
                -- wait buffer cnt init signal
                if(s_buffer_cnt_init = '1')then
                    --copy buffer length for each function
                    for I in 0 to (g_FGC_CHANNELS-1) loop
                        -- initialize the buffer counter for each function 
                        s_buffer_data_cnt(I)  <= resize(unsigned(s_function_size_i(I)) * c_M_AXI_DATA_WIDTH_BYTES,s_buffer_data_cnt(I)'length);
                        s_buffer(I).index     <= (OTHERS => '0');
                    end loop;
                
                -----------------------------------------------------------------
                -- update buffer counter value.
                elsif(s_RD_cmd.start = '1')then
                    -- update buffer index of the function
                    s_buffer(to_integer(s_FIFO_cnt)).index      <= s_buffer(to_integer(s_FIFO_cnt)).index + unsigned(s_RD_cmd.data_size);
                    s_buffer_data_cnt(to_integer(s_FIFO_cnt))   <= s_buffer_data_cnt(to_integer(s_FIFO_cnt)) - unsigned(s_RD_cmd.data_size(s_buffer_data_cnt(0)'range));
                end if;
            else --enable_n
                --reset
                 s_buffer_data_cnt   <= (OTHERS => (OTHERS => '0'));
                
                for I in 0 to (g_FGC_CHANNELS-1) loop
                    s_buffer(I).index   <= (OTHERS => '0');
                end loop;
            end if;
            -----------------------------------------------------------------
        end if;
    end process p_buffer_cnt_updater;
     
end arch_imp;
