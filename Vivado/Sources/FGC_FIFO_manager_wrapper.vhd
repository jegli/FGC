-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, FGC FIFO manager wrapper--
-- --
-------------------------------------------------------------------------------
--
-- unit name: FGC FIFO manager
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 11/04/2018
--
-- version: 1.0
--
-- description: wrapper around FCG FIFO manager
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FCG_PACK.all;

--=============================================================================
-- Entity declaration for my_entity
--=============================================================================
entity FGC_FIFO_manager_wrapper is
	generic (
            -- Function parameters
            g_FGC_CHANNELS          : integer   := 16; 
            --Buffer length parameters
            g_BUFFER_LENGTH_WIDTH   : integer   := 32; 
            -- AXI read interface width
            g_M_AXI_DATA_WIDTH      : integer   := 64;
            -- FIFO burst 
            g_FIFO_BURST            : integer   := 256;
            -- FIFO info data width
            g_FIFO_INFO_DATA_WIDTH  : integer   := 8
    );
    Port ( 
            -- read interfaces
            RD_cmd_start_o          : out   std_logic;
			RD_cmd_burst_length_o   : out   std_logic_vector (8 downto 0);
			RD_cmd_address_o        : out   std_logic_vector (31 downto 0);
			RD_cmd_data_size_o      : out 	std_logic_vector (31 downto 0); 
			RD_error_o              : out   std_logic;

            AXI_RD_data_valid_i     : in    std_logic;
            AXI_RD_data_i           : in    std_logic_vector (g_M_AXI_DATA_WIDTH-1 downto 0);
            AXI_RD_done_i           : in    std_logic;
            AXI_RD_busy_i           : in    std_logic;
            
            -- status interface
            fifo_full_fault_i           : in    std_logic;
            loading_finished_o          : out   std_logic;
            busy_o                      : out   std_logic;
            FIFO_preloading_finished_o  : out   std_logic;
            funct_mem_ready_fault_o     : out   std_logic;
            
            -- buffer interfaces
            buffer_enabe_i          : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
			buffer_sel              : out   std_logic;
            buffer_start_addr_i     : in    std_logic_vector(31 downto 0);
            function_size_i         : in    t_FCG_ARRAY_32(g_FGC_CHANNELS-1 downto 0);
            
            -- control interface
            funct_mem_ready         : in    std_logic;
            prepare_i               : in    std_logic;
            enable_i                : in    std_logic;
            
            clk_i                   : in    std_logic;
            reset_i                 : in    std_logic
           ); 
end FGC_FIFO_manager_wrapper;
--=============================================================================
-- architecture declaration
--=============================================================================
architecture structure of FGC_FIFO_manager_wrapper is

	-- component instantiation 
	component FGC_FIFO_manager is
		generic (
                -- Function parameters
                g_FGC_CHANNELS        : integer  := 16; 
                --Buffer length parameters
                g_BUFFER_LENGTH_WIDTH : integer  := 32; 
                -- AXI read interface width
                g_M_AXI_DATA_WIDTH    : integer := 64;
                -- FIFO burst 
                g_FIFO_BURST          : integer    := 256
		);
		Port ( 
				-- FIFO interfaces
				FIFO_empty_o            : out       std_logic_vector (g_FGC_CHANNELS-1 downto 0);
				FIFO_rddata_o           : out       t_FCG_ARRAY_64(g_FGC_CHANNELS-1 downto 0);
				FIFO_rdinfo_o           : out       t_FCG_ARRAY_08(g_FGC_CHANNELS-1 downto 0);
				FIFO_rd_i               : in        std_logic_vector (g_FGC_CHANNELS-1 downto 0);
				FIFO_rdvalid_o          : out       std_logic_vector (g_FGC_CHANNELS-1 downto 0);
				
				-- read interfaces
				RD_cmd_o                : out       t_AXI_command_std;
				RD_error_o              : out       std_logic;

				AXI_RD_data_valid_i     : in        std_logic;
				AXI_RD_data_i           : in        std_logic_vector (g_M_AXI_DATA_WIDTH-1 downto 0);
				AXI_RD_done_i           : in        std_logic;
				AXI_RD_busy_i           : in        std_logic;
				
                irq_err_fifo_full_o         : out       std_logic;
                irq_err_fifo_empty_o        : out       std_logic;
                busy_o                      : out       std_logic;
                irq_loading_finished_o      : out       std_logic;
                FIFO_preloading_finished_o  : out       std_logic;
				
				-- buffer interfaces
				buffer_i                : in        t_func_array(g_FGC_CHANNELS-1 downto 0);
				buffer_start_addr_i     : in        std_logic_vector(31 downto 0);
				buffer_size_i           : in        std_logic_vector(31 downto 0);
				
				trigger_i               : in        std_logic;
				enable_i                : in        std_logic;
				clk_i                   : in        std_logic;
				reset_i                 : in        std_logic
			   ); 
	end component FGC_FIFO_manager;
	
	-- internal signals definition
	signal s_FIFO_rddata	: t_FCG_ARRAY_64(g_FGC_CHANNELS-1 downto 0);
	signal s_FIFO_rdinfo    : t_FCG_ARRAY_08(g_FGC_CHANNELS-1 downto 0);
	
	signal s_buffer         : t_func_array(g_FGC_CHANNELS-1 downto 0);

begin
	-- Instantiation of FGC FIFO manager wrapper
    FCG_FIFO_manager_wrapper:FGC_FIFO_manager
	generic map (
				g_FGC_CHANNELS          => g_FGC_CHANNELS,
				g_BUFFER_LENGTH_WIDTH   => g_BUFFER_LENGTH_WIDTH,
				g_M_AXI_DATA_WIDTH      => g_M_AXI_DATA_WIDTH,
				g_FIFO_BURST            => g_FIFO_BURST
	)
    port map( 
				FIFO_empty_o                => FIFO_empty_o,
				FIFO_rddata_o               => s_FIFO_rddata,
				FIFO_rdinfo_o               => s_FIFO_rdinfo,
				FIFO_rd_i                   => FIFO_rd_i,
				FIFO_rdvalid_o              => FIFO_rdvalid_o,
				RD_cmd_o.start              => RD_cmd_start_o,
                RD_cmd_o.burst_length       => RD_cmd_burst_length_o,
                RD_cmd_o.address            => RD_cmd_address_o,
                RD_cmd_o.data_size          => RD_cmd_data_size_o,
                RD_error_o                  => RD_error_o,
				AXI_RD_data_valid_i         => AXI_RD_data_valid_i,
				AXI_RD_data_i               => AXI_RD_data_i,
				AXI_RD_done_i               => AXI_RD_done_i,
				AXI_RD_busy_i               => AXI_RD_busy_i,
				irq_err_fifo_full_o         => irq_err_fifo_full_o,
				irq_err_fifo_empty_o        => irq_err_fifo_empty_o,
				busy_o                      => busy_o,
				irq_loading_finished_o      => irq_loading_finished_o,
				FIFO_preloading_finished_o  => FIFO_preloading_finished_o,
				buffer_i                    => s_buffer,
				buffer_start_addr_i         => buffer_start_addr_i,
				buffer_size_i               => buffer_size_i,
				trigger_i                   => trigger_i,
				enable_i                    => enable_i,
				clk_i                       => clk_i,
				reset_i                     => reset_i
            );
            
    -- asychronous
    FIFO_rddata_0_o <= s_FIFO_rddata(0) when g_FGC_CHANNELS > 0 else (OTHERS => '0');
    FIFO_rddata_1_o <= s_FIFO_rddata(1) when g_FGC_CHANNELS > 1 else (OTHERS => '0');
    FIFO_rddata_2_o <= s_FIFO_rddata(2) when g_FGC_CHANNELS > 2 else (OTHERS => '0');
    FIFO_rddata_3_o <= s_FIFO_rddata(3) when g_FGC_CHANNELS > 3 else (OTHERS => '0');
    FIFO_rddata_4_o <= s_FIFO_rddata(4) when g_FGC_CHANNELS > 4 else (OTHERS => '0');
    FIFO_rddata_5_o <= s_FIFO_rddata(5) when g_FGC_CHANNELS > 5 else (OTHERS => '0');
    FIFO_rddata_6_o <= s_FIFO_rddata(6) when g_FGC_CHANNELS > 6 else (OTHERS => '0');
    FIFO_rddata_7_o <= s_FIFO_rddata(7) when g_FGC_CHANNELS > 7 else (OTHERS => '0');
    FIFO_rddata_8_o <= s_FIFO_rddata(8) when g_FGC_CHANNELS > 8 else (OTHERS => '0');
    FIFO_rddata_9_o <= s_FIFO_rddata(9) when g_FGC_CHANNELS > 9 else (OTHERS => '0');
    FIFO_rddata_10_o <= s_FIFO_rddata(10) when g_FGC_CHANNELS > 10 else (OTHERS => '0');
    FIFO_rddata_11_o <= s_FIFO_rddata(11) when g_FGC_CHANNELS > 11 else (OTHERS => '0');
    FIFO_rddata_12_o <= s_FIFO_rddata(12) when g_FGC_CHANNELS > 12 else (OTHERS => '0');
    FIFO_rddata_13_o <= s_FIFO_rddata(13) when g_FGC_CHANNELS > 13 else (OTHERS => '0');
    FIFO_rddata_14_o <= s_FIFO_rddata(14) when g_FGC_CHANNELS > 14 else (OTHERS => '0');
    FIFO_rddata_15_o <= s_FIFO_rddata(15) when g_FGC_CHANNELS > 15 else (OTHERS => '0');

    FIFO_rdinfo_0_o <= s_FIFO_rdinfo(0) when g_FGC_CHANNELS > 0 else (OTHERS => '0');
    FIFO_rdinfo_1_o <= s_FIFO_rdinfo(1) when g_FGC_CHANNELS > 1 else (OTHERS => '0');
    FIFO_rdinfo_2_o <= s_FIFO_rdinfo(2) when g_FGC_CHANNELS > 2 else (OTHERS => '0');
    FIFO_rdinfo_3_o <= s_FIFO_rdinfo(3) when g_FGC_CHANNELS > 3 else (OTHERS => '0');
    FIFO_rdinfo_4_o <= s_FIFO_rdinfo(4) when g_FGC_CHANNELS > 4 else (OTHERS => '0');
    FIFO_rdinfo_5_o <= s_FIFO_rdinfo(5) when g_FGC_CHANNELS > 5 else (OTHERS => '0');
    FIFO_rdinfo_6_o <= s_FIFO_rdinfo(6) when g_FGC_CHANNELS > 6 else (OTHERS => '0');
    FIFO_rdinfo_7_o <= s_FIFO_rdinfo(7) when g_FGC_CHANNELS > 7 else (OTHERS => '0');
    FIFO_rdinfo_8_o <= s_FIFO_rdinfo(8) when g_FGC_CHANNELS > 8 else (OTHERS => '0');
    FIFO_rdinfo_9_o <= s_FIFO_rdinfo(9) when g_FGC_CHANNELS > 9 else (OTHERS => '0');
    FIFO_rdinfo_10_o <= s_FIFO_rdinfo(10) when g_FGC_CHANNELS > 10 else (OTHERS => '0');
    FIFO_rdinfo_11_o <= s_FIFO_rdinfo(11) when g_FGC_CHANNELS > 11 else (OTHERS => '0');
    FIFO_rdinfo_12_o <= s_FIFO_rdinfo(12) when g_FGC_CHANNELS > 12 else (OTHERS => '0');
    FIFO_rdinfo_13_o <= s_FIFO_rdinfo(13) when g_FGC_CHANNELS > 13 else (OTHERS => '0');
    FIFO_rdinfo_14_o <= s_FIFO_rdinfo(14) when g_FGC_CHANNELS > 14 else (OTHERS => '0');
    FIFO_rdinfo_15_o <= s_FIFO_rdinfo(15) when g_FGC_CHANNELS > 15 else (OTHERS => '0');

    s_buffer(0).enable <=  buffer_enabe_0_i when g_FGC_CHANNELS > 0 else '0';
    s_buffer(1).enable <=  buffer_enabe_1_i when g_FGC_CHANNELS > 1 else '0';
    s_buffer(2).enable <=  buffer_enabe_2_i when g_FGC_CHANNELS > 2 else '0';
    s_buffer(3).enable <=  buffer_enabe_3_i when g_FGC_CHANNELS > 3 else '0';
    s_buffer(4).enable <=  buffer_enabe_4_i when g_FGC_CHANNELS > 4 else '0';
    s_buffer(5).enable <=  buffer_enabe_5_i when g_FGC_CHANNELS > 5 else '0';
    s_buffer(6).enable <=  buffer_enabe_6_i when g_FGC_CHANNELS > 6 else '0';
    s_buffer(7).enable <=  buffer_enabe_7_i when g_FGC_CHANNELS > 7 else '0';
    s_buffer(8).enable <=  buffer_enabe_8_i when g_FGC_CHANNELS > 8 else '0';
    s_buffer(9).enable <=  buffer_enabe_9_i when g_FGC_CHANNELS > 9 else '0';
    s_buffer(10).enable <=  buffer_enabe_10_i when g_FGC_CHANNELS > 10 else '0';
    s_buffer(11).enable <=  buffer_enabe_11_i when g_FGC_CHANNELS > 11 else '0';
    s_buffer(12).enable <=  buffer_enabe_12_i when g_FGC_CHANNELS > 12 else '0';
    s_buffer(13).enable <=  buffer_enabe_13_i when g_FGC_CHANNELS > 13 else '0';
    s_buffer(14).enable <=  buffer_enabe_14_i when g_FGC_CHANNELS > 14 else '0';
    s_buffer(15).enable <=  buffer_enabe_15_i when g_FGC_CHANNELS > 15 else '0';
    
    s_buffer(0).length <=  buffer_length_0_i when g_FGC_CHANNELS > 0 else (OTHERS => '0');
    s_buffer(1).length <=  buffer_length_1_i when g_FGC_CHANNELS > 1 else (OTHERS => '0');
    s_buffer(2).length <=  buffer_length_2_i when g_FGC_CHANNELS > 2 else (OTHERS => '0');
    s_buffer(3).length <=  buffer_length_3_i when g_FGC_CHANNELS > 3 else (OTHERS => '0');
    s_buffer(4).length <=  buffer_length_4_i when g_FGC_CHANNELS > 4 else (OTHERS => '0');
    s_buffer(5).length <=  buffer_length_5_i when g_FGC_CHANNELS > 5 else (OTHERS => '0');
    s_buffer(6).length <=  buffer_length_6_i when g_FGC_CHANNELS > 6 else (OTHERS => '0');
    s_buffer(7).length <=  buffer_length_7_i when g_FGC_CHANNELS > 7 else (OTHERS => '0');
    s_buffer(8).length <=  buffer_length_8_i when g_FGC_CHANNELS > 8 else (OTHERS => '0');
    s_buffer(9).length <=  buffer_length_9_i when g_FGC_CHANNELS > 9 else (OTHERS => '0');
    s_buffer(10).length <=  buffer_length_10_i when g_FGC_CHANNELS > 10 else (OTHERS => '0');
    s_buffer(11).length <=  buffer_length_11_i when g_FGC_CHANNELS > 11 else (OTHERS => '0');
    s_buffer(12).length <=  buffer_length_12_i when g_FGC_CHANNELS > 12 else (OTHERS => '0');
    s_buffer(13).length <=  buffer_length_13_i when g_FGC_CHANNELS > 13 else (OTHERS => '0');
    s_buffer(14).length <=  buffer_length_14_i when g_FGC_CHANNELS > 14 else (OTHERS => '0');
    s_buffer(15).length <=  buffer_length_15_i when g_FGC_CHANNELS > 15 else (OTHERS => '0');

end structure;