-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, FGC_chan --
-- --
-------------------------------------------------------------------------------
--
-- unit name: FGC_chan
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 11/04/2018
--
-- version: 1.0
--
-- description: FGC channel, generate the function and the coast function for one channel
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

--=============================================================================
-- Entity declaration for my_entity
--=============================================================================
entity FGC_chan is
	generic (
            -- Function parameters
            g_FGC_CHANNELS          : integer  := 16; 
            -- AXI interface width
            g_M_AXI_DATA_WIDTH      : integer  := 64;
            -- FIFO info data width
            g_FIFO_INFO_DATA_WIDTH  : integer := 8
    );
    Port ( 
            -- FIFO interfaces to fifo manager block
            FIFO_wr_i                   : in        std_logic;                                              -- fifo write
            FIFO_wrdata_i               : in        std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);        -- fifo write data 
            FIFO_wrinfo_i               : in        std_logic_vector(g_FIFO_INFO_DATA_WIDTH-1 downto 0);    -- fifo write data information 
            FIFO_prog_empty_o           : out       std_logic;                                              -- fifo programmable empty
            FIFO_prog_full_o            : out       std_logic;                                              -- fifo programmable full
            FIFO_full_o                 : out       std_logic;                                              -- fifo full
            FIFO_wr_rst_busy_o          : out       std_logic;                                              -- fifo wr reset is busy
            FIFO_reset_i                : in        std_logic;                                              -- fifo reset (unused)
            
            -- RAM interface to RAM access manager block
            next_vector_o               : out       std_logic;
            vector_valid_i              : in        std_logic;
            coast_vector_i              : in        t_vector_format;                
            
            -- control interface
            start_value_i                   : in        std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);
            enable_i                        : in        std_logic;
            coast_enable_i                  : in        std_logic;
            coast_start_i                   : in        std_logic;
            coast_stop_i                    : in        std_logic;
            interpol_disable_i              : in        std_logic;
            wrap_en_i                       : in        std_logic;
            clear_function_after_coast_i    : in        std_logic;
            
            -- triggers
            start_trig_sel_i            : in        std_logic_vector(1 downto 0);
            soft_trig_start_i           : in        std_logic;
            ext_trig_start_0_i          : in        std_logic;
            ext_trig_start_1_i          : in        std_logic;
            
            restart_trig_sel_i          : in        std_logic_vector(1 downto 0);
            soft_trig_restart_i         : in        std_logic;
            ext_trig_restart_0_i        : in        std_logic;
            ext_trig_restart_1_i        : in        std_logic;
            
            coast_trim_i                : in        std_logic;
            
            --buffer selection
            fgc_buffer_sel_update_i     : in    std_logic;                      -- buffer selection update from fifo manager
            fgc_buffer_sel_i            : in    std_logic;                      -- buffer selection value
            
            -- fault/status interface         
            acc_sat_fault_o             : out       std_logic;
            fifo_empty_fault_o          : out       std_logic;
            cycle_mismatch_fault_o      : out       std_logic;
            funct_busy_o                : out       std_logic;
            coast_busy_o                : out       std_logic;
            coast_mode_o                : out       std_logic;
            
            -- function
            function_output_o           : out       std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);

            inter_clk_en_i              : in        std_logic;
            clk_i                       : in        std_logic;  -- general clock
            wr_clk_i                    : in        std_logic;  -- fifo wr clock
            reset_i                     : in        std_logic   -- general reset
           ); 
end FGC_chan;
--=============================================================================
-- architecture declaration
--=============================================================================
architecture arch_imp of FGC_chan is
    ----------------------------------------------------------------------
    -- constant instantation 
    
    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_FIFO_rd                    : std_logic;
    signal s_FIFO_vector                : t_vector_format;
    signal s_FIFO_rdinfo                : std_logic_vector (g_FIFO_INFO_DATA_WIDTH-1 downto 0);
    signal s_FIFO_empty                 : std_logic;
    signal s_FIFO_rdvalid               : std_logic;
    signal s_start                      : std_logic;
    signal s_start_sync                 : std_logic;
    signal s_restart                    : std_logic;
    signal s_restart_sync               : std_logic;
    signal s_coast_mode                 : std_logic;
    signal s_load_start_value           : std_logic;
    signal s_accu_start_value           : std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);
    signal s_slope                      : std_logic_vector(c_SLOPE_WIDTH-1 downto 0);     
    signal s_slope_en                   : std_logic;
    signal s_coast_trim                 : std_logic;
    signal s_coast_slope                : std_logic_vector(c_SLOPE_WIDTH-1 downto 0);   
    signal s_coast_slope_en             : std_logic;
    signal s_funct_sat                  : std_logic;
    signal s_function                   : std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);
    signal s_coast                      : std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);
    signal s_coast_sat                  : std_logic;
    signal s_adder_sat                  : std_logic;
    signal s_fgc_reset                  : std_logic;
    signal s_internal_reset             : std_logic;
    ----------------------------------------------------------------------
    -- state machine instantation
    
    -------------------------------------------------------------
    -- component instantation 
    component fifo_generator_0 is
        port (
                wr_clk          : in        std_logic;
                rd_clk          : in        std_logic;
                srst            : in        std_logic;
                din             : in        std_logic_vector(71 downto 0);
                wr_en           : in        std_logic;
                rd_en           : in        std_logic;
                dout            : out       std_logic_vector(71 downto 0);
                full            : out       std_logic;
                empty           : out       std_logic;
                valid           : out       std_logic;
                prog_empty      : out       std_logic;
                prog_full       : out       std_logic;
                wr_rst_busy     : out       std_logic;
                rd_rst_busy     : out       std_logic
        );
    end component fifo_generator_0;
    -----------------------------------------------------------------------
    component function_state_machine is
        Port ( 
            -- control interface
            enable_i                        : in    std_logic;  -- general enable
            start_i                         : in    std_logic;  -- start trigger input
            restart_i                       : in    std_logic;  -- restart trigger input
            coast_enable_i                  : in    std_logic;  -- coast enable bit
            coast_start_i                   : in    std_logic;  -- coast start trigger bit
            coast_stop_i                    : in    std_logic;  -- coast stop trigger bit
            coast_mode_o                    : out   std_logic;  -- coast mode output
            interpol_disable_i              : in    std_logic;  -- disable accumulator interpolation (if '1', cartesion coordinate format is applied)
            clear_function_after_coast_i    : in    std_logic;  -- reset the function (fifo, accu, ...) after end of coast
            
            -- accumulator interface
            start_value_i               : in    std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);   -- start value for the accumulator (from register)
            load_start_value_o          : out   std_logic;                                          -- load the start value of the accumulator
            accu_start_value_o          : out   std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);   -- start value for the accumulator (to accumulator)
            slope_o                     : out   std_logic_vector(c_SLOPE_WIDTH-1 downto 0);         -- slope increment value
            slope_en_o                  : out   std_logic;                                          -- slope increment enable bit
            fgc_reset_o                 : out   std_logic;                                          -- accumulator and fifo reset
            
            -- FIFO interface
            fifo_rd_o                   : out   std_logic;                                      -- fifo read request
            fifo_empty_i                : in    std_logic;                                      -- fifo empty stauts
            fifo_rd_info_i              : in    std_logic_vector (c_INFO_WIDTH-1 downto 0);     -- fifo read information 
            fifo_vector_i               : in    t_vector_format;                                -- fifo vector values (slope, interval)
            fifo_rd_valid_i             : in    std_logic;                                      -- fifo vector values are valid
            
            --buffer selection
            fgc_buffer_sel_update_i     : in    std_logic;                      -- buffer selection update from fifo manager
            fgc_buffer_sel_i            : in    std_logic;                      -- buffer selection value
            
            -- status interface
            funct_busy_o                : out   std_logic;                      -- busy signal, 1 = the function generation is running
            fifo_empty_fault_o          : out   std_logic;                      -- fifo emty fault detected
            cycle_mismatch_fault_o      : out   std_logic;                      -- cycle mismatch fault detected
            
            inter_clk_en_i              : in    std_logic;                      -- interpolation clock
            clk_i                       : in    std_logic;                      -- general clock
            reset_i                     : in    std_logic                       -- reset clock (active High)
        );
    end component function_state_machine;
    -----------------------------------------------------------------------
    component coast_state_machine is
        Port ( 
                -- control interface
                coast_mode_i                : in    std_logic;  -- coast mode enable input
                coast_trim_i                : in    std_logic;  -- coast trim trigger
                
                -- accumulator interface
                slope_o                     : out   std_logic_vector(31 downto 0);  -- slope increment value
                slope_en_o                  : out   std_logic;                      -- slope increment enable
                
                -- RAM interface
                next_vector_o               : out   std_logic;          -- next vector request
                vector_valid_i              : in    std_logic;          -- vector values are valid 
                vector_i                    : in    t_vector_format;    -- vector values
                
                -- Status
                coast_busy_o                : out   std_logic;  -- coast busy status bit      
                
                -- general
                inter_clk_en_i              : in    std_logic;  -- interpolation clock
                clk_i                       : in    std_logic;  -- general clock
                reset_i                     : in    std_logic   -- general reset
              );
    end component coast_state_machine;
    -----------------------------------------------------------------------
    component accumulator is
        Port ( 
                -- control interface
                wrap_en_i                   : in    std_logic;  --wrapper enable, 1= counter wrapping enabeld, 0=saturation mode enabled
                
                -- status interface
                saturation_o                : out   std_logic;  --saturation counter bit
                
                -- accumulator interface
                load_start_value_i          : in   std_logic;                                           -- load start value control bit
                start_value_i               : in   std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);       -- start value of the accumulator
                slope_i                     : in   std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);       -- increment of the counter
                slope_en_i                  : in   std_logic;                                           -- increment enable 
                dout_o                      : out  std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);       -- accumuator output value
                
                -- general
                inter_clk_en_i              : in    std_logic;  -- accumulator interpolation clock
                clk_i                       : in    std_logic;  -- general clock
                reset_i                     : in    std_logic   -- general reset
              );
    end component accumulator;
    -----------------------------------------------------------------------
    component adder is
        generic(
                    -- adder width
                    g_WIDTH         : integer := 32
        );
        Port ( 
                -- control interface
                wrap_en_i                   : in    std_logic;  --wrapper enable, 1= counter wrapping enabeld, 0=saturation mode enabled
                
                -- status interface
                saturation_o                : out   std_logic;  --saturation counter bit
                
                -- adder interface
                din_0_i                     : in   std_logic_vector(g_WIDTH-1 downto 0);        -- adder input 0
                din_1_i                     : in   std_logic_vector(g_WIDTH-1 downto 0);        -- adder input 1
                dout_o                      : out  std_logic_vector(g_WIDTH-1 downto 0);        -- adder truncated output value
                
                -- general
                clk_i                       : in    std_logic;  -- general clock
                reset_i                     : in    std_logic   -- general reset
              );
    end component adder;
    -----------------------------------------------------------------------
    component multiplexer is
        Port ( 
                sel_i   : in    std_logic_vector(1 downto 0);
                --input
                in0_i    : in    std_logic;
                in1_i    : in    std_logic;
                in2_i    : in    std_logic;
                --ouput
                out_o   : out   std_logic
              );
    end component multiplexer;
    -----------------------------------------------------------------------
    component sync is
        Port ( 
    
                in_i                : in    std_logic;  --input
                out_o               : out   std_logic;  --output
                
                -- general
                inter_clk_en_i      : in    std_logic;  -- accumulator interpolation clock
                clk_i               : in    std_logic;  -- general clock
                reset_i             : in    std_logic   -- general reset
              );
    end component sync;
    -----------------------------------------------------------------------
begin
    s_internal_reset <= reset_i or s_fgc_reset;
    coast_mode_o <= s_coast_mode;
     
    --=============================================================================
    -- FIFO instantations
    --=============================================================================
    FCG_chan_fifo_0:fifo_generator_0
    port map( 
                rd_clk => clk_i,
                wr_clk=>wr_clk_i,
                srst => s_internal_reset,
                din(63 downto 0) => FIFO_wrdata_i,
                din(71 downto 64) => FIFO_wrinfo_i,
                wr_en => FIFO_wr_i,
                rd_en => s_FIFO_rd,
                dout(31 downto 0) => s_FIFO_vector.slope,
                dout(63 downto 32) => s_FIFO_vector.interval,
                dout(71 downto 64) => s_FIFO_rdinfo,
                full => FIFO_full_o,
                empty => s_FIFO_empty,
                valid => s_FIFO_rdvalid,
                prog_empty => FIFO_prog_empty_o,
                prog_full => FIFO_prog_full_o,
                wr_rst_busy => FIFO_wr_rst_busy_o,
                rd_rst_busy => open
            );
    --=============================================================================
    -- function state machine instantations
    --=============================================================================
    function_state_machine_0:function_state_machine
        port map(
            -- control interface
            enable_i=>enable_i,
            start_i=>s_start_sync,
            restart_i=>s_restart_sync,
            coast_enable_i=>coast_enable_i,
            coast_start_i=>coast_start_i,
            coast_stop_i=>coast_stop_i,
            coast_mode_o=>s_coast_mode,
            interpol_disable_i=>interpol_disable_i,
            clear_function_after_coast_i => clear_function_after_coast_i,
            
            -- accumulator interface
            start_value_i=>start_value_i,
            load_start_value_o=>s_load_start_value,
            accu_start_value_o=>s_accu_start_value,
            slope_o=>s_slope,
            slope_en_o=>s_slope_en,
            fgc_reset_o=>s_fgc_reset,
            
            -- FIFO interface
            fifo_rd_o=>s_FIFO_rd,
            fifo_empty_i=>s_FIFO_empty,
            fifo_rd_info_i=>s_FIFO_rdinfo,
            fifo_vector_i=>s_FIFO_vector,
            fifo_rd_valid_i=>s_FIFO_rdvalid,
            
            --buffer selection
            fgc_buffer_sel_update_i => fgc_buffer_sel_update_i,
            fgc_buffer_sel_i => fgc_buffer_sel_i,
            
            -- status interface
            funct_busy_o =>funct_busy_o,
            fifo_empty_fault_o=>fifo_empty_fault_o,
            cycle_mismatch_fault_o=>cycle_mismatch_fault_o,
            
            inter_clk_en_i => inter_clk_en_i,
            clk_i=>clk_i,
            reset_i=>reset_i
        );
        
    --=============================================================================
    -- coast state machine instantation
    --=============================================================================
    s_coast_trim <= coast_enable_i and coast_trim_i;
    
    coast_state_machine_0: coast_state_machine
    port map( 
            coast_mode_i=>s_coast_mode,
            coast_trim_i=>s_coast_trim,
            
            -- accumulator interface
            slope_o => s_coast_slope,
            slope_en_o => s_coast_slope_en,
            
            -- RAM interface
            next_vector_o => next_vector_o,
            vector_valid_i => vector_valid_i,
            vector_i => coast_vector_i,
            
            -- Status
            coast_busy_o => coast_busy_o,
            
            -- general
            inter_clk_en_i => inter_clk_en_i,
            clk_i => clk_i,
            reset_i => s_internal_reset
          );
    --=============================================================================
    -- function accumulator instantation
    --=============================================================================
    funct_accumulator: accumulator
    port map( 
            -- control interface
            wrap_en_i => wrap_en_i,
            
            -- status interface
            saturation_o => s_funct_sat,
            
            -- accumulator interface
            load_start_value_i => s_load_start_value,
            start_value_i => s_accu_start_value,
            slope_i => s_slope,
            slope_en_i => s_slope_en,
            dout_o => s_function,
            
            -- general
            inter_clk_en_i => inter_clk_en_i,
            clk_i => clk_i,                   
            reset_i => s_internal_reset          
          );
      --=============================================================================
      -- coast accumulator instantation
      --=============================================================================
      coast_accumulator: accumulator
      port map( 
              -- control interface
              wrap_en_i => wrap_en_i,
              
              -- status interface
              saturation_o => s_coast_sat,
              
              -- accumulator interface
              load_start_value_i => '0',
              start_value_i => (others => '0'),
              slope_i => s_coast_slope,
              slope_en_i => s_coast_slope_en,
              dout_o => s_coast,
              
              -- general
              inter_clk_en_i => inter_clk_en_i,
              clk_i => clk_i,                   
              reset_i => s_internal_reset            
            );
        --=============================================================================
        -- coast accumulator instantation
        --============================================================================= 
       acc_sat_fault_o <= (s_funct_sat or s_coast_sat or s_adder_sat) and not wrap_en_i;
                 
        adder_0:adder
        generic map(
                    g_WIDTH => c_FUNCTION_WIDTH
                )
        port map( 
                -- control interface
                wrap_en_i => wrap_en_i,
                
                -- status interface
                saturation_o => s_adder_sat,
                
                -- adder interface
                din_0_i => s_function,
                din_1_i => s_coast,
                dout_o => function_output_o,
                
                -- general
                clk_i => clk_i,
                reset_i => s_internal_reset
              );
        --=============================================================================
        -- start multiplexer instantation
        --=============================================================================              
        FGC_start_multiplexer: multiplexer
          port map( 
                  sel_i => start_trig_sel_i,
                  --input
                  in0_i => soft_trig_start_i,
                  in1_i => ext_trig_start_0_i,
                  in2_i => ext_trig_start_1_i,
                  --ouput
                  out_o => s_start
                );
                
        --=============================================================================
        -- restart multiplexer instantation
        --=============================================================================              
        FGC_restart_multiplexer: multiplexer
          port map( 
                  sel_i => restart_trig_sel_i,
                  --input
                  in0_i => soft_trig_restart_i,
                  in1_i => ext_trig_restart_0_i,
                  in2_i => ext_trig_restart_1_i,
                  --ouput
                  out_o => s_restart
                );        
                
        --=============================================================================
        -- FGC start sync instantation
        --============================================================================= 
        FGC_start_sync: sync
          Port map( 
        
                  in_i => s_start,
                  out_o => s_start_sync,
                  
                  -- general
                  inter_clk_en_i => inter_clk_en_i,
                  clk_i => clk_i,
                  reset_i => reset_i
                );
                
        --=============================================================================
        -- FGC restart sync instantation
        --============================================================================= 
        FGC_restart_sync: sync
          Port map( 
        
                  in_i => s_restart,
                  out_o => s_restart_sync,
                  
                  -- general
                  inter_clk_en_i => inter_clk_en_i,
                  clk_i => clk_i,
                  reset_i => reset_i
                );                

end arch_imp;
