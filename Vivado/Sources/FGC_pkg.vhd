-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, FGC Package --
-- --
-------------------------------------------------------------------------------
--
-- unit name: FGC Package
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 11/04/2018
--
-- version: 1.0
--
-- description: function generator package
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

package FGC_PACK is
    --=====================================================================
    -- Constant instantation
    --=====================================================================
    constant c_FUNCTION_WIDTH               : integer := 32;
    constant c_START_VALUE_WIDTH            : integer := 32;
    constant c_SLOPE_WIDTH                  : integer := 32;
    constant c_INTERVAL_WIDTH               : integer := 32;
    constant c_INFO_WIDTH                   : integer := 8;
    constant c_BUFFER_WIDTH                 : integer := 32;
    constant c_REG_WIDTH                    : integer := 32;
        
    -- fifo information description
    constant c_FUNCT_START : integer := 0; -- currently not used
    constant c_FUNCT_END   : integer := 1;
    constant c_CYCLE_NBR   : integer := 2;
    
    -- special vector
    constant c_zero_slope   : std_logic_vector(31 downto 0) := x"00000000";
    constant c_inf_interval : std_logic_vector(31 downto 0) := x"FFFFFFFF";
    
    --IP versioning
    constant c_IDENT                : std_logic_vector(63 downto 0) := X"0000000000005555"; --temporary
    constant c_FIRMWARE_VERSION     : std_logic_vector(31 downto 0) := X"20180506";         --temporary
    constant c_MEM_MAP_VESRION      : std_logic_vector(31 downto 0) := X"00000001";         --temporary
    
    --=====================================================================
    -- Type instantation
    --=====================================================================

    -- buffer type declaration
    type t_buffer is
    record
        index       : unsigned(c_BUFFER_WIDTH-1 downto 0);
    
    end record;
       
    type t_buffer_array is array (integer range <>) of t_buffer;
    ------------------------------------------------------------------------------------ 
    -- function input parameters type declaration
    -- all are std_logic_vector due they are used as inputs/outputs
    type t_func_param is
    record
        enable          : std_logic;
        length          : std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
    
    end record;
      
    type t_func_array is array (integer range <>) of t_func_param;
    ------------------------------------------------------------------------------------ 
    -- AXI read command
    -- used for the signals
    type t_AXI_RD_command is
    record 
        start              : std_logic;
        burst_length       : unsigned (8 downto 0);
        address            : unsigned (31 downto 0);
        data_size          : unsigned (31 downto 0); 
    end record;
    ------------------------------------------------------------------------------------ 
    -- AXI read command with std_logic_vector
    -- all are std_logic_vector due they are used as inputs/outputs
    type t_AXI_command_std is
    record 
        start              : std_logic;
        burst_length       : std_logic_vector (8 downto 0);
        address            : std_logic_vector (31 downto 0);
        data_size          : std_logic_vector (31 downto 0); 
    end record;
    ------------------------------------------------------------------------------------ 
    -- vector format
    -- definition of the vector format
    type t_vector_format is
    record 
        slope            : std_logic_vector (c_SLOPE_WIDTH-1 downto 0);
        interval         : std_logic_vector (c_INTERVAL_WIDTH-1 downto 0); 
    end record;
    ------------------------------------------------------------------------------------
    -- function configuration (functConfig) register
    -- definition of functConfig register
    type t_functConfig is
    record 
        unused1         : std_logic_vector(1 downto 0);
        startTrigSel    : std_logic_vector(3 downto 2);
        restartTrigSel  : std_logic_vector(5 downto 4);
        unused2         : std_logic_vector(14 downto 6);
        enable          : std_logic_vector(15 downto 15);
        interpolDisable : std_logic_vector(16 downto 16);
        wrapEn          : std_logic_vector(17 downto 17);
        coastEnable     : std_logic_vector(18 downto 18);
        unused3         : std_logic_vector(31 downto 19);
    end record;
    
    constant c_functConfig_default : t_functConfig := ( unused1         => (others => '0'),
                                                        startTrigSel    => (others => '0'),
                                                        restartTrigSel  => (others => '0'),
                                                        unused2         => (others => '0'),
                                                        enable          => (others => '0'),
                                                        interpolDisable => (others => '0'),
                                                        wrapEn          => (others => '0'),
                                                        coastEnable     => (others => '0'),
                                                        unused3         => (others => '0'));
    ------------------------------------------------------------------------------------

    -- bit 7-2 are unused for the moment
    ------------------------------------------------------------------------------------
    --  8-bit array type declaration
    type t_FCG_ARRAY_08 is array (integer range <>) of std_logic_vector(7 downto 0);
    -- 16-bit array type declaration
    type t_FCG_ARRAY_16 is array (integer range <>) of unsigned(15 downto 0);
    type t_FCG_ARRAY_16_STD is array (integer range <>) of std_logic_vector(15 downto 0);
    -- 19-bit array type declaration
    type t_FCG_ARRAY_19 is array (integer range <>) of unsigned(18 downto 0);
    -- 32-bit array type declaration
    type t_FCG_ARRAY_32     is array (integer range <>) of unsigned(31 downto 0);
    type t_FCG_ARRAY_32_STD is array (integer range <>) of std_logic_vector(31 downto 0);
    -- 64-bit array type declaration
    type t_FCG_ARRAY_64 is array (integer range <>) of std_logic_vector(63 downto 0);
    -- functConfig array type declaration
    type t_functConfig_array is array (integer range <>) of t_functConfig;
    --=====================================================================
    -- function declaration
    --=====================================================================
    -- return the number of bit to express x in binary
    function f_log2 (x : positive) return natural;     
    function f_functConfig_to_std (x : t_functConfig) return std_logic_vector;
    function f_std_to_functConfig (x : std_logic_vector)  return t_functConfig;

end FGC_PACK;

    --=====================================================================
    -- function declaration
    --=====================================================================

    package body FGC_PACK is
        ------------------------------------------------
        function f_log2 (x : positive) return natural is 
            variable i : natural;
        begin
            i := 0;  
            while (2**i < x) and i < 31 loop
             i := i + 1;
            end loop;
            return i;
        end function;
        ------------------------------------------------
        -- convert from functConfig type to std_logic_vector
        function f_functConfig_to_std (x : t_functConfig) return std_logic_vector is 
        begin
            return x.unused3 & x.coastEnable & x.wrapEn & x.interpolDisable & x.enable & x.unused2 & x.restartTrigSel & x.startTrigSel & x.unused1;
        end function;
        ------------------------------------------------
        -- convert from std_logic_vector type to functConfig
        function f_std_to_functConfig (x : std_logic_vector) return t_functConfig is 
            variable v_functConfig : t_functConfig;
        begin
            v_functConfig.unused3           := x(v_functConfig.unused3'range);
            v_functConfig.coastEnable       := x(v_functConfig.coastEnable'range);
            v_functConfig.wrapEn            := x(v_functConfig.wrapEn'range);
            v_functConfig.interpolDisable   := x(v_functConfig.interpolDisable'range);
            v_functConfig.enable            := x(v_functConfig.enable'range);
            v_functConfig.unused2           := x(v_functConfig.unused2'range);
            v_functConfig.restartTrigSel    := x(v_functConfig.restartTrigSel'range);
            v_functConfig.startTrigSel      := x(v_functConfig.startTrigSel'range);
            v_functConfig.unused1           := x(v_functConfig.unused1'range);
            return v_functConfig;
        end function;
    end FGC_PACK;