--- VME memory map fgc
--- Memory map version 20190528
--- Generated on 2019-05-28 by jegli using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_fgc is

	-- Ident Code
	constant C_fgc_IdentCode : std_logic_vector(31 downto 0) := X"000000ff";

	-- Memory Map Version
	constant C_fgc_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20190528,32));

	-- Semantic Memory Map Version
	constant C_fgc_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00000001";

	-- Register Addresses : Memory Map
	constant C_Reg_fgc_control		:		std_logic_vector(12 downto 2) := "00000001000";-- : Word address : "000" & "000" & X"08"; Byte Address : "000" & X"10"
	constant C_Reg_fgc_status		:		std_logic_vector(12 downto 2) := "00000001001";-- : Word address : "000" & "000" & X"09"; Byte Address : "000" & X"12"
	constant C_Reg_fgc_busy		:		std_logic_vector(12 downto 2) := "00000001010";-- : Word address : "000" & "000" & X"0a"; Byte Address : "000" & X"14"
	constant C_Reg_fgc_faults		:		std_logic_vector(12 downto 2) := "00000001011";-- : Word address : "000" & "000" & X"0b"; Byte Address : "000" & X"16"
	constant C_Reg_fgc_faultsEnable		:		std_logic_vector(12 downto 2) := "00000001100";-- : Word address : "000" & "000" & X"0c"; Byte Address : "000" & X"18"
	constant C_Reg_fgc_fifoFaults		:		std_logic_vector(12 downto 2) := "00000001101";-- : Word address : "000" & "000" & X"0d"; Byte Address : "000" & X"1a"
	constant C_Reg_fgc_functionFaults		:		std_logic_vector(12 downto 2) := "00000001110";-- : Word address : "000" & "000" & X"0e"; Byte Address : "000" & X"1c"
	constant C_Reg_fgc_functSel		:		std_logic_vector(12 downto 2) := "00000001111";-- : Word address : "000" & "000" & X"0f"; Byte Address : "000" & X"1e"
	constant C_Reg_fgc_functConfig		:		std_logic_vector(12 downto 2) := "00000010000";-- : Word address : "000" & "000" & X"10"; Byte Address : "000" & X"20"
	constant C_Reg_fgc_offlineStartValue		:		std_logic_vector(12 downto 2) := "00000010001";-- : Word address : "000" & "000" & X"11"; Byte Address : "000" & X"22"
	constant C_Reg_fgc_onlineStartValue		:		std_logic_vector(12 downto 2) := "00000010010";-- : Word address : "000" & "000" & X"12"; Byte Address : "000" & X"24"
	constant C_Reg_fgc_functionSize		:		std_logic_vector(12 downto 2) := "00000010011";-- : Word address : "000" & "000" & X"13"; Byte Address : "000" & X"26"
	constant C_Reg_fgc_currentValue		:		std_logic_vector(12 downto 2) := "00000010100";-- : Word address : "000" & "000" & X"14"; Byte Address : "000" & X"28"
	constant C_Reg_fgc_softTrig		:		std_logic_vector(12 downto 2) := "00000010101";-- : Word address : "000" & "000" & X"15"; Byte Address : "000" & X"2a"
	constant C_Reg_fgc_bufferStartAddress		:		std_logic_vector(12 downto 2) := "00000010110";-- : Word address : "000" & "000" & X"16"; Byte Address : "000" & X"2c"
	constant C_Reg_fgc_bufferSize		:		std_logic_vector(12 downto 2) := "00000010111";-- : Word address : "000" & "000" & X"17"; Byte Address : "000" & X"2e"

	-- Register Auto Clear Masks : Memory Map
	constant C_ACM_fgc_control		:		std_logic_vector(31 downto 0) := "00000000000000011011111100000000";-- : Value : X"0001bf00"
	constant C_ACM_fgc_status		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_busy		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_faults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_faultsEnable		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_fifoFaults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_functionFaults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_functSel		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_functConfig		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_offlineStartValue		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_onlineStartValue		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_functionSize		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_currentValue		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_softTrig		:		std_logic_vector(31 downto 0) := "11111111111111111111111111111111";-- : Value : X"ffffffff"
	constant C_ACM_fgc_bufferStartAddress		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_fgc_bufferSize		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"

	-- Register Preset Masks : Memory Map
	constant C_PSM_fgc_control		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_status		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_busy		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_faults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_faultsEnable		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_fifoFaults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_functionFaults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_functSel		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_functConfig		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_offlineStartValue		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_onlineStartValue		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_functionSize		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_currentValue		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_softTrig		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_bufferStartAddress		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_fgc_bufferSize		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"

	-- CODE FIELDS
	constant C_Code_fgc_functConfig_startTrigSel_soft		:		std_logic_vector(3 downto 2) := "00";
	constant C_Code_fgc_functConfig_startTrigSel_trig		:		std_logic_vector(3 downto 2) := "01";
	constant C_Code_fgc_functConfig_startTrigSel_hard		:		std_logic_vector(3 downto 2) := "10";
	constant C_Code_fgc_functConfig_startTrigSel_disable		:		std_logic_vector(3 downto 2) := "11";
	constant C_Code_fgc_functConfig_restartTrigSel_soft		:		std_logic_vector(5 downto 4) := "00";
	constant C_Code_fgc_functConfig_restartTrigSel_trig		:		std_logic_vector(5 downto 4) := "01";
	constant C_Code_fgc_functConfig_restartTrigSel_hard		:		std_logic_vector(5 downto 4) := "10";
	constant C_Code_fgc_functConfig_restartTrigSel_disable		:		std_logic_vector(5 downto 4) := "11";
	-- Memory Data : Memory Map
	constant C_Mem_fgc_coastMemory_Sta		:		std_logic_vector(12 downto 2) := "10000000000";-- : Word address : "100" & "100" & X"00"; Byte Address : "000" & X"00"
	constant C_Mem_fgc_coastMemory_End		:		std_logic_vector(12 downto 2) := "11111111111";-- : Word address : "111" & "111" & X"ff"; Byte Address : "111" & X"fe"
	-- Submap Addresses : Memory Map
	constant C_Submap_fgc_ipInfo		:		std_logic_vector(12 downto 5) := "00000000";-- : Word address : "0" & X"00"; Byte Address : X"00"
end;
-- EOF