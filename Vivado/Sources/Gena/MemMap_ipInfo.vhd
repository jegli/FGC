--- VME memory map ipInfo
--- Memory map version 20190527
--- Generated on 2019-05-28 by jegli using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_ipInfo is

	-- Ident Code
	constant C_ipInfo_IdentCode : std_logic_vector(31 downto 0) := X"000000ff";

	-- Memory Map Version
	constant C_ipInfo_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20190527,32));

	-- Semantic Memory Map Version
	constant C_ipInfo_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00000001";

	-- Register Addresses : Memory Map
	constant C_Reg_ipInfo_ident_1		:		std_logic_vector(4 downto 2) := "000";-- : Word address : "000" & "000" & X"0"; Byte Address : "000" & X"0"
	constant C_Reg_ipInfo_ident_0		:		std_logic_vector(4 downto 2) := "001";-- : Word address : "001" & "001" & X"0"; Byte Address : "010" & X"0"
	constant C_Reg_ipInfo_firmwareVersion		:		std_logic_vector(4 downto 2) := "010";-- : Word address : "010" & "010" & X"0"; Byte Address : "100" & X"0"
	constant C_Reg_ipInfo_memMapVersion		:		std_logic_vector(4 downto 2) := "011";-- : Word address : "011" & "011" & X"0"; Byte Address : "110" & X"0"
	constant C_Reg_ipInfo_sysControl		:		std_logic_vector(4 downto 2) := "100";-- : Word address : "100" & "100" & X"0"; Byte Address : "000" & X"0"

	-- Register Auto Clear Masks : Memory Map
	constant C_ACM_ipInfo_ident_1		:		std_logic_vector(63 downto 32) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_ipInfo_ident_0		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_ipInfo_firmwareVersion		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_ipInfo_memMapVersion		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_ipInfo_sysControl		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- Register Preset Masks : Memory Map
	constant C_PSM_ipInfo_ident_1		:		std_logic_vector(63 downto 32) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_ipInfo_ident_0		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_ipInfo_firmwareVersion		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_ipInfo_memMapVersion		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_ipInfo_sysControl		:		std_logic_vector(15 downto 0) := "0000000000000000";-- : Value : X"0000"

	-- CODE FIELDS
	-- Memory Data : Memory Map
	-- Submap Addresses : Memory Map
end;
-- EOF