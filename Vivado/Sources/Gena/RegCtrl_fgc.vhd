-- VME registers for fgc
-- Memory map version 20190528
-- Generated on 2019-05-28 15:08:50 by jegli using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_fgc.all;
use work.MemMap_ipInfo.all;

entity RegCtrl_fgc is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(12 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		ipInfo_ident : in std_logic_vector(63 downto 0);
		ipInfo_firmwareVersion : in std_logic_vector(31 downto 0);
		ipInfo_memMapVersion : in std_logic_vector(31 downto 0);
		ipInfo_sysControl : out std_logic_vector(15 downto 0);
		control_clearFunctsAfterCoast : out std_logic;
		control_softReset : out std_logic;
		control_clrFaults : out std_logic;
		control_fifoManEnable : out std_logic;
		control_functMemReady : out std_logic;
		control_coastMemReady : out std_logic;
		control_softTrigPrepare : out std_logic;
		control_softTrigTrim : out std_logic;
		control_softTrigCoastStart : out std_logic;
		control_softTrigCoastStop : out std_logic;
		control_prepareTrigSel : out std_logic_vector(7 downto 6);
		control_coastTrigSel : out std_logic_vector(5 downto 4);
		control_coastStartTrigSel : out std_logic_vector(3 downto 2);
		control_coastStopTrigSel : out std_logic_vector(1 downto 0);
		status_fifoManBusy : in std_logic;
		status_fifoManPreloadingFinished : in std_logic;
		status_noFaults : in std_logic;
		status_noOverflow : in std_logic;
		status_coastBuffSel : in std_logic;
		status_buffSel : in std_logic;
		status_coastMode : in std_logic_vector(31 downto 16);
		busy_functBusy : in std_logic_vector(15 downto 0);
		busy_coastTrimtBusy : in std_logic_vector(31 downto 16);
		faults_accSatFault : in std_logic;
		faults_fifoEmptyFault : in std_logic;
		faults_fifoFullFault : in std_logic;
		faults_cycleMismatchFault : in std_logic;
		faults_functMemReadyFault : in std_logic;
		faults_coastMemReadyFault : in std_logic;
		faults_SRFF : out std_logic_vector(31 downto 0);
		faults_ClrSRFF : in std_logic;
		faultsEnable_accSatFaultEnable : out std_logic;
		faultsEnable_fifoEmptyFaultEnable : out std_logic;
		faultsEnable_fifoFullFaultEnable : out std_logic;
		faultsEnable_cycleMismatchFaultEnable : out std_logic;
		faultsEnable_functMemReadyFaultEnable : out std_logic;
		faultsEnable_coastMemReadyFaultEnable : out std_logic;
		fifoFaults_fifoEmptyFault0 : in std_logic;
		fifoFaults_fifoEmptyFault1 : in std_logic;
		fifoFaults_fifoEmptyFault2 : in std_logic;
		fifoFaults_fifoEmptyFault3 : in std_logic;
		fifoFaults_fifoEmptyFault4 : in std_logic;
		fifoFaults_fifoEmptyFault5 : in std_logic;
		fifoFaults_fifoEmptyFault6 : in std_logic;
		fifoFaults_fifoEmptyFault7 : in std_logic;
		fifoFaults_fifoEmptyFault8 : in std_logic;
		fifoFaults_fifoEmptyFault9 : in std_logic;
		fifoFaults_fifoEmptyFault10 : in std_logic;
		fifoFaults_fifoEmptyFault11 : in std_logic;
		fifoFaults_fifoEmptyFault12 : in std_logic;
		fifoFaults_fifoEmptyFault13 : in std_logic;
		fifoFaults_fifoEmptyFault14 : in std_logic;
		fifoFaults_fifoEmptyFault15 : in std_logic;
		fifoFaults_fifoFullFault0 : in std_logic;
		fifoFaults_fifoFullFault1 : in std_logic;
		fifoFaults_fifoFullFault2 : in std_logic;
		fifoFaults_fifoFullFault3 : in std_logic;
		fifoFaults_fifoFullFault4 : in std_logic;
		fifoFaults_fifoFullFault5 : in std_logic;
		fifoFaults_fifoFullFault6 : in std_logic;
		fifoFaults_fifoFullFault7 : in std_logic;
		fifoFaults_fifoFullFault8 : in std_logic;
		fifoFaults_fifoFullFault9 : in std_logic;
		fifoFaults_fifoFullFault10 : in std_logic;
		fifoFaults_fifoFullFault11 : in std_logic;
		fifoFaults_fifoFullFault12 : in std_logic;
		fifoFaults_fifoFullFault13 : in std_logic;
		fifoFaults_fifoFullFault14 : in std_logic;
		fifoFaults_fifoFullFault15 : in std_logic;
		fifoFaults_SRFF : out std_logic_vector(31 downto 0);
		fifoFaults_ClrSRFF : in std_logic;
		functionFaults_accSatFault0 : in std_logic;
		functionFaults_accSatFault1 : in std_logic;
		functionFaults_accSatFault2 : in std_logic;
		functionFaults_accSatFault3 : in std_logic;
		functionFaults_accSatFault4 : in std_logic;
		functionFaults_accSatFault5 : in std_logic;
		functionFaults_accSatFault6 : in std_logic;
		functionFaults_accSatFault7 : in std_logic;
		functionFaults_accSatFault8 : in std_logic;
		functionFaults_accSatFault9 : in std_logic;
		functionFaults_accSatFault10 : in std_logic;
		functionFaults_accSatFault11 : in std_logic;
		functionFaults_accSatFault12 : in std_logic;
		functionFaults_accSatFault13 : in std_logic;
		functionFaults_accSatFault14 : in std_logic;
		functionFaults_accSatFault15 : in std_logic;
		functionFaults_cycleMismatchFault0 : in std_logic;
		functionFaults_cycleMismatchFault1 : in std_logic;
		functionFaults_cycleMismatchFault2 : in std_logic;
		functionFaults_cycleMismatchFault3 : in std_logic;
		functionFaults_cycleMismatchFault4 : in std_logic;
		functionFaults_cycleMismatchFault5 : in std_logic;
		functionFaults_cycleMismatchFault6 : in std_logic;
		functionFaults_cycleMismatchFault7 : in std_logic;
		functionFaults_cycleMismatchFault8 : in std_logic;
		functionFaults_cycleMismatchFault9 : in std_logic;
		functionFaults_cycleMismatchFault10 : in std_logic;
		functionFaults_cycleMismatchFault11 : in std_logic;
		functionFaults_cycleMismatchFault12 : in std_logic;
		functionFaults_cycleMismatchFault13 : in std_logic;
		functionFaults_cycleMismatchFault14 : in std_logic;
		functionFaults_cycleMismatchFault15 : in std_logic;
		functionFaults_SRFF : out std_logic_vector(31 downto 0);
		functionFaults_ClrSRFF : in std_logic;
		functSel_functionSelect : out std_logic_vector(16 downto 13);
		functConfig : in std_logic_vector(31 downto 0);
		functConfig_Sel : out std_logic;
		functConfig_WrStrobe : out std_logic;
		functConfig_RdStrobe : out std_logic;
		offlineStartValue : in std_logic_vector(31 downto 0);
		offlineStartValue_Sel : out std_logic;
		offlineStartValue_WrStrobe : out std_logic;
		offlineStartValue_RdStrobe : out std_logic;
		onlineStartValue : in std_logic_vector(31 downto 0);
		functionSize : in std_logic_vector(31 downto 0);
		functionSize_Sel : out std_logic;
		functionSize_WrStrobe : out std_logic;
		functionSize_RdStrobe : out std_logic;
		currentValue : in std_logic_vector(31 downto 0);
		softTrig_functStartTrig0 : out std_logic;
		softTrig_functStartTrig1 : out std_logic;
		softTrig_functStartTrig2 : out std_logic;
		softTrig_functStartTrig3 : out std_logic;
		softTrig_functStartTrig4 : out std_logic;
		softTrig_functStartTrig5 : out std_logic;
		softTrig_functStartTrig6 : out std_logic;
		softTrig_functStartTrig7 : out std_logic;
		softTrig_functStartTrig8 : out std_logic;
		softTrig_functStartTrig9 : out std_logic;
		softTrig_functStartTrig10 : out std_logic;
		softTrig_functStartTrig11 : out std_logic;
		softTrig_functStartTrig12 : out std_logic;
		softTrig_functStartTrig13 : out std_logic;
		softTrig_functStartTrig14 : out std_logic;
		softTrig_functStartTrig15 : out std_logic;
		softTrig_functRestartTrig0 : out std_logic;
		softTrig_functRestartTrig1 : out std_logic;
		softTrig_functRestartTrig2 : out std_logic;
		softTrig_functRestartTrig3 : out std_logic;
		softTrig_functRestartTrig4 : out std_logic;
		softTrig_functRestartTrig5 : out std_logic;
		softTrig_functRestartTrig6 : out std_logic;
		softTrig_functRestartTrig7 : out std_logic;
		softTrig_functRestartTrig8 : out std_logic;
		softTrig_functRestartTrig9 : out std_logic;
		softTrig_functRestartTrig10 : out std_logic;
		softTrig_functRestartTrig11 : out std_logic;
		softTrig_functRestartTrig12 : out std_logic;
		softTrig_functRestartTrig13 : out std_logic;
		softTrig_functRestartTrig14 : out std_logic;
		softTrig_functRestartTrig15 : out std_logic;
		bufferStartAddress : out std_logic_vector(31 downto 0);
		bufferStartAddress_WrStrobe : out std_logic;
		bufferSize : in std_logic_vector(31 downto 0);
		coastMemory_Sel : out std_logic;
		coastMemory_Addr : out std_logic_vector(11 downto 2);
		coastMemory_RdData : in std_logic_vector(31 downto 0);
		coastMemory_WrData : out std_logic_vector(31 downto 0);
		coastMemory_RdMem : out std_logic;
		coastMemory_WrMem : out std_logic;
		coastMemory_RdDone : in std_logic;
		coastMemory_WrDone : in std_logic;
		coastMemory_RdError : in std_logic := '0';
		coastMemory_WrError : in std_logic := '0'
	);
end;

architecture Gena of RegCtrl_fgc is
	component RMWReg
		generic (
			N : natural := 8
		);
		port (
			VMEWrData : in std_logic_vector(2*N-1 downto 0);
			Clk : in std_logic;
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			Rst : in std_logic;
			CRegSel : in std_logic;
			CReg : out std_logic_vector(N-1 downto 0);
			WriteMem : in std_logic;
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : RMWReg use entity CommonVisual.RMWReg(RMWReg);

	component CtrlRegN
		generic (
			N : integer := 16
		);
		port (
			Clk : in std_logic;
			Rst : in std_logic;
			CRegSel : in std_logic;
			WriteMem : in std_logic;
			VMEWrData : in std_logic_vector(N-1 downto 0);
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			CReg : out std_logic_vector(N-1 downto 0);
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : CtrlRegN use entity CommonVisual.CtrlRegN(V1);

	component SRFFxN
		generic (
			N : positive := 16
		);
		port (
			Clk : in std_logic;
			Rst : in std_logic:='0';
			Set : in std_logic_vector(N-1 downto 0);
			Clr : in std_logic:='0';
			Q : out std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : SRFFxN use entity CommonVisual.SRFFxN(V1);

	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal ipInfo_CRegRdData : std_logic_vector(31 downto 0);
	signal ipInfo_CRegRdOK : std_logic;
	signal ipInfo_CRegWrOK : std_logic;
	signal Loc_ipInfo_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_CRegRdOK : std_logic;
	signal Loc_ipInfo_CRegWrOK : std_logic;
	signal ipInfo_RegRdDone : std_logic;
	signal ipInfo_RegWrDone : std_logic;
	signal ipInfo_RegRdData : std_logic_vector(31 downto 0);
	signal ipInfo_RegRdOK : std_logic;
	signal Loc_ipInfo_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_RegRdOK : std_logic;
	signal ipInfo_MemRdData : std_logic_vector(31 downto 0);
	signal ipInfo_MemRdDone : std_logic;
	signal ipInfo_MemWrDone : std_logic;
	signal Loc_ipInfo_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_MemRdDone : std_logic;
	signal Loc_ipInfo_MemWrDone : std_logic;
	signal ipInfo_RdData : std_logic_vector(31 downto 0);
	signal ipInfo_RdDone : std_logic;
	signal ipInfo_WrDone : std_logic;
	signal ipInfo_RegRdError : std_logic;
	signal ipInfo_RegWrError : std_logic;
	signal ipInfo_MemRdError : std_logic;
	signal ipInfo_MemWrError : std_logic;
	signal Loc_ipInfo_MemRdError : std_logic;
	signal Loc_ipInfo_MemWrError : std_logic;
	signal ipInfo_RdError : std_logic;
	signal ipInfo_WrError : std_logic;
	signal Loc_ipInfo_ident : std_logic_vector(63 downto 0);
	signal Loc_ipInfo_firmwareVersion : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_memMapVersion : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_sysControl : std_logic_vector(15 downto 0);
	signal WrSel_ipInfo_sysControl : std_logic;
	signal Loc_control : std_logic_vector(31 downto 0);
	signal WrSel_control : std_logic;
	signal Loc_status : std_logic_vector(31 downto 0);
	signal Loc_busy : std_logic_vector(31 downto 0);
	signal Loc_faults : std_logic_vector(31 downto 0);
	signal Loc_faults_SRFF : std_logic_vector(31 downto 0);
	signal Loc_faultsEnable : std_logic_vector(31 downto 0);
	signal WrSel_faultsEnable : std_logic;
	signal Loc_fifoFaults : std_logic_vector(31 downto 0);
	signal Loc_fifoFaults_SRFF : std_logic_vector(31 downto 0);
	signal Loc_functionFaults : std_logic_vector(31 downto 0);
	signal Loc_functionFaults_SRFF : std_logic_vector(31 downto 0);
	signal Loc_functSel : std_logic_vector(31 downto 0);
	signal WrSel_functSel : std_logic;
	signal Loc_functConfig : std_logic_vector(31 downto 0);
	signal WrSel_functConfig : std_logic;
	signal RdSel_functConfig : std_logic;
	signal Loc_offlineStartValue : std_logic_vector(31 downto 0);
	signal WrSel_offlineStartValue : std_logic;
	signal RdSel_offlineStartValue : std_logic;
	signal Loc_onlineStartValue : std_logic_vector(31 downto 0);
	signal Loc_functionSize : std_logic_vector(31 downto 0);
	signal WrSel_functionSize : std_logic;
	signal RdSel_functionSize : std_logic;
	signal Loc_currentValue : std_logic_vector(31 downto 0);
	signal Loc_softTrig : std_logic_vector(31 downto 0);
	signal WrSel_softTrig : std_logic;
	signal Loc_bufferStartAddress : std_logic_vector(31 downto 0);
	signal WrSel_bufferStartAddress : std_logic;
	signal Loc_bufferSize : std_logic_vector(31 downto 0);
	signal Sel_coastMemory : std_logic;
begin
	Reg_control: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_control,
			AutoClrMsk => C_ACM_fgc_control,
			Preset => C_PSM_fgc_control,
			CReg => Loc_control(31 downto 0)
		);

	SRFF_faults: SRFFxN
		generic map (
			N => 32
		)
		port map (
			Clk => Clk,
			Rst => Rst,
			Set => Loc_faults,
			Clr => faults_ClrSRFF,
			Q => Loc_faults_SRFF
		);

	Reg_faultsEnable: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_faultsEnable,
			AutoClrMsk => C_ACM_fgc_faultsEnable,
			Preset => C_PSM_fgc_faultsEnable,
			CReg => Loc_faultsEnable(31 downto 0)
		);

	SRFF_fifoFaults: SRFFxN
		generic map (
			N => 32
		)
		port map (
			Clk => Clk,
			Rst => Rst,
			Set => Loc_fifoFaults,
			Clr => fifoFaults_ClrSRFF,
			Q => Loc_fifoFaults_SRFF
		);

	SRFF_functionFaults: SRFFxN
		generic map (
			N => 32
		)
		port map (
			Clk => Clk,
			Rst => Rst,
			Set => Loc_functionFaults,
			Clr => functionFaults_ClrSRFF,
			Q => Loc_functionFaults_SRFF
		);

	Reg_functSel: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_functSel,
			AutoClrMsk => C_ACM_fgc_functSel,
			Preset => C_PSM_fgc_functSel,
			CReg => Loc_functSel(31 downto 0)
		);

	Reg_softTrig: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_softTrig,
			AutoClrMsk => C_ACM_fgc_softTrig,
			Preset => C_PSM_fgc_softTrig,
			CReg => Loc_softTrig(31 downto 0)
		);

	Reg_bufferStartAddress: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_bufferStartAddress,
			AutoClrMsk => C_ACM_fgc_bufferStartAddress,
			Preset => C_PSM_fgc_bufferStartAddress,
			CReg => Loc_bufferStartAddress(31 downto 0)
		);

	control_clearFunctsAfterCoast <= Loc_control(17);
	control_softReset <= Loc_control(16);
	control_clrFaults <= Loc_control(15);
	control_fifoManEnable <= Loc_control(14);
	control_functMemReady <= Loc_control(13);
	control_coastMemReady <= Loc_control(12);
	control_softTrigPrepare <= Loc_control(11);
	control_softTrigTrim <= Loc_control(10);
	control_softTrigCoastStart <= Loc_control(9);
	control_softTrigCoastStop <= Loc_control(8);
	control_prepareTrigSel <= Loc_control(7 downto 6);
	control_coastTrigSel <= Loc_control(5 downto 4);
	control_coastStartTrigSel <= Loc_control(3 downto 2);
	control_coastStopTrigSel <= Loc_control(1 downto 0);

	Loc_status(31 downto 16) <= status_coastMode;
	Loc_status(15) <= status_fifoManBusy;
	Loc_status(14) <= status_fifoManPreloadingFinished;
	Loc_status(13) <= status_noFaults;
	Loc_status(12) <= status_noOverflow;
	Loc_status(11) <= status_coastBuffSel;
	Loc_status(10) <= status_buffSel;
	Loc_status(9 downto 0) <= C_PSM_fgc_status(9 downto 0);

	Loc_busy(31 downto 16) <= busy_coastTrimtBusy;
	Loc_busy(15 downto 0) <= busy_functBusy;

	faults_SRFF <= Loc_faults_SRFF;
	Loc_faults(31 downto 6) <= C_PSM_fgc_faults(31 downto 6);
	Loc_faults(5) <= faults_coastMemReadyFault;
	Loc_faults(4) <= faults_functMemReadyFault;
	Loc_faults(3) <= faults_cycleMismatchFault;
	Loc_faults(2) <= faults_fifoFullFault;
	Loc_faults(1) <= faults_fifoEmptyFault;
	Loc_faults(0) <= faults_accSatFault;

	faultsEnable_accSatFaultEnable <= Loc_faultsEnable(0);
	faultsEnable_fifoEmptyFaultEnable <= Loc_faultsEnable(1);
	faultsEnable_fifoFullFaultEnable <= Loc_faultsEnable(2);
	faultsEnable_cycleMismatchFaultEnable <= Loc_faultsEnable(3);
	faultsEnable_functMemReadyFaultEnable <= Loc_faultsEnable(4);
	faultsEnable_coastMemReadyFaultEnable <= Loc_faultsEnable(5);

	fifoFaults_SRFF <= Loc_fifoFaults_SRFF;
	Loc_fifoFaults(31) <= fifoFaults_fifoFullFault15;
	Loc_fifoFaults(30) <= fifoFaults_fifoFullFault14;
	Loc_fifoFaults(29) <= fifoFaults_fifoFullFault13;
	Loc_fifoFaults(28) <= fifoFaults_fifoFullFault12;
	Loc_fifoFaults(27) <= fifoFaults_fifoFullFault11;
	Loc_fifoFaults(26) <= fifoFaults_fifoFullFault10;
	Loc_fifoFaults(25) <= fifoFaults_fifoFullFault9;
	Loc_fifoFaults(24) <= fifoFaults_fifoFullFault8;
	Loc_fifoFaults(23) <= fifoFaults_fifoFullFault7;
	Loc_fifoFaults(22) <= fifoFaults_fifoFullFault6;
	Loc_fifoFaults(21) <= fifoFaults_fifoFullFault5;
	Loc_fifoFaults(20) <= fifoFaults_fifoFullFault4;
	Loc_fifoFaults(19) <= fifoFaults_fifoFullFault3;
	Loc_fifoFaults(18) <= fifoFaults_fifoFullFault2;
	Loc_fifoFaults(17) <= fifoFaults_fifoFullFault1;
	Loc_fifoFaults(16) <= fifoFaults_fifoFullFault0;
	Loc_fifoFaults(15) <= fifoFaults_fifoEmptyFault15;
	Loc_fifoFaults(14) <= fifoFaults_fifoEmptyFault14;
	Loc_fifoFaults(13) <= fifoFaults_fifoEmptyFault13;
	Loc_fifoFaults(12) <= fifoFaults_fifoEmptyFault12;
	Loc_fifoFaults(11) <= fifoFaults_fifoEmptyFault11;
	Loc_fifoFaults(10) <= fifoFaults_fifoEmptyFault10;
	Loc_fifoFaults(9) <= fifoFaults_fifoEmptyFault9;
	Loc_fifoFaults(8) <= fifoFaults_fifoEmptyFault8;
	Loc_fifoFaults(7) <= fifoFaults_fifoEmptyFault7;
	Loc_fifoFaults(6) <= fifoFaults_fifoEmptyFault6;
	Loc_fifoFaults(5) <= fifoFaults_fifoEmptyFault5;
	Loc_fifoFaults(4) <= fifoFaults_fifoEmptyFault4;
	Loc_fifoFaults(3) <= fifoFaults_fifoEmptyFault3;
	Loc_fifoFaults(2) <= fifoFaults_fifoEmptyFault2;
	Loc_fifoFaults(1) <= fifoFaults_fifoEmptyFault1;
	Loc_fifoFaults(0) <= fifoFaults_fifoEmptyFault0;

	functionFaults_SRFF <= Loc_functionFaults_SRFF;
	Loc_functionFaults(31) <= functionFaults_cycleMismatchFault15;
	Loc_functionFaults(30) <= functionFaults_cycleMismatchFault14;
	Loc_functionFaults(29) <= functionFaults_cycleMismatchFault13;
	Loc_functionFaults(28) <= functionFaults_cycleMismatchFault12;
	Loc_functionFaults(27) <= functionFaults_cycleMismatchFault11;
	Loc_functionFaults(26) <= functionFaults_cycleMismatchFault10;
	Loc_functionFaults(25) <= functionFaults_cycleMismatchFault9;
	Loc_functionFaults(24) <= functionFaults_cycleMismatchFault8;
	Loc_functionFaults(23) <= functionFaults_cycleMismatchFault7;
	Loc_functionFaults(22) <= functionFaults_cycleMismatchFault6;
	Loc_functionFaults(21) <= functionFaults_cycleMismatchFault5;
	Loc_functionFaults(20) <= functionFaults_cycleMismatchFault4;
	Loc_functionFaults(19) <= functionFaults_cycleMismatchFault3;
	Loc_functionFaults(18) <= functionFaults_cycleMismatchFault2;
	Loc_functionFaults(17) <= functionFaults_cycleMismatchFault1;
	Loc_functionFaults(16) <= functionFaults_cycleMismatchFault0;
	Loc_functionFaults(15) <= functionFaults_accSatFault15;
	Loc_functionFaults(14) <= functionFaults_accSatFault14;
	Loc_functionFaults(13) <= functionFaults_accSatFault13;
	Loc_functionFaults(12) <= functionFaults_accSatFault12;
	Loc_functionFaults(11) <= functionFaults_accSatFault11;
	Loc_functionFaults(10) <= functionFaults_accSatFault10;
	Loc_functionFaults(9) <= functionFaults_accSatFault9;
	Loc_functionFaults(8) <= functionFaults_accSatFault8;
	Loc_functionFaults(7) <= functionFaults_accSatFault7;
	Loc_functionFaults(6) <= functionFaults_accSatFault6;
	Loc_functionFaults(5) <= functionFaults_accSatFault5;
	Loc_functionFaults(4) <= functionFaults_accSatFault4;
	Loc_functionFaults(3) <= functionFaults_accSatFault3;
	Loc_functionFaults(2) <= functionFaults_accSatFault2;
	Loc_functionFaults(1) <= functionFaults_accSatFault1;
	Loc_functionFaults(0) <= functionFaults_accSatFault0;

	functSel_functionSelect <= Loc_functSel(16 downto 13);

	Loc_functConfig <= functConfig;
	functConfig_Sel <= WrSel_functConfig;
	functConfig_WrStrobe <= WrSel_functConfig and RegWrDone;
	functConfig_RdStrobe <= RdSel_functConfig and RegRdDone;

	Loc_offlineStartValue <= offlineStartValue;
	offlineStartValue_Sel <= WrSel_offlineStartValue;
	offlineStartValue_WrStrobe <= WrSel_offlineStartValue and RegWrDone;
	offlineStartValue_RdStrobe <= RdSel_offlineStartValue and RegRdDone;

	Loc_onlineStartValue <= onlineStartValue;

	Loc_functionSize <= functionSize;
	functionSize_Sel <= WrSel_functionSize;
	functionSize_WrStrobe <= WrSel_functionSize and RegWrDone;
	functionSize_RdStrobe <= RdSel_functionSize and RegRdDone;

	Loc_currentValue <= currentValue;

	softTrig_functStartTrig0 <= Loc_softTrig(0);
	softTrig_functStartTrig1 <= Loc_softTrig(1);
	softTrig_functStartTrig2 <= Loc_softTrig(2);
	softTrig_functStartTrig3 <= Loc_softTrig(3);
	softTrig_functStartTrig4 <= Loc_softTrig(4);
	softTrig_functStartTrig5 <= Loc_softTrig(5);
	softTrig_functStartTrig6 <= Loc_softTrig(6);
	softTrig_functStartTrig7 <= Loc_softTrig(7);
	softTrig_functStartTrig8 <= Loc_softTrig(8);
	softTrig_functStartTrig9 <= Loc_softTrig(9);
	softTrig_functStartTrig10 <= Loc_softTrig(10);
	softTrig_functStartTrig11 <= Loc_softTrig(11);
	softTrig_functStartTrig12 <= Loc_softTrig(12);
	softTrig_functStartTrig13 <= Loc_softTrig(13);
	softTrig_functStartTrig14 <= Loc_softTrig(14);
	softTrig_functStartTrig15 <= Loc_softTrig(15);
	softTrig_functRestartTrig0 <= Loc_softTrig(16);
	softTrig_functRestartTrig1 <= Loc_softTrig(17);
	softTrig_functRestartTrig2 <= Loc_softTrig(18);
	softTrig_functRestartTrig3 <= Loc_softTrig(19);
	softTrig_functRestartTrig4 <= Loc_softTrig(20);
	softTrig_functRestartTrig5 <= Loc_softTrig(21);
	softTrig_functRestartTrig6 <= Loc_softTrig(22);
	softTrig_functRestartTrig7 <= Loc_softTrig(23);
	softTrig_functRestartTrig8 <= Loc_softTrig(24);
	softTrig_functRestartTrig9 <= Loc_softTrig(25);
	softTrig_functRestartTrig10 <= Loc_softTrig(26);
	softTrig_functRestartTrig11 <= Loc_softTrig(27);
	softTrig_functRestartTrig12 <= Loc_softTrig(28);
	softTrig_functRestartTrig13 <= Loc_softTrig(29);
	softTrig_functRestartTrig14 <= Loc_softTrig(30);
	softTrig_functRestartTrig15 <= Loc_softTrig(31);

	bufferStartAddress <= Loc_bufferStartAddress;
	bufferStartAddress_WrStrobe <= WrSel_bufferStartAddress and RegWrDone;

	Loc_bufferSize <= bufferSize;

	WrSelDec: process (VMEAddr) begin
		WrSel_control <= '0';
		WrSel_faultsEnable <= '0';
		WrSel_functSel <= '0';
		WrSel_functConfig <= '0';
		WrSel_offlineStartValue <= '0';
		WrSel_functionSize <= '0';
		WrSel_softTrig <= '0';
		WrSel_bufferStartAddress <= '0';
		case VMEAddr(12 downto 2) is
			when C_Reg_fgc_control =>
				WrSel_control <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_faultsEnable =>
				WrSel_faultsEnable <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_functSel =>
				WrSel_functSel <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_functConfig =>
				WrSel_functConfig <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_offlineStartValue =>
				WrSel_offlineStartValue <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_functionSize =>
				WrSel_functionSize <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_softTrig =>
				WrSel_softTrig <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_fgc_bufferStartAddress =>
				WrSel_bufferStartAddress <= '1';
				Loc_CRegWrOK <= '1';
			when others =>
				Loc_CRegWrOK <= '0';
		end case;
	end process WrSelDec;

	CRegRdMux: process (VMEAddr, Loc_control, Loc_faultsEnable, Loc_functSel, Loc_functConfig, Loc_offlineStartValue, Loc_functionSize, Loc_softTrig, Loc_bufferStartAddress) begin
		RdSel_functConfig <= '0';
		RdSel_offlineStartValue <= '0';
		RdSel_functionSize <= '0';
		case VMEAddr(12 downto 2) is
			when C_Reg_fgc_control =>
				Loc_CRegRdData <= Loc_control(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_fgc_faultsEnable =>
				Loc_CRegRdData <= Loc_faultsEnable(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_fgc_functSel =>
				Loc_CRegRdData <= Loc_functSel(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_fgc_functConfig =>
				Loc_CRegRdData <= Loc_functConfig(31 downto 0);
				Loc_CRegRdOK <= '1';
				RdSel_functConfig <= '1';
			when C_Reg_fgc_offlineStartValue =>
				Loc_CRegRdData <= Loc_offlineStartValue(31 downto 0);
				Loc_CRegRdOK <= '1';
				RdSel_offlineStartValue <= '1';
			when C_Reg_fgc_functionSize =>
				Loc_CRegRdData <= Loc_functionSize(31 downto 0);
				Loc_CRegRdOK <= '1';
				RdSel_functionSize <= '1';
			when C_Reg_fgc_softTrig =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
			when C_Reg_fgc_bufferStartAddress =>
				Loc_CRegRdData <= Loc_bufferStartAddress(31 downto 0);
				Loc_CRegRdOK <= '1';
			when others =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
		end case;
	end process CRegRdMux;

	CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			CRegRdData <= Loc_CRegRdData;
			CRegRdOK <= Loc_CRegRdOK;
			CRegWrOK <= Loc_CRegWrOk;
		end if;
	end process CRegRdMux_DFF;

	RegRdMux: process (VMEAddr, CRegRdData, CRegRdOK, Loc_status, Loc_busy, Loc_faults_SRFF, Loc_fifoFaults_SRFF, Loc_functionFaults_SRFF, Loc_onlineStartValue, Loc_currentValue, Loc_bufferSize) begin
		case VMEAddr(12 downto 2) is
			when C_Reg_fgc_status =>
				Loc_RegRdData <= Loc_status(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_busy =>
				Loc_RegRdData <= Loc_busy(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_faults =>
				Loc_RegRdData <= Loc_faults_SRFF(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_fifoFaults =>
				Loc_RegRdData <= Loc_fifoFaults_SRFF(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_functionFaults =>
				Loc_RegRdData <= Loc_functionFaults_SRFF(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_onlineStartValue =>
				Loc_RegRdData <= Loc_onlineStartValue(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_currentValue =>
				Loc_RegRdData <= Loc_currentValue(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_fgc_bufferSize =>
				Loc_RegRdData <= Loc_bufferSize(31 downto 0);
				Loc_RegRdOK <= '1';
			when others =>
				Loc_RegRdData <= CRegRdData;
				Loc_RegRdOK <= CRegRdOK;
		end case;
	end process RegRdMux;

	RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			RegRdData <= Loc_RegRdData;
			RegRdOK <= Loc_RegRdOK;
		end if;
	end process RegRdMux_DFF;

	RegRdDone <= Loc_VMERdMem(2) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(1) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(2) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(1) and not CRegWrOK;

	MemRdMux: process (VMEAddr, RegRdData, RegRdDone, RegRdError, coastMemory_RdData, coastMemory_RdDone, coastMemory_RdError) begin
		Sel_coastMemory <= '0';
		if VMEAddr(12 downto 2) >= C_Mem_fgc_coastMemory_Sta and VMEAddr(12 downto 2) <= C_Mem_fgc_coastMemory_End then
			Sel_coastMemory <= '1';
			Loc_MemRdData <= coastMemory_RdData;
			Loc_MemRdDone <= coastMemory_RdDone;
			Loc_MemRdError <= coastMemory_RdError;
		else
			Loc_MemRdData <= RegRdData;
			Loc_MemRdDone <= RegRdDone;
			Loc_MemRdError <= RegRdError;
		end if;
	end process MemRdMux;

	MemRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			MemRdData <= Loc_MemRdData;
			MemRdDone <= Loc_MemRdDone;
			MemRdError <= Loc_MemRdError;
		end if;
	end process MemRdMux_DFF;

	MemWrMux: process (VMEAddr, RegWrDone, RegWrError, coastMemory_WrDone, coastMemory_WrError) begin
		if VMEAddr(12 downto 2) >= C_Mem_fgc_coastMemory_Sta and VMEAddr(12 downto 2) <= C_Mem_fgc_coastMemory_End then
			Loc_MemWrDone <= coastMemory_WrDone;
			Loc_MemWrError <= coastMemory_WrError;
		else
			Loc_MemWrDone <= RegWrDone;
			Loc_MemWrError <= RegWrError;
		end if;
	end process MemWrMux;

	MemWrMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			MemWrDone <= Loc_MemWrDone;
			MemWrError <= Loc_MemWrError;
		end if;
	end process MemWrMux_DFF;

	coastMemory_Addr <= VMEAddr(11 downto 2);
	coastMemory_Sel <= Sel_coastMemory;
	coastMemory_RdMem <= Sel_coastMemory and VMERdMem;
	coastMemory_WrMem <= Sel_coastMemory and VMEWrMem;
	coastMemory_WrData <= VMEWrData;

	AreaRdMux: process (VMEAddr, MemRdData, MemRdDone, ipInfo_RdData, ipInfo_RdDone, ipInfo_RdError, MemRdError) begin
		if VMEAddr(12 downto 5) = C_Submap_fgc_ipInfo then
			RdData <= ipInfo_RdData;
			RdDone <= ipInfo_RdDone;
			RdError <= ipInfo_RdError;
		else
			RdData <= MemRdData;
			RdDone <= MemRdDone;
			RdError <= MemRdError;
		end if;
	end process AreaRdMux;

	AreaWrMux: process (VMEAddr, MemWrDone, ipInfo_WrDone, ipInfo_WrError, MemWrError) begin
		if VMEAddr(12 downto 5) = C_Submap_fgc_ipInfo then
			WrDone <= ipInfo_WrDone;
			WrError <= ipInfo_WrError;
		else
			WrDone <= MemWrDone;
			WrError <= MemWrError;
		end if;
	end process AreaWrMux;


	Reg_ipInfo_sysControl: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_ipInfo_sysControl,
			AutoClrMsk => C_ACM_ipInfo_sysControl,
			Preset => C_PSM_ipInfo_sysControl,
			CReg => Loc_ipInfo_sysControl(15 downto 0)
		);

	Loc_ipInfo_ident <= ipInfo_ident;

	Loc_ipInfo_firmwareVersion <= ipInfo_firmwareVersion;

	Loc_ipInfo_memMapVersion <= ipInfo_memMapVersion;

	ipInfo_sysControl <= Loc_ipInfo_sysControl;

	ipInfo_WrSelDec: process (VMEAddr) begin
		WrSel_ipInfo_sysControl <= '0';
		if VMEAddr(12 downto 5) = C_Submap_fgc_ipInfo then
			case VMEAddr(4 downto 2) is
				when C_Reg_ipInfo_sysControl =>
					WrSel_ipInfo_sysControl <= '1';
					Loc_ipInfo_CRegWrOK <= '1';
				when others =>
					Loc_ipInfo_CRegWrOK <= '0';
			end case;
		else
			Loc_ipInfo_CRegWrOK <= '0';
		end if;
	end process ipInfo_WrSelDec;

	ipInfo_CRegRdMux: process (VMEAddr, Loc_ipInfo_sysControl) begin
		if VMEAddr(12 downto 5) = C_Submap_fgc_ipInfo then
			case VMEAddr(4 downto 2) is
				when C_Reg_ipInfo_sysControl =>
					Loc_ipInfo_CRegRdData <= std_logic_vector(resize(unsigned(Loc_ipInfo_sysControl(15 downto 0)), 32));
					Loc_ipInfo_CRegRdOK <= '1';
				when others =>
					Loc_ipInfo_CRegRdData <= (others => '0');
					Loc_ipInfo_CRegRdOK <= '0';
			end case;
		else
			Loc_ipInfo_CRegRdData <= (others => '0');
			Loc_ipInfo_CRegRdOK <= '0';
		end if;
	end process ipInfo_CRegRdMux;

	ipInfo_CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			ipInfo_CRegRdData <= Loc_ipInfo_CRegRdData;
			ipInfo_CRegRdOK <= Loc_ipInfo_CRegRdOK;
			ipInfo_CRegWrOK <= Loc_ipInfo_CRegWrOk;
		end if;
	end process ipInfo_CRegRdMux_DFF;

	ipInfo_RegRdMux: process (VMEAddr, ipInfo_CRegRdData, ipInfo_CRegRdOK, Loc_ipInfo_ident, Loc_ipInfo_firmwareVersion, Loc_ipInfo_memMapVersion) begin
		if VMEAddr(12 downto 5) = C_Submap_fgc_ipInfo then
			case VMEAddr(4 downto 2) is
				when C_Reg_ipInfo_ident_1 =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_ident(63 downto 32);
					Loc_ipInfo_RegRdOK <= '1';
				when C_Reg_ipInfo_ident_0 =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_ident(31 downto 0);
					Loc_ipInfo_RegRdOK <= '1';
				when C_Reg_ipInfo_firmwareVersion =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_firmwareVersion(31 downto 0);
					Loc_ipInfo_RegRdOK <= '1';
				when C_Reg_ipInfo_memMapVersion =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_memMapVersion(31 downto 0);
					Loc_ipInfo_RegRdOK <= '1';
				when others =>
					Loc_ipInfo_RegRdData <= ipInfo_CRegRdData;
					Loc_ipInfo_RegRdOK <= ipInfo_CRegRdOK;
			end case;
		else
			Loc_ipInfo_RegRdData <= ipInfo_CRegRdData;
			Loc_ipInfo_RegRdOK <= ipInfo_CRegRdOK;
		end if;
	end process ipInfo_RegRdMux;

	ipInfo_RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			ipInfo_RegRdData <= Loc_ipInfo_RegRdData;
			ipInfo_RegRdOK <= Loc_ipInfo_RegRdOK;
		end if;
	end process ipInfo_RegRdMux_DFF;

	ipInfo_RegRdDone <= Loc_VMERdMem(2) and ipInfo_RegRdOK;
	ipInfo_RegWrDone <= Loc_VMEWrMem(1) and ipInfo_CRegWrOK;

	ipInfo_RegRdError <= Loc_VMERdMem(2) and not ipInfo_RegRdOK;
	ipInfo_RegWrError <= Loc_VMEWrMem(1) and not ipInfo_CRegWrOK;

	Loc_ipInfo_MemRdData <= ipInfo_RegRdData;
	Loc_ipInfo_MemRdDone <= ipInfo_RegRdDone;
	Loc_ipInfo_MemRdError <= ipInfo_RegRdError;

	ipInfo_MemRdData <= Loc_ipInfo_MemRdData;
	ipInfo_MemRdDone <= Loc_ipInfo_MemRdDone;
	ipInfo_MemRdError <= Loc_ipInfo_MemRdError;

	Loc_ipInfo_MemWrDone <= ipInfo_RegWrDone;
	Loc_ipInfo_MemWrError <= ipInfo_RegWrError;

	ipInfo_MemWrDone <= Loc_ipInfo_MemWrDone;
	ipInfo_MemWrError <= Loc_ipInfo_MemWrError;

	ipInfo_RdData <= ipInfo_MemRdData;
	ipInfo_RdDone <= ipInfo_MemRdDone;
	ipInfo_WrDone <= ipInfo_MemWrDone;
	ipInfo_RdError <= ipInfo_MemRdError;
	ipInfo_WrError <= ipInfo_MemWrError;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF
