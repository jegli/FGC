-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, RAM access manager --
-- --
-------------------------------------------------------------------------------
--
-- unit name: RAM access manager
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 02/07/2018
--
-- version: 1.0
--
-- description: Coast RAM memory access managaer 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity RAM_access_manager is
	generic (
        -- Width of S_AXI data bus
        g_S_AXI_DATA_WIDTH      : integer    := 64;
        -- Width of RAM address bus
        g_RAM_ADDR_WIDTH        : integer    := 9;
        -- number of channel
        g_FGC_CHANNELS              : integer    := 16
    );
    Port ( 
            coast_buff_sel_o            : out   std_logic;  -- coast buffer selection (buffer 0 or buffer 1)
            coast_trim_i                : in    std_logic;  -- coast trim trigger input   
            coast_mem_ready_fault_o     : out   std_logic;  -- coast memory ready fault
            coast_mem_ready_i           : in    std_logic;  -- coast memory ready input
            coast_enable_i              : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0); -- coast enabel input (one bit per function)
            
            RAM_rd_addr_o               : out   std_logic_vector (g_RAM_ADDR_WIDTH-1 downto 0); -- RAM read address output
            RAM_rd_data_i               : in    t_vector_format;                                -- RAM read data input
            
            vector_o                    : out   t_vector_format;                            -- vector values
            next_vector_i               : in    std_logic_vector (g_FGC_CHANNELS-1 downto 0);   -- next vector request 
            vector_valid_o              : out   std_logic_vector (g_FGC_CHANNELS-1 downto 0);   -- vector is valid 
            
            clk_i                       : in    std_logic;  -- general clock
            reset_i                     : in    std_logic   -- general reset
          );
end RAM_access_manager;

architecture arch_imp of RAM_access_manager is
    ----------------------------------------------------------------------
    -- type instantation
    type t_vector_index_type is array (0 to g_FGC_CHANNELS-1) of unsigned(4 downto 0); 
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_coast_trim                 : std_logic;
    signal s_coast_mem_ready_fault      : std_logic;
    signal s_coast_buff_sel             : std_logic;
    signal s_coast_mem_ready            : std_logic;
    signal s_vector_index               : t_vector_index_type;
    signal s_channel_cnt                : unsigned(3 downto 0);
    signal s_channel_cnt_1              : unsigned(s_channel_cnt'range);
    signal s_channel_cnt_2              : unsigned(s_channel_cnt'range);
    signal s_special_vector_detected    : std_logic_vector (g_FGC_CHANNELS-1 downto 0);
    signal s_vector_valid               : std_logic_vector(vector_valid_o'range);
    signal s_vector_valid_1         : std_logic_vector(vector_valid_o'range);
    signal s_next_vector                : std_logic_vector(next_vector_i'range);
    
    ----------------------------------------------------------------------
    -- state machine instantation
    type t_state is (   IDLE,        
                        SCANNING,
                        WAITING
                    );
    
    signal s_main_state  : t_state; 

begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 
    coast_mem_ready_fault_o <= s_coast_mem_ready_fault;
    coast_buff_sel_o        <= s_coast_buff_sel;
    vector_o                <= RAM_rd_data_i;
    vector_valid_o          <= s_vector_valid_1;
    
    --=============================================================================
    -- latch
    --=============================================================================
    p_latch: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_vector_valid_1  <= (others => '0');
            s_channel_cnt_1  <= (others => '0');
        elsif (rising_edge (clk_i))then
            -- one clock cycle read latency
            s_vector_valid_1 <= s_vector_valid;
            -- add one clock latency to index
            s_channel_cnt_1 <= s_channel_cnt;
            s_channel_cnt_2 <= s_channel_cnt_1;
        end if;
    end process p_latch;
    
    --=============================================================================
    -- coast buffer selection
    --=============================================================================
    p_coast_buffer_selection: process(clk_i,reset_i,coast_mem_ready_i,coast_trim_i)
    begin
        if(reset_i = '1')then
            s_coast_buff_sel    <= '0';
            s_coast_mem_ready   <= '0';
        elsif (rising_edge (clk_i))then
        
            -- buffer selection
            if(coast_mem_ready_i = '1')then
                s_coast_buff_sel    <= not s_coast_buff_sel; -- changed buffer selection when coast_mem_ready is asserted
                s_coast_mem_ready   <= '1';
            elsif (coast_trim_i = '1')then    
                s_coast_mem_ready   <= '0';
            end if;
        end if;
    end process p_coast_buffer_selection;
    
    --=============================================================================
    -- special vector detection
    --=============================================================================
    p_special_vector_detection: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_special_vector_detected  <= (others => '0');
        elsif (rising_edge (clk_i))then
            if(s_main_state = IDLE)then
                s_special_vector_detected  <= (others => '0');
            else
                -- one clock cycle read latency
                if(s_vector_valid_1(to_integer(s_channel_cnt_2)) = '1')then
                    if(RAM_rd_data_i.interval = c_inf_interval)then
                        s_special_vector_detected(to_integer(s_channel_cnt_2)) <= '1';
                    end if;
                end if;
             end if;   
        end if;
    end process p_special_vector_detection;

    --=============================================================================
    -- RAM access management
    --=============================================================================
    p_RAM_access_management: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            RAM_rd_addr_o           <= (others => '0');
            s_channel_cnt           <= (others => '0');
            s_vector_index          <= (others => (others => '0'));
            s_vector_valid          <= (others => '0');
            s_coast_mem_ready_fault <= '0';
            s_next_vector           <= (others => '0');
            s_coast_trim            <= '0';
        elsif (rising_edge (clk_i))then

            -- default
            s_coast_trim            <= coast_trim_i;
            s_vector_valid          <= (others => '0');
            s_coast_mem_ready_fault <= '0';  
            --S-latch
            s_next_vector <= s_next_vector or next_vector_i;

            case s_main_state is
                    when IDLE =>    s_vector_valid  <= (others => '0');
                                    RAM_rd_addr_o   <= (others => '0');
                                    s_vector_index  <= (others => (others => '0'));
                                    s_channel_cnt   <= (others => '0');
                                    
                                    -- the coast functions were loaded in the buffer memory
                                    -- rising edge of trigger
                                    if(coast_trim_i = '1' and s_coast_trim = '0')then
                                        if(s_coast_mem_ready = '1')then
                                            s_main_state <= SCANNING;
                                        else     
                                            -- fault: coast buffer is not ready  
                                            s_coast_mem_ready_fault <= '1';    
                                        end if;
                                    end if;
                                
                when SCANNING =>    -- change channel selection
                                    s_channel_cnt <= s_channel_cnt + 1;
                
                                    -- one channel asked a new vector?
                                    if(s_next_vector(to_integer(s_channel_cnt)) = '1' AND s_special_vector_detected(to_integer(s_channel_cnt)) = '0' AND coast_enable_i(to_integer(s_channel_cnt)) = '1')then
                                        -- RAM address
                                        RAM_rd_addr_o(8 downto 5) <= std_logic_vector(s_channel_cnt); 
                                        RAM_rd_addr_o(4 downto 0) <= std_logic_vector(s_vector_index(to_integer(s_channel_cnt)));
                                        -- increment index
                                        s_vector_index(to_integer(s_channel_cnt)) <= s_vector_index(to_integer(s_channel_cnt)) + 1;
                                        -- vector is valid
                                        s_vector_valid(to_integer(s_channel_cnt)) <= '1';
                                        -- reset 
                                        s_next_vector(to_integer(s_channel_cnt)) <= '0';
                                    end if; 

                                    -- coast trim finished 
                                    if(s_special_vector_detected = coast_enable_i)then
                                        s_main_state <= IDLE;    
                                    end if;

                when others => s_main_state <= IDLE;
                end case;
                
                -- channel counter reset
                if(s_channel_cnt >= g_FGC_CHANNELS-1)then
                   s_channel_cnt <= (others => '0'); 
                end if;
        end if;
    end process p_RAM_access_management;
    
end arch_imp;
