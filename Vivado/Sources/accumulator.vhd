-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, accumulator --
-- --
-------------------------------------------------------------------------------
--
-- unit name: accumulator
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 05/07/2018
--
-- version: 1.0
--
-- description: 32-bit signed accumulator
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity accumulator is
    Port ( 
            -- control interface
            wrap_en_i                   : in    std_logic;  --wrapper enable, 1= counter wrapping enabeld, 0=saturation mode enabled
            
            -- status interface
            saturation_o                : out   std_logic;  --saturation counter bit
            
            -- accumulator interface
            load_start_value_i          : in   std_logic;                                           -- load start value control bit
            start_value_i               : in   std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);    -- start value of the accumulator
            slope_i                     : in   std_logic_vector(c_SLOPE_WIDTH-1 downto 0);          -- increment of the counter
            slope_en_i                  : in   std_logic;                                           -- increment enable 
            dout_o                      : out  std_logic_vector(c_FUNCTION_WIDTH-1 downto 0);       -- accumuator output value
            
            -- general
            inter_clk_en_i              : in    std_logic;  -- accumulator interpolation clock
            clk_i                       : in    std_logic;  -- general clock
            reset_i                     : in    std_logic   -- general reset
          );
end accumulator;

architecture arch_imp of accumulator is
    ----------------------------------------------------------------------
    -- constant instantation 
    constant c_max_value : std_logic_vector(c_FUNCTION_WIDTH-1 downto 0) := x"7fffffff";
    constant c_min_value : std_logic_vector(c_FUNCTION_WIDTH-1 downto 0) := x"80000000";
    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_accu           : std_logic_vector (c_FUNCTION_WIDTH-1 downto 0);
    signal s_adder          : std_logic_vector (c_FUNCTION_WIDTH-1 downto 0);
    signal s_result         : std_logic_vector (c_FUNCTION_WIDTH-1 downto 0);
    signal s_underflow      : std_logic;
    signal s_overflow       : std_logic;
    signal s_inter_clk_en   : std_logic;
    
    ----------------------------------------------------------------------
    -- state machine instantation

begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 
    saturation_o <= s_underflow or s_overflow;
    
    -- in saturation mode the output value is cropped!
    s_result <= c_min_value when (s_accu(s_accu'HIGH) = '1' and slope_i(slope_i'HIGH) = '1' and s_adder(s_adder'HIGH) = '0') and wrap_en_i = '0' else
                c_max_value when (s_accu(s_accu'HIGH) = '0' and slope_i(slope_i'HIGH) = '0' and s_adder(s_adder'HIGH) = '1') and wrap_en_i = '0' else 
                s_adder;
                
    s_adder  <= std_logic_vector(signed(s_accu) + signed(slope_i));
              
    s_underflow <= '1' when (s_accu(s_accu'HIGH) = '1' and slope_i(slope_i'HIGH) = '1' and s_adder(s_adder'HIGH) = '0') and wrap_en_i = '0' else '0';
    s_overflow  <= '1' when (s_accu(s_accu'HIGH) = '0' and slope_i(slope_i'HIGH) = '0' and s_adder(s_adder'HIGH) = '1') and wrap_en_i = '0' else '0';
    
    --output the accumulator value
    dout_o <= s_accu;
    --=============================================================================
    -- latch
    --=============================================================================
    -- generate one clock latched interpolation clock enable signal to be sychronized with control signals
    p_latch: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_inter_clk_en      <= '0';   
        elsif (rising_edge (clk_i))then
            s_inter_clk_en   <= inter_clk_en_i;
        end if;
    end process p_latch;
    
    --=============================================================================
    -- accumulator
    --=============================================================================                 
    p_accumulate: process (clk_i,reset_i) begin
        if(reset_i = '1')then
            s_accu                <= (others => '0');
        elsif rising_edge(clk_i) then
        
            -- interpolation frequency synchronization
            if(s_inter_clk_en = '1')then
                -- loading start value
                if(load_start_value_i = '1')then 
                    s_accu               <= start_value_i;      -- for cartesian coordonate format, start_value is used instead of slope
                else    
                    if(slope_en_i = '1')then
                        s_accu <= s_result;    
                    end if;
                end if;
            end if;
        end if;
    end process p_accumulate;
end arch_imp;
