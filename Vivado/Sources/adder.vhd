-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, adder --
-- --
-------------------------------------------------------------------------------
--
-- unit name: adder
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 05/07/2018
--
-- version: 1.0
--
-- description: adder with two inputs 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity adder is
    generic(    
                -- general input/output width
                g_WIDTH   : integer := 32
    );
    Port ( 
            -- control interface
            wrap_en_i                   : in    std_logic;  --wrapper enable, 1= counter wrapping enabeld, 0=saturation mode enabled
            
            -- status interface
            saturation_o                : out   std_logic;  --saturation counter bit
            
            -- adder interface
            din_0_i                     : in   std_logic_vector(g_WIDTH-1 downto 0);        -- adder input 0
            din_1_i                     : in   std_logic_vector(g_WIDTH-1 downto 0);        -- adder input 1
            dout_o                      : out  std_logic_vector(g_WIDTH-1 downto 0);        -- adder truncated output value
            
            -- general
            clk_i                       : in    std_logic;  -- general clock
            reset_i                     : in    std_logic   -- general reset
          );
end adder;

architecture arch_imp of adder is
    ----------------------------------------------------------------------
    -- constant instantation 
    constant c_max_value : std_logic_vector(g_WIDTH-1 downto 0) := x"7fffffff";
    constant c_min_value : std_logic_vector(g_WIDTH-1 downto 0) := x"80000000";
    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_accu       : std_logic_vector (g_WIDTH-1 downto 0);
    signal s_adder      : std_logic_vector (g_WIDTH-1 downto 0);
    signal s_result     : std_logic_vector (g_WIDTH-1 downto 0);
    signal s_underflow  : std_logic;
    signal s_overflow   : std_logic;
    
    ----------------------------------------------------------------------
    -- state machine instantation

begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 
    saturation_o <= s_underflow or s_overflow;
    
    -- in saturation mode the output value is cropped!
    s_result <= c_min_value when (din_0_i(din_0_i'HIGH) = '1' and din_1_i(din_1_i'HIGH) = '1' and s_adder(s_adder'HIGH) = '0') and wrap_en_i = '0' else
                c_max_value when (din_0_i(din_0_i'HIGH) = '0' and din_1_i(din_1_i'HIGH) = '0' and s_adder(s_adder'HIGH) = '1') and wrap_en_i = '0' else 
                s_adder;
    
    s_adder  <= std_logic_vector(signed(din_0_i) + signed(din_1_i));

    s_underflow <= '1' when (din_0_i(din_0_i'HIGH) = '1' and din_1_i(din_1_i'HIGH) = '1' and s_adder(s_adder'HIGH) = '0') and wrap_en_i = '0' else '0';
    s_overflow  <= '1' when (din_0_i(din_0_i'HIGH) = '0' and din_1_i(din_1_i'HIGH) = '0' and s_adder(s_adder'HIGH) = '1') and wrap_en_i = '0' else '0';

    -- adder output       
    dout_o            <= s_accu;      
    
    --=============================================================================
    -- adder
    --=============================================================================                 
    p_adder: process (clk_i,reset_i) begin
        if(reset_i = '1')then
            s_accu <= (others => '0');
        elsif rising_edge(clk_i) then
            s_accu <= s_result;
        end if;
    end process p_adder;
end arch_imp;
