-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, coast memory --
-- --
-------------------------------------------------------------------------------
--
-- unit name: coast memory
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 04/07/2018
--
-- version: 1.0
--
-- description: coast memory port map
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity coast_memory is
	generic (
		-- number of functions
        g_FGC_CHANNELS          : integer   := 16;
		-- Width of S_AXI data bus
		g_S_AXI_DATA_WIDTH	: integer	:= 64;
		-- Width of S_AXI address bus
		g_S_AXI_ADDR_WIDTH	: integer	:= 32;
		-- Width of RAM address bus
        g_RAM_ADDR_WIDTH    : integer   := 9
	);
	port (
		-- COAST MEMORY INTERFACE
        coast_next_vector_i     : in  std_logic_vector(g_FGC_CHANNELS-1 downto 0);
        coast_vector_valid_o    : out std_logic_vector(g_FGC_CHANNELS-1 downto 0);
        coast_slope_o           : out std_logic_vector(c_SLOPE_WIDTH-1 downto 0);
        coast_interval_o        : out std_logic_vector(c_INTERVAL_WIDTH-1 downto 0);
        coast_mem_ready_i       : in  std_logic;
        coast_mem_ready_fault_o : out std_logic;
        coast_trim_i            : in  std_logic;
        coast_buff_sel_o        : out std_logic;
        coast_enable_i          : in  std_logic_vector(g_FGC_CHANNELS-1 downto 0);
        
        -- RAM INTERFACE FROM CHEBURASHKA
        coastMemory_Addr_i        : in    std_logic_vector(11 downto 2);
        coastMemory_RdData_o      : out   std_logic_vector(31 downto 0);
        coastMemory_WrData_i      : in    std_logic_vector(31 downto 0);
        coastMemory_RdMem_i       : in    std_logic;
        coastMemory_WrMem_i       : in    std_logic;
        coastMemory_RdDone_o      : out   std_logic;
        coastMemory_WrDone_o      : out   std_logic;
        
        --General Clock Signal
        clk_i                   : in  std_logic;
        reset_i                 : in  std_logic

-- **** It was used for an additional AXI interface, now ram access is performed through the cheburashka interface ****     
        -- AXI-LITE INTERFACE
		
--		-- Write address (issued by master, acceped by Slave)
--		S_AXI_AWADDR	: in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
--		-- Write channel Protection type. This signal indicates the
--    		-- privilege and security level of the transaction, and whether
--    		-- the transaction is a data access or an instruction access.
--		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
--		-- Write address valid. This signal indicates that the master signaling
--    		-- valid write address and control information.
--		S_AXI_AWVALID	: in std_logic;
--		-- Write address ready. This signal indicates that the slave is ready
--    		-- to accept an address and associated control signals.
--		S_AXI_AWREADY	: out std_logic;
--		-- Write data (issued by master, acceped by Slave) 
--		S_AXI_WDATA	: in std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
--		-- Write strobes. This signal indicates which byte lanes hold
--    		-- valid data. There is one write strobe bit for each eight
--    		-- bits of the write data bus.    
--		S_AXI_WSTRB	: in std_logic_vector((g_S_AXI_DATA_WIDTH/8)-1 downto 0);
--		-- Write valid. This signal indicates that valid write
--    		-- data and strobes are available.
--		S_AXI_WVALID	: in std_logic;
--		-- Write ready. This signal indicates that the slave
--    		-- can accept the write data.
--		S_AXI_WREADY	: out std_logic;
--		-- Write response. This signal indicates the status
--    		-- of the write transaction.
--		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
--		-- Write response valid. This signal indicates that the channel
--    		-- is signaling a valid write response.
--		S_AXI_BVALID	: out std_logic;
--		-- Response ready. This signal indicates that the master
--    		-- can accept a write response.
--		S_AXI_BREADY	: in std_logic;
--		-- Read address (issued by master, acceped by Slave)
--		S_AXI_ARADDR	: in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
--		-- Protection type. This signal indicates the privilege
--    		-- and security level of the transaction, and whether the
--    		-- transaction is a data access or an instruction access.
--		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
--		-- Read address valid. This signal indicates that the channel
--    		-- is signaling valid read address and control information.
--		S_AXI_ARVALID	: in std_logic;
--		-- Read address ready. This signal indicates that the slave is
--    		-- ready to accept an address and associated control signals.
--		S_AXI_ARREADY	: out std_logic;
--		-- Read data (issued by slave)
--		S_AXI_RDATA	: out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
--		-- Read response. This signal indicates the status of the
--    		-- read transfer.
--		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
--		-- Read valid. This signal indicates that the channel is
--    		-- signaling the required read data.
--		S_AXI_RVALID	: out std_logic;
--		-- Read ready. This signal indicates that the master can
--    		-- accept the read data and response information.
--		S_AXI_RREADY	: in std_logic
	);
end coast_memory;

architecture arch_imp of coast_memory is
    ----------------------------------------------------------------------
    -- constant instantation 
    
    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_mux_sel     : std_logic;
    signal s_reset_n     : std_logic;

    -- cheburashka interface
    signal s_CHEB_wea               : std_logic;
    signal s_CHEB_douta             : std_logic_vector(31 downto 0);
    signal s_RAM_acc_man_addrb      : std_logic_vector(8 downto 0);
    signal s_RAM_acc_man_doutb      : t_vector_format;
    
    -- dpram bus 0
    signal s_CHEB_wea_0             : std_logic_vector(0 downto 0);
    signal s_CHEB_douta_0           : std_logic_vector(31 downto 0);
    signal s_RAM_acc_man_addrb_0    : std_logic_vector(8 downto 0);
    signal s_RAM_acc_man_doutb_0    : std_logic_vector(63 downto 0);
    
    -- dpram bus 1
    signal s_CHEB_wea_1             : std_logic_vector(0 downto 0);
    signal s_CHEB_douta_1           : std_logic_vector(31 downto 0);
    signal s_RAM_acc_man_addrb_1    : std_logic_vector(8 downto 0);
    signal s_RAM_acc_man_doutb_1    : std_logic_vector(63 downto 0);

    ----------------------------------------------------------------------
    -- state machine instantation

    -------------------------------------------------------------
    -- component instantation
    
    component coast_mem_ram_0 is
      port (
          clka : IN STD_LOGIC;
          rsta : IN STD_LOGIC;
          wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
          addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
          dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
          douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
          clkb : IN STD_LOGIC;
          rstb : IN STD_LOGIC;
          web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
          addrb : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
          dinb : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
          doutb : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
          rsta_busy : OUT STD_LOGIC;
          rstb_busy : OUT STD_LOGIC
      );
    end component coast_mem_ram_0;

-------------------------------------------------------------
-- ****************************************************************************************************************
-- **** It was used for an additional AXI interface, now ram access is performed through the cheburashka interface **** 

--    component AXI_lite_to_RAM_interface is
--      generic(
--          -- Width of S_AXI data bus
--          g_S_AXI_DATA_WIDTH    : integer    := 64;
--          -- Width of S_AXI address bus
--          g_S_AXI_ADDR_WIDTH    : integer    := 32;
--          -- Width of RAM address bus
--          g_RAM_ADDR_WIDTH    : integer      := 9
--      );
--      port (
--          -- RAM MEMORY INTERFACE
--          RAM_wr_o        : out std_logic;
--          RAM_addr_o      : out std_logic_vector(g_RAM_ADDR_WIDTH-1 downto 0);
--          RAM_wr_data_o   : out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
          
--          --RAM_rd_addr_o   : out std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
--          RAM_rd_data_i   : in  std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
          
--          -- AXI-LITE INTERFACE
--          -- Global Clock Signal
--          S_AXI_ACLK    : in std_logic;
--          -- Global Reset Signal. This Signal is Active LOW
--          S_AXI_ARESETN    : in std_logic;
          
--          -- Write address (issued by master, acceped by Slave)
--          S_AXI_AWADDR    : in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
--          -- Write channel Protection type. This signal indicates the
--              -- privilege and security level of the transaction, and whether
--              -- the transaction is a data access or an instruction access.
--          S_AXI_AWPROT    : in std_logic_vector(2 downto 0);
--          -- Write address valid. This signal indicates that the master signaling
--              -- valid write address and control information.
--          S_AXI_AWVALID    : in std_logic;
--          -- Write address ready. This signal indicates that the slave is ready
--              -- to accept an address and associated control signals.
--          S_AXI_AWREADY    : out std_logic;
--          -- Write data (issued by master, acceped by Slave) 
--          S_AXI_WDATA    : in std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
--          -- Write strobes. This signal indicates which byte lanes hold
--              -- valid data. There is one write strobe bit for each eight
--              -- bits of the write data bus.    
--          S_AXI_WSTRB    : in std_logic_vector((g_S_AXI_DATA_WIDTH/8)-1 downto 0);
--          -- Write valid. This signal indicates that valid write
--              -- data and strobes are available.
--          S_AXI_WVALID    : in std_logic;
--          -- Write ready. This signal indicates that the slave
--              -- can accept the write data.
--          S_AXI_WREADY    : out std_logic;
--          -- Write response. This signal indicates the status
--              -- of the write transaction.
--          S_AXI_BRESP    : out std_logic_vector(1 downto 0);
--          -- Write response valid. This signal indicates that the channel
--              -- is signaling a valid write response.
--          S_AXI_BVALID    : out std_logic;
--          -- Response ready. This signal indicates that the master
--              -- can accept a write response.
--          S_AXI_BREADY    : in std_logic;
--          -- Read address (issued by master, acceped by Slave)
--          S_AXI_ARADDR    : in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
--          -- Protection type. This signal indicates the privilege
--              -- and security level of the transaction, and whether the
--              -- transaction is a data access or an instruction access.
--          S_AXI_ARPROT    : in std_logic_vector(2 downto 0);
--          -- Read address valid. This signal indicates that the channel
--              -- is signaling valid read address and control information.
--          S_AXI_ARVALID    : in std_logic;
--          -- Read address ready. This signal indicates that the slave is
--              -- ready to accept an address and associated control signals.
--          S_AXI_ARREADY    : out std_logic;
--          -- Read data (issued by slave)
--          S_AXI_RDATA    : out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
--          -- Read response. This signal indicates the status of the
--              -- read transfer.
--          S_AXI_RRESP    : out std_logic_vector(1 downto 0);
--          -- Read valid. This signal indicates that the channel is
--              -- signaling the required read data.
--          S_AXI_RVALID    : out std_logic;
--          -- Read ready. This signal indicates that the master can
--              -- accept the read data and response information.
--          S_AXI_RREADY    : in std_logic
--      );
--      end component AXI_lite_to_RAM_interface;
-- ****************************************************************************************************************
 ------------------------------------------------------------- 
      component RAM_access_manager is
          generic (
              -- Width of S_AXI data bus
              g_S_AXI_DATA_WIDTH      : integer    := 64;
              -- Width of RAM address bus
              g_RAM_ADDR_WIDTH        : integer    := 9;
              -- number of channel
              g_FGC_CHANNELS              : integer    := 16
          );
          Port ( 
                  coast_buff_sel_o            : out   std_logic;
                  coast_trim_i                : in    std_logic;            
                  coast_mem_ready_fault_o     : out   std_logic;
                  coast_mem_ready_i           : in    std_logic;
                  coast_enable_i              : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                  
                  RAM_rd_addr_o               : out   std_logic_vector (g_RAM_ADDR_WIDTH-1 downto 0);
                  RAM_rd_data_i               : in    t_vector_format;
                  
                  vector_o                    : out   t_vector_format;
                  next_vector_i               : in    std_logic_vector (g_FGC_CHANNELS-1 downto 0);
                  vector_valid_o              : out   std_logic_vector (g_FGC_CHANNELS-1 downto 0);
                  
                  clk_i                       : in    std_logic;
                  reset_i                     : in    std_logic
                );
      end component RAM_access_manager;

begin  
   
   coast_buff_sel_o <= s_mux_sel;
   s_reset_n        <= not reset_i;
   
  -- PORT MAP --
-- ****************************************************************************************************************  
-- **** It was used for an additional AXI interface, now ram access is performed through the cheburashka interface **** 
  
--  AXI_lite_to_RAM_interface_0: AXI_lite_to_RAM_interface
--    generic map(
--        g_S_AXI_DATA_WIDTH => g_S_AXI_DATA_WIDTH,
--        g_S_AXI_ADDR_WIDTH => g_S_AXI_ADDR_WIDTH,
--        g_RAM_ADDR_WIDTH   => g_RAM_ADDR_WIDTH
--    )
--    port map(
--        -- RAM MEMORY INTERFACE
--        RAM_wr_o        => s_AXI_wea,
--        RAM_addr_o      => s_AXI_addra,
--        RAM_wr_data_o   => s_AXI_dina,
--        RAM_rd_data_i   => s_AXI_douta,
        
--        S_AXI_ACLK => clk_i,
--        S_AXI_ARESETN => s_reset_n,
--        S_AXI_AWADDR => S_AXI_AWADDR,
--        S_AXI_AWPROT => S_AXI_AWPROT,
--        S_AXI_AWVALID => S_AXI_AWVALID,
--        S_AXI_AWREADY => S_AXI_AWREADY,
--        S_AXI_WDATA => S_AXI_WDATA,   
--        S_AXI_WSTRB => S_AXI_WSTRB,   
--        S_AXI_WVALID => S_AXI_WVALID,   
--        S_AXI_WREADY => S_AXI_WREADY,   
--        S_AXI_BRESP => S_AXI_BRESP,    
--        S_AXI_BVALID => S_AXI_BVALID,   
--        S_AXI_BREADY => S_AXI_BREADY,   
--        S_AXI_ARADDR => S_AXI_ARADDR,    
--        S_AXI_ARPROT => S_AXI_ARPROT,   
--        S_AXI_ARVALID => S_AXI_ARVALID,   
--        S_AXI_ARREADY => S_AXI_ARREADY,   
--        S_AXI_RDATA => S_AXI_RDATA,    
--        S_AXI_RRESP => S_AXI_RRESP,   
--        S_AXI_RVALID => S_AXI_RVALID,   
--        S_AXI_RREADY => S_AXI_RREADY  
--    );

-- ****************************************************************************************************************
    -------------------------------------------------------------
    RAM_access_manager_0:  RAM_access_manager
        generic map(
            g_S_AXI_DATA_WIDTH => g_S_AXI_DATA_WIDTH,
            g_RAM_ADDR_WIDTH => g_RAM_ADDR_WIDTH,
            g_FGC_CHANNELS => g_FGC_CHANNELS
        )
        Port map( 
                coast_buff_sel_o => s_mux_sel,
                RAM_rd_addr_o => s_RAM_acc_man_addrb,
                coast_trim_i => coast_trim_i,            
                coast_mem_ready_fault_o => coast_mem_ready_fault_o,
                coast_mem_ready_i => coast_mem_ready_i,
                coast_enable_i => coast_enable_i,
                RAM_rd_data_i=> s_RAM_acc_man_doutb,
                vector_o.slope => coast_slope_o,
                vector_o.interval => coast_interval_o,
                next_vector_i => coast_next_vector_i,
                vector_valid_o => coast_vector_valid_o,
                clk_i => clk_i,
                reset_i => reset_i
         );
    -------------------------------------------------------------     
    coast_buffer_0: coast_mem_ram_0
           port map(
               clka => clk_i,
               rsta => reset_i,
               wea  => s_CHEB_wea_0,
               addra => coastMemory_Addr_i,
               dina => coastMemory_WrData_i,
               douta => s_CHEB_douta_0,
               clkb => clk_i,
               rstb => reset_i,
               web => (others => '0'),
               addrb => s_RAM_acc_man_addrb,
               dinb => (others => '0'),
               doutb => s_RAM_acc_man_doutb_0,
               rsta_busy => open,
               rstb_busy => open
           );
    -------------------------------------------------------------       
    coast_buffer_1: coast_mem_ram_0
           port map(
               clka => clk_i,
               rsta => reset_i,
               wea  => s_CHEB_wea_1,
               addra => coastMemory_Addr_i,
               dina => coastMemory_WrData_i,
               douta => s_CHEB_douta_1,
               clkb => clk_i,
               rstb => reset_i,
               web => (others => '0'),
               addrb => s_RAM_acc_man_addrb,
               dinb => (others => '0'),
               doutb => s_RAM_acc_man_doutb_1,
               rsta_busy => open,
               rstb_busy => open
           );
    -----------------------------------------------------------------------

    p_cheb_done: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            coastMemory_RdDone_o <= '0';
            coastMemory_WrDone_o <= '0';
        elsif (rising_edge (clk_i))then
            coastMemory_RdDone_o <= coastMemory_RdMem_i;
            coastMemory_WrDone_o <= coastMemory_WrMem_i;
        end if;
    end process;
    
    --=============================================================================
    -- multiplexer 
    --=============================================================================
    -- double buffering management
    s_CHEB_wea_0(0)                  <= coastMemory_WrMem_i when s_mux_sel = '0' else '0';
    s_CHEB_wea_1(0)                  <= coastMemory_WrMem_i when s_mux_sel = '1' else '0';
    coastMemory_RdData_o              <= s_CHEB_douta_0 when s_mux_sel = '0' else s_CHEB_douta_1; 
    s_RAM_acc_man_doutb.slope       <= s_RAM_acc_man_doutb_0(31 downto 0) when s_mux_sel = '1' else s_RAM_acc_man_doutb_1(31 downto 0);   
    s_RAM_acc_man_doutb.interval    <= s_RAM_acc_man_doutb_0(63 downto 32) when s_mux_sel = '1' else s_RAM_acc_man_doutb_1(63 downto 32);   
    
end arch_imp;

			