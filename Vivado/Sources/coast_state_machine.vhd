-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, coast state machine --
-- --
-------------------------------------------------------------------------------
--
-- unit name: coast state machine
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 05/07/2018
--
-- version: 1.0
--
-- description: state machine managing the coast trim generation
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity coast_state_machine is
    Port ( 
            -- control interface
            coast_mode_i                : in    std_logic;  -- coast mode enable input
            coast_trim_i                : in    std_logic;  -- coast trim trigger
            
            -- accumulator interface
            slope_o                     : out   std_logic_vector(c_SLOPE_WIDTH-1 downto 0);  -- slope increment value
            slope_en_o                  : out   std_logic;                      -- slope increment enable
            
            -- RAM interface
            next_vector_o               : out   std_logic;          -- next vector request
            vector_valid_i              : in    std_logic;          -- vector values are valid 
            vector_i                    : in    t_vector_format;    -- vector values
            
            -- Status
            coast_busy_o                : out   std_logic;  -- coast busy status bit      
            
            -- general
            inter_clk_en_i              : in    std_logic;  -- interpolation clock
            clk_i                       : in    std_logic;  -- general clock
            reset_i                     : in    std_logic   -- general reset
          );
end coast_state_machine;

architecture arch_imp of coast_state_machine is
    ----------------------------------------------------------------------
    -- constant instantation 
    
    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_coast_trim             : std_logic;
    signal s_busy                   : std_logic;
    signal s_interval_cnt           : unsigned(vector_i.interval'range); 
    signal s_vector_valid           : std_logic;
    signal s_vector                 : t_vector_format;
    signal s_coast_trim_finished    : std_logic;
    ----------------------------------------------------------------------
    -- state machine instantation
    type t_state is (   IDLE,      
                        WAITING_NEXT_VECTOR,
                        COAST_PLAYING
                    );
    
    signal s_main_state  : t_state; 
begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 
    coast_busy_o <= s_busy;
    --=============================================================================
    -- latch
    --=============================================================================
    p_latch: process(clk_i, reset_i, vector_valid_i, inter_clk_en_i)
    begin
        if(reset_i = '1')then
            s_vector_valid          <= '0';
        elsif (rising_edge (clk_i))then
            if(s_vector_valid = '0')then
                s_vector_valid <= vector_valid_i;
            else
                -- interpolation clock synchronization
                if(inter_clk_en_i = '1')then
                    s_vector_valid <= '0';
                end if;
            end if;
        end if;
    end process p_latch;
    
    --=============================================================================
    -- main state machine 
    --=============================================================================
    p_main_state_machine: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            slope_o                 <= (others => '0');
            slope_en_o              <= '0';
            s_busy                  <= '0';
            s_coast_trim            <= '0';
            s_main_state            <= IDLE;
            next_vector_o           <= '0';
            s_interval_cnt          <= (others => '0');
            s_coast_trim_finished   <= '0'; 
            s_vector.slope          <= (others => '0');
            s_vector.interval       <= (others => '0');
        elsif (rising_edge (clk_i))then
            --default
            s_coast_trim    <= coast_trim_i;
            next_vector_o   <= '0';
            
            if(coast_mode_i = '1')then
                case s_main_state is
                    when IDLE =>    --reset
                                    slope_o                 <= (others => '0');
                                    slope_en_o              <= '0';
                                    s_busy                  <= '0';
                                    s_interval_cnt          <= (others => '0');
                                    s_coast_trim_finished   <= '0';
                                    s_vector.slope          <= (others => '0');
                                    s_vector.interval       <= (others => '0');
                                    
                                    -- waiting rising edge of trigger to start the state machine 
                                    if(s_coast_trim = '0' and coast_trim_i = '1')then
                                       s_main_state     <= WAITING_NEXT_VECTOR; 
                                       s_busy           <= '1';
                                        -- request the next vector (first vector)
                                        next_vector_o   <= '1';
                                        
                                    end if;
                    when WAITING_NEXT_VECTOR =>
                                    -- copy new valid vector 
                                    if(s_vector_valid = '1')then
                                        -- interpolation clock synchronization
                                        if(inter_clk_en_i = '1')then
                                            -- start incrementation 
                                            slope_en_o      <= '1';
                                            -- copy vector values
                                            s_interval_cnt  <= unsigned(s_vector.interval)-1;
                                            slope_o         <= s_vector.slope; 
                                            
                                            -- if coast trim end was detected => go to IDLe state else start new vector playing
                                            if( s_coast_trim_finished = '0')then
                                                s_main_state     <= COAST_PLAYING; 
                                            else
                                                s_main_state <= IDLE;
                                            end if;
                                        end if; 
                                    else    
                                        -- copy new valid vector 
                                        if(vector_valid_i = '1')then
                                            s_vector <= vector_i;
                                            -- end vector detected
                                            if(vector_i.interval = c_inf_interval)then
                                                s_coast_trim_finished <= '1'; 
                                            end if;
                                        end if;
                                    end if;         
                                    
                    when COAST_PLAYING =>
                                    -- interpolation clock synchronization
                                    if(inter_clk_en_i = '1')then
                                        -- decrement counter
                                        s_interval_cnt <= s_interval_cnt - 1;
                                        
                                        --read next vector
                                        if(s_interval_cnt = 1)then
                                            next_vector_o   <= '1';
                                            s_main_state    <= WAITING_NEXT_VECTOR;
                                        end if;
                                     end if;  
                    when others => s_main_state <= IDLE;
                 end case;
                 
            else --coast mode disabled
                --reset
                slope_o                 <= (others => '0');
                slope_en_o              <= '0';
                s_busy                  <= '0';
                s_main_state            <= IDLE;
                next_vector_o           <= '0';
                s_interval_cnt          <= (others => '0');
                s_coast_trim_finished   <= '0'; 
            end if;
        end if;
    end process p_main_state_machine;

end arch_imp;
