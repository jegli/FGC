-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, fault manager --
-- --
-------------------------------------------------------------------------------
--
-- unit name: fault manager
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 06/07/2018
--
-- version: 1.0
--
-- description: fault management
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity fault_manager is
    Generic (
                g_FGC_CHANNELS : integer := 16
        );
    Port ( 
                --clear faults
                clr_faults_i    : in    std_logic;
                -- fault enable 
                fault_en_0_i    : in    std_logic;
                fault_en_1_i    : in    std_logic;
                fault_en_2_i    : in    std_logic;
                fault_en_3_i    : in    std_logic;
                fault_en_4_i    : in    std_logic;
                fault_en_5_i    : in    std_logic;
                
                -- fault inputs
                fault_0_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                fault_1_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                fault_2_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                fault_3_i       : in    std_logic_vector(g_FGC_CHANNELS-1 downto 0);
                fault_4_i       : in    std_logic;
                fault_5_i       : in    std_logic;
                
                --fault output
                fault_output_o  : out   std_logic;
                
                clk_i           : in    std_logic;
                reset_i         : in    std_logic
          );
end fault_manager;

architecture arch_imp of fault_manager is
    ----------------------------------------------------------------------
    -- constant instantation 

    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_fault_0        : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fault_1        : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fault_2        : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fault_3        : std_logic_vector(g_FGC_CHANNELS-1 downto 0);
    signal s_fault_trig_0   : std_logic;
    signal s_fault_trig_1   : std_logic;
    signal s_fault_trig_2   : std_logic;
    signal s_fault_trig_3   : std_logic;
    signal s_fault_trig_4   : std_logic;
    signal s_fault_trig_5   : std_logic;
    ----------------------------------------------------------------------
    -- state machine instantation

begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 
    fault_output_o <= s_fault_trig_0 or s_fault_trig_1 or s_fault_trig_2 or s_fault_trig_3 or s_fault_trig_4 or s_fault_trig_5;
    --=============================================================================
    -- fault manager process
    --=============================================================================                 
    p_fault_manager: process (clk_i,reset_i) 
    begin
        if(reset_i = '1')then
            s_fault_0       <= (others => '0');
            s_fault_1       <= (others => '0');
            s_fault_2       <= (others => '0');
            s_fault_3       <= (others => '0');
            s_fault_trig_0  <= '0'; 
            s_fault_trig_1  <= '0'; 
            s_fault_trig_2  <= '0'; 
            s_fault_trig_3  <= '0'; 
            s_fault_trig_4  <= '0'; 
            s_fault_trig_5  <= '0'; 
        elsif rising_edge(clk_i) then
            
            -- clear faults 
            if(clr_faults_i = '0')then
                if(unsigned(fault_0_i) /= 0 and fault_en_0_i = '1')then
                    s_fault_0       <= s_fault_0 or fault_0_i; 
                    s_fault_trig_0  <= '1'; 
                end if; 
                
                if(unsigned(fault_1_i) /= 0 and fault_en_1_i = '1')then
                    s_fault_1       <= s_fault_1 or fault_1_i; 
                    s_fault_trig_1  <= '1'; 
                end if; 
                
                if(unsigned(fault_2_i) /= 0 and fault_en_2_i = '1')then
                    s_fault_2       <= s_fault_2 or fault_2_i; 
                    s_fault_trig_2  <= '1'; 
                end if; 
                
                if(unsigned(fault_3_i) /= 0 and fault_en_3_i = '1')then
                    s_fault_3       <= s_fault_3 or fault_3_i; 
                    s_fault_trig_3  <= '1';  
                end if; 

                if(s_fault_trig_4 = '0' and fault_en_4_i = '1')then
                    s_fault_trig_4 <= fault_4_i; 
                end if; 
                
                if(s_fault_trig_5 = '0' and fault_en_5_i = '1')then
                    s_fault_trig_5 <= fault_5_i; 
                end if; 
            else
                s_fault_0       <= (others => '0');
                s_fault_1       <= (others => '0');
                s_fault_2       <= (others => '0');
                s_fault_3       <= (others => '0');
                s_fault_trig_0  <= '0'; 
                s_fault_trig_1  <= '0'; 
                s_fault_trig_2  <= '0'; 
                s_fault_trig_3  <= '0'; 
                s_fault_trig_4  <= '0'; 
                s_fault_trig_5  <= '0'; 
            end if;
        end if;
    end process p_fault_manager;
end arch_imp;
