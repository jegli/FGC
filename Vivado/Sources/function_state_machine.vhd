-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, function state machine --
-- --
-------------------------------------------------------------------------------
--
-- unit name: function state machine
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 05/07/2018
--
-- version: 1.0
--
-- description: state machine managing the function generation
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity function_state_machine is
    Port ( 
            -- control interface
            enable_i                        : in    std_logic;  -- general enable
            start_i                         : in    std_logic;  -- start trigger input
            restart_i                       : in    std_logic;  -- restart trigger input
            coast_enable_i                  : in    std_logic;  -- coast enable bit
            coast_start_i                   : in    std_logic;  -- coast start trigger bit
            coast_stop_i                    : in    std_logic;  -- coast stop trigger bit
            coast_mode_o                    : out   std_logic;  -- coast mode output
            interpol_disable_i              : in    std_logic;  -- disable accumulator interpolation (if '1', cartesion coordinate format is applied)
            clear_function_after_coast_i    : in    std_logic;  -- reset the function (fifo, accu, ...) after end of coast
            
            -- accumulator interface
            start_value_i               : in    std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);   -- start value for the accumulator (from register)
            load_start_value_o          : out   std_logic;                                          -- load the start value of the accumulator
            accu_start_value_o          : out   std_logic_vector(c_START_VALUE_WIDTH-1 downto 0);   -- start value for the accumulator (to accumulator)
            slope_o                     : out   std_logic_vector(c_SLOPE_WIDTH-1 downto 0);         -- slope increment value
            slope_en_o                  : out   std_logic;                                          -- slope increment enable bit
            fgc_reset_o                 : out   std_logic;                                          -- accumulator and fifo reset
            
            -- FIFO interface
            fifo_rd_o                   : out   std_logic;                                      -- fifo read request
            fifo_empty_i                : in    std_logic;                                      -- fifo empty stauts
            fifo_rd_info_i              : in    std_logic_vector (c_INFO_WIDTH-1 downto 0);     -- fifo read information 
            fifo_vector_i               : in    t_vector_format;                                -- fifo vector values (slope, interval)
            fifo_rd_valid_i             : in    std_logic;                                      -- fifo vector values are valid
            
            --buffer selection
            fgc_buffer_sel_update_i     : in    std_logic;                      -- buffer selection update from fifo manager
            fgc_buffer_sel_i            : in    std_logic;                      -- buffer selection value
            
            -- status interface
            funct_busy_o                : out   std_logic;                      -- busy signal, 1 = the function generation is running
            fifo_empty_fault_o          : out   std_logic;                      -- fifo emty fault detected
            cycle_mismatch_fault_o      : out   std_logic;                      -- cycle mismatch fault detected
            
            inter_clk_en_i              : in    std_logic;                      -- interpolation clock
            clk_i                       : in    std_logic;                      -- general clock
            reset_i                     : in    std_logic                       -- reset clock (active High)
          );
end function_state_machine;

architecture arch_imp of function_state_machine is
    ----------------------------------------------------------------------
    -- constant instantation 
    
    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_busy                       : std_logic;
    signal s_busy_1                     : std_logic;
    signal s_busy_2                     : std_logic;
    signal s_start                      : std_logic;
    signal s_restart                    : std_logic;
    signal s_coast_start                : std_logic;
    signal s_coast_stop                 : std_logic;
    signal s_interval_cnt               : unsigned(fifo_vector_i.interval'range);  
    signal s_buffer_select              : std_logic;
    signal s_fgc_buffer_sel_update      : std_logic;
    signal s_fgc_buffer_sel_update_1    : std_logic;
    signal s_fgc_buffer_sel_update_2    : std_logic;
    signal s_fgc_buffer_sel             : std_logic;
    signal s_fgc_buffer_sel_1           : std_logic;
    signal s_fifo_empty_fault           : std_logic;
    signal s_cycle_mismatch_fault       : std_logic;
    signal s_funct_start_fault          : std_logic;
    signal s_in_reset                   : std_logic;
    signal s_coast_mode                 : std_logic;
    signal s_special_vector_detected    : std_logic;
    signal s_fifo_rd                    : std_logic;
    signal s_clear_function_after_coast : std_logic;
    ----------------------------------------------------------------------
    -- state machine instantation
    type t_state is (   IDLE,   
                        LOADING_START_VALUE,     
                        FUNCT_PLAYING
                    );
                    
    type t_coast_state is   (   IDLE,        
                                COAST
                            );
    
    signal s_main_state  : t_state; 
    signal s_coast_state : t_coast_state;
begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 
    fifo_empty_fault_o <= s_fifo_empty_fault;
    cycle_mismatch_fault_o <= s_cycle_mismatch_fault;
    coast_mode_o <= s_coast_mode;
    -- general reset when fault was detected
    fgc_reset_o <= s_cycle_mismatch_fault or s_fifo_empty_fault or s_funct_start_fault or s_in_reset;
    -- fifo read request
    fifo_rd_o <= s_fifo_rd;
    --=============================================================================
    -- buffer sel updater
    --=============================================================================
    p_buffer_sel_updater: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_buffer_select <= '0';
        elsif (rising_edge (clk_i))then
            if(s_fgc_buffer_sel_update_2 = '0' and s_fgc_buffer_sel_update_1 = '1')then
                s_buffer_select <= s_fgc_buffer_sel_1;
            end if;
        end if;
    end process p_buffer_sel_updater;
    
    --=============================================================================
    -- latch
    --=============================================================================
    p_cdc: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_restart                   <= '0';
            s_coast_start               <= '0';
            s_coast_stop                <= '0';
            s_busy_1                    <= '0';
            s_busy_2                    <= '0';
            s_fgc_buffer_sel_update     <= '0';
            s_fgc_buffer_sel_update_1   <= '0';
            s_fgc_buffer_sel_update_2   <= '0';
            s_fgc_buffer_sel            <= '0';
            s_fgc_buffer_sel_1          <= '0';
            funct_busy_o                <= '0';
        elsif (rising_edge (clk_i))then
            -- default
            s_restart           <= restart_i;
            s_coast_start       <= coast_start_i;
            s_coast_stop        <= coast_stop_i;
            
            --CDC
            s_fgc_buffer_sel_update     <= fgc_buffer_sel_update_i;
            s_fgc_buffer_sel_update_1   <= s_fgc_buffer_sel_update;
            s_fgc_buffer_sel_update_2   <= s_fgc_buffer_sel_update_1;
            s_fgc_buffer_sel            <= fgc_buffer_sel_i;
            s_fgc_buffer_sel_1          <= s_fgc_buffer_sel;
            
            -- busy output is synchronized with the adder output first value
            if(inter_clk_en_i = '1')then
                s_busy_1 <= s_busy;
            end if;
            
            s_busy_2        <= s_busy_1;
            funct_busy_o    <= s_busy_2;
        end if;
    end process p_cdc;
    
    --=============================================================================
    -- special vector detection
    --=============================================================================
    p_special_vector_detection: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_special_vector_detected <= '0';
        elsif (rising_edge (clk_i))then
            -- detection of the special vector
            if(s_special_vector_detected = '0')then
                if(fifo_vector_i.slope = c_zero_slope and fifo_vector_i.interval = c_inf_interval)then
                    s_special_vector_detected <= '1';
                end if;
            elsif(restart_i = '1')then
                s_special_vector_detected <= '0';
            end if;
        end if;
    end process p_special_vector_detection;
    
    --=============================================================================
    -- main state machine 
    --=============================================================================
    p_main_state_machine: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_start                     <= '0';
            load_start_value_o          <= '0';
            accu_start_value_o          <= (others => '0');
            slope_o                     <= (others => '0');
            slope_en_o                  <= '0';
            s_busy                      <= '0';
            s_main_state                <= IDLE;
            s_interval_cnt              <= (others => '0');
            s_fifo_rd                   <= '0';
            s_funct_start_fault         <= '0';
            s_fifo_empty_fault          <= '0';
            s_cycle_mismatch_fault      <= '0';
            s_in_reset                  <= '1';
        elsif (rising_edge (clk_i))then
            --default
            s_fifo_rd               <= '0';
            s_funct_start_fault     <= '0';
            s_fifo_empty_fault      <= '0';
            s_cycle_mismatch_fault  <= '0';
            s_in_reset              <= '0';
            
            -- set enable to '0' to perfom a reset
            if(enable_i = '1')then
                --coast mode freeze the process
                if(s_coast_mode = '0')then
                -- interpolation clock synchronization
                    if(inter_clk_en_i = '1')then
                        --default
                        s_start             <= start_i;
                        load_start_value_o  <= '0';
                    
                        case s_main_state is
                            when IDLE =>    --reset
                                            load_start_value_o  <= '0';
                                            accu_start_value_o  <= (others => '0');
                                            slope_o             <= (others => '0');
                                            slope_en_o          <= '0';
                                            s_busy              <= '0';
                                            s_main_state        <= IDLE;
                                            s_interval_cnt      <= (others => '0');
                                            
                                            -- waiting rising edge of start trigger to start the state machine 
                                            if(start_i = '1' and s_start = '0')then 
                                               s_main_state     <= LOADING_START_VALUE;

                                               -- function generator is busy
                                               s_busy           <= '1';
                                            end if;
                                            
                            when LOADING_START_VALUE => 
                                            -- loading the start value  
                                            load_start_value_o  <= '1';
                                            
                                             -- if interpolation is disabled, cartesian coordonate format is used instead of vector format              
                                            if(interpol_disable_i = '1')then
                                                accu_start_value_o  <= fifo_vector_i.slope;
                                            else
                                                -- load first vector
                                                slope_o             <= fifo_vector_i.slope;
                                                accu_start_value_o  <= start_value_i;
                                            end if;
    
                                            -- init interval counter
                                            s_interval_cnt  <= unsigned(fifo_vector_i.interval)-1; -- start value count as a part (increment) of the vector
    
                                            -- verify the first vector in the fifo 
                                            if(fifo_rd_info_i(c_FUNCT_START) = '1')then
                                                -- read next vector only if available
                                                if(fifo_empty_i = '0')then
                                                    s_fifo_rd       <= '1';
                                                end if;
                                            
                                                -- change state 
                                                s_main_state        <= FUNCT_PLAYING; 
                                            else
                                                -- fault detected 
                                                s_main_state            <= IDLE; 
                                                s_funct_start_fault     <= '1';
                                            end if;
            
                            when FUNCT_PLAYING =>
                                            -- start incrementation
                                            slope_en_o      <= not interpol_disable_i;
                            
                                            -- decrement counter
                                            s_interval_cnt <= s_interval_cnt - 1;
                                            
                                            --read next vector when reached the length of the vector or when coast mode stopped and coast mode was started with a special vector
                                            if(s_interval_cnt = 0)then
                                                -- read next vector only if available
                                                if(fifo_empty_i = '0')then
                                                    s_fifo_rd       <= '1';
                                                end if;
                                                
                                                -- if interpolation is disabled, cartesian coordonate format is used instead of vector format
                                                if(interpol_disable_i = '1')then
                                                    accu_start_value_o  <= fifo_vector_i.slope;
                                                    load_start_value_o  <= '1';
                                                else
                                                    -- load next vector
                                                    slope_o         <= fifo_vector_i.slope;
                                                end if;
                                                
                                                -- init interval counter
                                                s_interval_cnt  <= unsigned(fifo_vector_i.interval)-1; 
                                                -- change state
                                                s_main_state    <= FUNCT_PLAYING;
                                                
                                                -- interval counter reset. After coast mode, the next vector will automatically played
                                                if(s_special_vector_detected = '1')then
                                                    s_interval_cnt  <= (others => '0');
                                                end if;
                                                
                                                -- end vector detected -> function generation is finsished
                                                if(fifo_rd_info_i(c_FUNCT_END) = '1')then
                                                    slope_en_o      <= '0';
                                                    s_busy          <= '0';
                                                    s_main_state    <= IDLE; 
                                                end if;
                                            end if;
                     
                            when others => s_main_state <= IDLE;
                         end case;
                     end if; 
                 else
                    -- stop function generation in coast mode
                    slope_en_o <= '0';
                    load_start_value_o  <= '0';
                 end if;   
             else --enable_n    
                --general reset
                load_start_value_o          <= '0';
                accu_start_value_o          <= (others => '0');
                slope_o                     <= (others => '0');
                slope_en_o                  <= '0';
                s_busy                      <= '0';
                s_main_state                <= IDLE;
                s_interval_cnt              <= (others => '0');
                s_in_reset                  <= '1';
             end if;   

             -- after end coast, if the clear_function_after_coast bit of control register is one -> reset the function generator
             if(s_clear_function_after_coast = '1')then
                s_main_state           <= IDLE;
                s_in_reset             <= '1';
             end if;

             -- only if function state machine is running
             if(s_busy = '1')then
                 -- mismatch between the cycle stored in the FIFO and the current cycle  or function start not correctly detected
                 if(s_buffer_select /= fifo_rd_info_i(c_CYCLE_NBR) or s_funct_start_fault = '1')then
                     s_cycle_mismatch_fault <= '1';
                     s_main_state           <= IDLE;
                     s_busy                 <= '0';
                 end if;    
                 -- fifo is empty
                 if(fifo_empty_i = '1')then
                     s_fifo_empty_fault     <= '1';
                     s_main_state           <= IDLE;
                     s_busy                 <= '0';
                 end if;
             end if;      
        end if;
    end process p_main_state_machine;
    
    --=============================================================================
    -- coast mode
    --=============================================================================
    p_coast_mode: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_coast_mode                    <= '0';
            s_coast_state                   <= IDLE;
            s_clear_function_after_coast    <= '0';
        elsif (rising_edge (clk_i))then

            -- default
            s_clear_function_after_coast    <= '0';
            
            if(enable_i = '1')then
                case s_coast_state is
                                    
                    when IDLE  =>   --default 
                                    s_coast_mode <= '0';
                                    
                                    -- must be during a cycle and coast mode must be enabled
                                    if(s_busy = '1')then
                                        -- start coast:
                                        -- 1) when start trig is received (rising edge)
                                        -- 2) when the special vector was detected
                                        if((s_coast_start = '0' and coast_start_i = '1') or (s_special_vector_detected = '1' and s_fifo_rd = '1'))then
                                            s_coast_state <= COAST;
                                            s_coast_mode  <= '1';
                                        end if;    
                                    end if;
                                    
                    when COAST =>   -- stop coast:
                                    -- 1) when stop trig is received (rising edge)
                                    -- 2) for special vector, when restart trig is received (rising edge)
                                    if((s_coast_stop = '0' and coast_stop_i = '1') or (s_special_vector_detected = '1' and s_restart = '0' and restart_i = '1'))then
                                        -- stop coast mode
                                        s_coast_state <= IDLE;
                                        s_coast_mode  <= '0';
                                        -- reset the 
                                        if(clear_function_after_coast_i = '1')then
                                            s_clear_function_after_coast <= '1';
                                        end if;
                                    end if;

                    when others => s_coast_state <= IDLE;
                 end case;
             else --enable_n
                s_coast_mode    <= '0';
                s_coast_state   <= IDLE;
             end if;
        end if;
    end process p_coast_mode;
end arch_imp;
