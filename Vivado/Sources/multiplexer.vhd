-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Multiplexer --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Multiplexer
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 05/07/2018
--
-- version: 1.0
--
-- description: variable input number multiplexer
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity multiplexer is
    Port ( 
            sel_i   : in    std_logic_vector(1 downto 0);
            --input
            in0_i    : in    std_logic;
            in1_i    : in    std_logic;
            in2_i    : in    std_logic;
            --ouput
            out_o   : out   std_logic
          );
end multiplexer;

architecture arch_imp of multiplexer is
begin
    out_o <= in0_i when sel_i = "00" else
             in1_i when sel_i = "01" else
             in2_i when sel_i = "10" else
             '0';
end arch_imp;
