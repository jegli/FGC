-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, prescaler --
-- --
-------------------------------------------------------------------------------
--
-- unit name: prescaler
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 06/07/2018
--
-- version: 1.0
--
-- description: main clock frequency division 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity prescaler is
    Generic(
            g_PRESCALER_VALUE     : integer := 625
          );
    Port ( 
            inter_clk_en_o              : out   std_logic;

            clk_i                       : in    std_logic;
            reset_i                     : in    std_logic  
          );
end prescaler;

architecture arch_imp of prescaler is
    ----------------------------------------------------------------------
    -- constant instantation 

    ----------------------------------------------------------------------
    -- type instantation
    
    ----------------------------------------------------------------------
    -- signals instantation 
    signal s_clk_cnt : unsigned( f_log2(g_PRESCALER_VALUE)-1 downto 0);
    ----------------------------------------------------------------------
    -- state machine instantation

begin
    --=============================================================================
    -- Asynchronous 
    --============================================================================= 

    --=============================================================================
    -- prescaler
    --=============================================================================                 
    p_prescaler: process (clk_i,reset_i) begin
        if(reset_i = '1')then
            inter_clk_en_o <= '0';
            s_clk_cnt      <= (others => '0');
        elsif rising_edge(clk_i) then
            -- default
            s_clk_cnt <= s_clk_cnt + 1;
            inter_clk_en_o <= '0';
            
            if(s_clk_cnt >= g_PRESCALER_VALUE-1)then
                inter_clk_en_o <= '1';
                s_clk_cnt      <= (others => '0');
            end if;
        end if;
    end process p_prescaler;

end arch_imp;
