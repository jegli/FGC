-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, reset synchronizer --
-- --
-------------------------------------------------------------------------------
--
-- unit name: reset synchronizer
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 07/01/2019
--
-- version: 1.0
--
-- description: synchronize asynchronous reset release with the clock 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: 
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity reset_synchronizer is
    Port ( clk_i        : in STD_LOGIC;     -- clock 
           rst_a_i      : in STD_LOGIC;     -- asynchronous reset
           rst_sync_o   : out STD_LOGIC);   -- synchronized reset
end reset_synchronizer;

architecture Behavioral of reset_synchronizer is
    -- signal declaration
    signal rff1 : std_logic;
    
begin
    process (clk_i, rst_a_i)
    begin
        if (rst_a_i = '1') then
            rff1        <= '1';
            rst_sync_o  <= '1';
        elsif (rising_edge(clk_i)) then
            rff1        <= '0';
            rst_sync_o  <= rff1;
        end if;
    end process;

end Behavioral;
