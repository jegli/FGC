-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, sync --
-- --
-------------------------------------------------------------------------------
--
-- unit name: accumulator
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 05/07/2018
--
-- version: 1.0
--
-- description: clock crossing domain synchronization
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- function generator package
library work;
use work.FGC_PACK.all;

entity sync is
    Port ( 

            in_i                : in    std_logic;  --input
            out_o               : out   std_logic;  --output
            
            -- general
            inter_clk_en_i      : in    std_logic;  -- accumulator interpolation clock
            clk_i               : in    std_logic;  -- general clock
            reset_i             : in    std_logic   -- general reset
          );
end sync;

architecture arch_imp of sync is
    signal s_out    : std_logic;
    signal s_latch  : std_logic;

begin
    --=============================================================================
    -- Asynchronous
    --=============================================================================
        out_o <= s_out;

    --=============================================================================
    -- synchronization
    --=============================================================================
    p_sync: process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_out   <= '0';
            s_latch <= '0';
        elsif (rising_edge (clk_i))then
            --detect input signal 
            if(s_latch = '0')then
                s_latch <= in_i; 
            else
                -- synchronized on interpolation clock
                if(inter_clk_en_i = '1')then
                    if(s_out = '0')then
                        -- set output to '1'
                        s_out <= '1';
                    else 
                        -- reset 
                        s_out   <= '0';
                        s_latch <= '0';
                    end if;
                end if;
            end if; 
        end if;
    end process p_sync;
end arch_imp;
