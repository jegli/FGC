
# Loading additional proc with user specified bodies to compute parameter values.
source [file join [file dirname [file dirname [info script]]] gui/FGC_v1_0.gtcl]

# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0" -display_name {General}]
  set_property tooltip {General IP parameters} ${Page_0}
  set g_BUFFER_SIZE [ipgui::add_param $IPINST -name "g_BUFFER_SIZE" -parent ${Page_0} -widget comboBox]
  set_property tooltip {memory size allocated for each function [in Bytes]} ${g_BUFFER_SIZE}
  set g_FGC_CHANNELS [ipgui::add_param $IPINST -name "g_FGC_CHANNELS" -parent ${Page_0}]
  set_property tooltip {number of function} ${g_FGC_CHANNELS}
  set g_INTERPOLATION_VALUE [ipgui::add_param $IPINST -name "g_INTERPOLATION_VALUE" -parent ${Page_0}]
  set_property tooltip {value to define the interpolation clock frequency (general clock / interpolation value = interpolation clock frequency)} ${g_INTERPOLATION_VALUE}
  #Adding Group
  set test [ipgui::add_group $IPINST -name "test" -parent ${Page_0} -display_name {AXI master interface}]
  set_property tooltip {Specify the AXI master interface paramaters} ${test}
  set g_M_AXI_DATA_WIDTH [ipgui::add_param $IPINST -name "g_M_AXI_DATA_WIDTH" -parent ${test} -widget comboBox]
  set_property tooltip {AXI master data width [in bits]} ${g_M_AXI_DATA_WIDTH}
  set g_M_AXI_ADDR_WIDTH [ipgui::add_param $IPINST -name "g_M_AXI_ADDR_WIDTH" -parent ${test} -widget comboBox]
  set_property tooltip {AXI master address width [in bits]} ${g_M_AXI_ADDR_WIDTH}


  #Adding Page
  set Functions [ipgui::add_page $IPINST -name "Functions"]
  set_property tooltip {Specify the function output width} ${Functions}
  set g_FUNCT_0_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_0_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_0_OUT_WIDTH}
  set g_FUNCT_1_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_1_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_1_OUT_WIDTH}
  set g_FUNCT_2_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_2_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_2_OUT_WIDTH}
  set g_FUNCT_3_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_3_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_3_OUT_WIDTH}
  set g_FUNCT_15_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_15_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_15_OUT_WIDTH}
  set g_FUNCT_14_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_14_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_14_OUT_WIDTH}
  set g_FUNCT_13_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_13_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_13_OUT_WIDTH}
  set g_FUNCT_12_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_12_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_12_OUT_WIDTH}
  set g_FUNCT_11_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_11_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_11_OUT_WIDTH}
  set g_FUNCT_10_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_10_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_10_OUT_WIDTH}
  set g_FUNCT_9_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_9_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_9_OUT_WIDTH}
  set g_FUNCT_8_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_8_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_8_OUT_WIDTH}
  set g_FUNCT_7_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_7_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_7_OUT_WIDTH}
  set g_FUNCT_6_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_6_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_6_OUT_WIDTH}
  ipgui::add_param $IPINST -name "g_FUNCT_5_OUT_WIDTH" -parent ${Functions}
  set g_FUNCT_4_OUT_WIDTH [ipgui::add_param $IPINST -name "g_FUNCT_4_OUT_WIDTH" -parent ${Functions}]
  set_property tooltip {function output width [in bits]} ${g_FUNCT_4_OUT_WIDTH}


}

proc update_PARAM_VALUE.g_FUNCT_0_OUT_WIDTH { PARAM_VALUE.g_FUNCT_0_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_0_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_0_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_0_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_0_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_0_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_0_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_0_OUT_WIDTH { PARAM_VALUE.g_FUNCT_0_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_0_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_10_OUT_WIDTH { PARAM_VALUE.g_FUNCT_10_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_10_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_10_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_10_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_10_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_10_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_10_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_10_OUT_WIDTH { PARAM_VALUE.g_FUNCT_10_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_10_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_11_OUT_WIDTH { PARAM_VALUE.g_FUNCT_11_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_11_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_11_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_11_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_11_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_11_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_11_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_11_OUT_WIDTH { PARAM_VALUE.g_FUNCT_11_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_11_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_12_OUT_WIDTH { PARAM_VALUE.g_FUNCT_12_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_12_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_12_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_12_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_12_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_12_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_12_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_12_OUT_WIDTH { PARAM_VALUE.g_FUNCT_12_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_12_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_13_OUT_WIDTH { PARAM_VALUE.g_FUNCT_13_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_13_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_13_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_13_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_13_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_13_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_13_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_13_OUT_WIDTH { PARAM_VALUE.g_FUNCT_13_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_13_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_14_OUT_WIDTH { PARAM_VALUE.g_FUNCT_14_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_14_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_14_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_14_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_14_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_14_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_14_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_14_OUT_WIDTH { PARAM_VALUE.g_FUNCT_14_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_14_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_15_OUT_WIDTH { PARAM_VALUE.g_FUNCT_15_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_15_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_15_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_15_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_15_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_15_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_15_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_15_OUT_WIDTH { PARAM_VALUE.g_FUNCT_15_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_15_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_1_OUT_WIDTH { PARAM_VALUE.g_FUNCT_1_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_1_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_1_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_1_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_1_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_1_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_1_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_1_OUT_WIDTH { PARAM_VALUE.g_FUNCT_1_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_1_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_2_OUT_WIDTH { PARAM_VALUE.g_FUNCT_2_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_2_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_2_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_2_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_2_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_2_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_2_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_2_OUT_WIDTH { PARAM_VALUE.g_FUNCT_2_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_2_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_3_OUT_WIDTH { PARAM_VALUE.g_FUNCT_3_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_3_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_3_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_3_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_3_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_3_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_3_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_3_OUT_WIDTH { PARAM_VALUE.g_FUNCT_3_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_3_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_4_OUT_WIDTH { PARAM_VALUE.g_FUNCT_4_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_4_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_4_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_4_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_4_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_4_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_4_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_4_OUT_WIDTH { PARAM_VALUE.g_FUNCT_4_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_4_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_5_OUT_WIDTH { PARAM_VALUE.g_FUNCT_5_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_5_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_5_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_5_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_5_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_5_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_5_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_5_OUT_WIDTH { PARAM_VALUE.g_FUNCT_5_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_5_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_6_OUT_WIDTH { PARAM_VALUE.g_FUNCT_6_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_6_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_6_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_6_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_6_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_6_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_6_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_6_OUT_WIDTH { PARAM_VALUE.g_FUNCT_6_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_6_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_7_OUT_WIDTH { PARAM_VALUE.g_FUNCT_7_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_7_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_7_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_7_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_7_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_7_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_7_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_7_OUT_WIDTH { PARAM_VALUE.g_FUNCT_7_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_7_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_8_OUT_WIDTH { PARAM_VALUE.g_FUNCT_8_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_8_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_8_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_8_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_8_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_8_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_8_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_8_OUT_WIDTH { PARAM_VALUE.g_FUNCT_8_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_8_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_FUNCT_9_OUT_WIDTH { PARAM_VALUE.g_FUNCT_9_OUT_WIDTH PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FUNCT_9_OUT_WIDTH when any of the dependent parameters in the arguments change
	
	set g_FUNCT_9_OUT_WIDTH ${PARAM_VALUE.g_FUNCT_9_OUT_WIDTH}
	set g_FGC_CHANNELS ${PARAM_VALUE.g_FGC_CHANNELS}
	set values(g_FGC_CHANNELS) [get_property value $g_FGC_CHANNELS]
	if { [gen_USERPARAMETER_g_FUNCT_9_OUT_WIDTH_ENABLEMENT $values(g_FGC_CHANNELS)] } {
		set_property enabled true $g_FUNCT_9_OUT_WIDTH
	} else {
		set_property enabled false $g_FUNCT_9_OUT_WIDTH
	}
}

proc validate_PARAM_VALUE.g_FUNCT_9_OUT_WIDTH { PARAM_VALUE.g_FUNCT_9_OUT_WIDTH } {
	# Procedure called to validate g_FUNCT_9_OUT_WIDTH
	return true
}

proc update_PARAM_VALUE.g_BUFFER_SIZE { PARAM_VALUE.g_BUFFER_SIZE } {
	# Procedure called to update g_BUFFER_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_BUFFER_SIZE { PARAM_VALUE.g_BUFFER_SIZE } {
	# Procedure called to validate g_BUFFER_SIZE
	return true
}

proc update_PARAM_VALUE.g_FGC_CHANNELS { PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to update g_FGC_CHANNELS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_FGC_CHANNELS { PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to validate g_FGC_CHANNELS
	return true
}

proc update_PARAM_VALUE.g_INTERPOLATION_VALUE { PARAM_VALUE.g_INTERPOLATION_VALUE } {
	# Procedure called to update g_INTERPOLATION_VALUE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_INTERPOLATION_VALUE { PARAM_VALUE.g_INTERPOLATION_VALUE } {
	# Procedure called to validate g_INTERPOLATION_VALUE
	return true
}

proc update_PARAM_VALUE.g_M_AXI_ADDR_WIDTH { PARAM_VALUE.g_M_AXI_ADDR_WIDTH } {
	# Procedure called to update g_M_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_M_AXI_ADDR_WIDTH { PARAM_VALUE.g_M_AXI_ADDR_WIDTH } {
	# Procedure called to validate g_M_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.g_M_AXI_DATA_WIDTH { PARAM_VALUE.g_M_AXI_DATA_WIDTH } {
	# Procedure called to update g_M_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_M_AXI_DATA_WIDTH { PARAM_VALUE.g_M_AXI_DATA_WIDTH } {
	# Procedure called to validate g_M_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.g_SIMULATION { PARAM_VALUE.g_SIMULATION } {
	# Procedure called to update g_SIMULATION when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_SIMULATION { PARAM_VALUE.g_SIMULATION } {
	# Procedure called to validate g_SIMULATION
	return true
}


proc update_MODELPARAM_VALUE.g_FGC_CHANNELS { MODELPARAM_VALUE.g_FGC_CHANNELS PARAM_VALUE.g_FGC_CHANNELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FGC_CHANNELS}] ${MODELPARAM_VALUE.g_FGC_CHANNELS}
}

proc update_MODELPARAM_VALUE.g_M_AXI_DATA_WIDTH { MODELPARAM_VALUE.g_M_AXI_DATA_WIDTH PARAM_VALUE.g_M_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_M_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.g_M_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.g_M_AXI_ADDR_WIDTH { MODELPARAM_VALUE.g_M_AXI_ADDR_WIDTH PARAM_VALUE.g_M_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_M_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.g_M_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.g_BUFFER_SIZE { MODELPARAM_VALUE.g_BUFFER_SIZE PARAM_VALUE.g_BUFFER_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_BUFFER_SIZE}] ${MODELPARAM_VALUE.g_BUFFER_SIZE}
}

proc update_MODELPARAM_VALUE.g_SIMULATION { MODELPARAM_VALUE.g_SIMULATION PARAM_VALUE.g_SIMULATION } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_SIMULATION}] ${MODELPARAM_VALUE.g_SIMULATION}
}

proc update_MODELPARAM_VALUE.g_FUNCT_0_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_0_OUT_WIDTH PARAM_VALUE.g_FUNCT_0_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_0_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_0_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_1_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_1_OUT_WIDTH PARAM_VALUE.g_FUNCT_1_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_1_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_1_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_2_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_2_OUT_WIDTH PARAM_VALUE.g_FUNCT_2_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_2_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_2_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_3_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_3_OUT_WIDTH PARAM_VALUE.g_FUNCT_3_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_3_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_3_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_4_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_4_OUT_WIDTH PARAM_VALUE.g_FUNCT_4_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_4_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_4_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_5_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_5_OUT_WIDTH PARAM_VALUE.g_FUNCT_5_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_5_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_5_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_6_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_6_OUT_WIDTH PARAM_VALUE.g_FUNCT_6_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_6_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_6_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_7_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_7_OUT_WIDTH PARAM_VALUE.g_FUNCT_7_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_7_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_7_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_8_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_8_OUT_WIDTH PARAM_VALUE.g_FUNCT_8_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_8_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_8_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_9_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_9_OUT_WIDTH PARAM_VALUE.g_FUNCT_9_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_9_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_9_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_10_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_10_OUT_WIDTH PARAM_VALUE.g_FUNCT_10_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_10_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_10_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_11_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_11_OUT_WIDTH PARAM_VALUE.g_FUNCT_11_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_11_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_11_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_12_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_12_OUT_WIDTH PARAM_VALUE.g_FUNCT_12_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_12_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_12_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_13_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_13_OUT_WIDTH PARAM_VALUE.g_FUNCT_13_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_13_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_13_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_14_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_14_OUT_WIDTH PARAM_VALUE.g_FUNCT_14_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_14_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_14_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FUNCT_15_OUT_WIDTH { MODELPARAM_VALUE.g_FUNCT_15_OUT_WIDTH PARAM_VALUE.g_FUNCT_15_OUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FUNCT_15_OUT_WIDTH}] ${MODELPARAM_VALUE.g_FUNCT_15_OUT_WIDTH}
}

proc update_MODELPARAM_VALUE.g_INTERPOLATION_VALUE { MODELPARAM_VALUE.g_INTERPOLATION_VALUE PARAM_VALUE.g_INTERPOLATION_VALUE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_INTERPOLATION_VALUE}] ${MODELPARAM_VALUE.g_INTERPOLATION_VALUE}
}

